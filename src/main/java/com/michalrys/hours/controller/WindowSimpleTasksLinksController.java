/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.controller;

import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.tasklink.TaskLinkContainer;
import com.michalrys.hours.model.tasklink.TaskLinkPublicPoint;
import com.michalrys.hours.view.hourssummary.MVCTaskLinks;

import java.io.File;
import java.util.List;
import java.util.Map;

public class WindowSimpleTasksLinksController implements MVCTaskLinks.Controller {
    private final MVCTaskLinks.View view;
    private File publicPoint;
    private HourTasksSummary modelTasksSummary;
    private TaskLinkContainer modelTaskLinks;

    public WindowSimpleTasksLinksController(MVCTaskLinks.View windowHoursSummaryView) {
        this.view = windowHoursSummaryView;
    }

    @Override
    public void setModel(HourTasksSummary modelTasksSummary) {
        this.modelTasksSummary = modelTasksSummary;
        publicPoint = new File(modelTasksSummary.getPublicPointPath());
        modelTaskLinks = new TaskLinkPublicPoint(publicPoint.getAbsolutePath());
        view.setModelTaskLinks(this.modelTaskLinks);
    }

    @Override
    public void setTasksInWindow() {
        List<String> currentDayTasks = modelTasksSummary.getCurrentDayTasks();
        Map<String, String> tasksLinksForCurrentDay = modelTaskLinks.get(currentDayTasks);
        view.setTaskLinksInWindow(tasksLinksForCurrentDay);
        view.setWindowSize(currentDayTasks.size());
    }

    @Override
    public void setAllTasksWithNoLinkInWindow() {
        Map<String, String> taskLinksWithoutLinksDefined = modelTaskLinks.getAllWithoutLink();
        view.setTaskLinksInWindow(taskLinksWithoutLinksDefined);
        view.setWindowSize(taskLinksWithoutLinksDefined.size());
    }
}
