/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.controller;

import com.michalrys.hours.model.excel.ExcelSummary;
import com.michalrys.hours.model.excel.FileSummary;
import com.michalrys.hours.model.excel.SubtasksCorrectorForExcelSummary;
import com.michalrys.hours.model.hourtask.Archivizer;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummaryAll;
import com.michalrys.hours.view.hourssummary.MVCSummary;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class WindowHoursSummaryController implements MVCSummary.Controller {
    private final MVCSummary.View view;
    private File publicPoint;
    private HourTasksSummary modelTasksSummary;
    public static final String THIS_DAY_IS_CLOSED = "this day is closed";
    public static final String THIS_DAY_IS_NOT_CLOSED_YET = "this day is not closed yet";
    private static final String NO_TASK_YET = "NO TASK YET";

    public WindowHoursSummaryController(MVCSummary.View windowHoursSummaryView) {
        this.view = windowHoursSummaryView;
    }

    @Override
    public void setUpController(File publicPoint) {
        this.publicPoint = publicPoint;
        modelTasksSummary = new HourTasksSummaryAll(publicPoint.getAbsolutePath());
        setModelInView();
    }

    @Override
    public void setModelInView() {
        view.setModel(modelTasksSummary);
    }

    @Override
    public void getDataFromModelAndSetupWindow() {
        view.setFDate(modelTasksSummary.getCurrentDay());
        view.setWW(String.valueOf(modelTasksSummary.getWorkWeek()));

        //FIXME now it is working, but bad. Make it more clear later.
        if (modelTasksSummary.getLatestHourTask().getTask().equals(THIS_DAY_IS_NOT_CLOSED_YET)) {
            view.setEndDayMessage(THIS_DAY_IS_NOT_CLOSED_YET);
        } else if (modelTasksSummary.getLatestHourTask().getTask().equals(NO_TASK_YET)) {
            view.setEndDayMessage("no tasks yet");
        } else {
            view.setEndDayMessage(THIS_DAY_IS_CLOSED);
        }

        view.updateButtonCloseDay();
        view.setStartTime(modelTasksSummary.getStartTime());
        view.setEndTime(modelTasksSummary.getEndTime());
        view.setTotalTime(modelTasksSummary.getTotalTime());
        view.setWorkTime(modelTasksSummary.getWorkTime());
        view.setBreakTime(modelTasksSummary.getBreakTime());

        if (modelTasksSummary.getHowManyDaysAreInPublicPoint() == 0) {
            view.setSummary("No task hours yet.");
            view.setSummaryDetails("No task hours yet.");
        } else {
            if (view.isRbTimeStyleHSelected()) {
                view.setSummary(modelTasksSummary.getSummary());
                view.setSummaryDetails(modelTasksSummary.getSummaryDetails());
            } else {
                view.setSummary(modelTasksSummary.getSummaryInHourMinuteStyle());
                view.setSummaryDetails(modelTasksSummary.getSummaryDetailsInHourMinuteStyle());
            }
        }
        view.setLabelWorkPercentage(modelTasksSummary.getWorkTime(), modelTasksSummary.getTotalTime());
        view.setLabelBreakPercentage(modelTasksSummary.getBreakTime(), modelTasksSummary.getTotalTime());

        if (modelTasksSummary.getLatestHourTask().getTask().equals(HourTasksSummaryAll.THIS_DAY_IS_NOT_CLOSED_YET)) {
            view.setLatestTaskFromCurrentDay(modelTasksSummary.getBeforeLatestHourTask());
        } else {
            view.setLatestTaskFromCurrentDay(modelTasksSummary.getLatestHourTask());
        }
    }

    @Override
    public void setPreviousDay() {
        if (modelTasksSummary.getHowManyDaysAreInPublicPoint() <= 1) {
            return;
        }
        modelTasksSummary.setPreviousDay();
        getDataFromModelAndSetupWindow();
    }

    @Override
    public void setNextDay() {
        if (modelTasksSummary.getHowManyDaysAreInPublicPoint() <= 1) {
            return;
        }
        modelTasksSummary.setNextDay();
        getDataFromModelAndSetupWindow();
    }

    @Override
    public void closeDay() {
        modelTasksSummary.closeDay();
        view.setEndDayMessage(modelTasksSummary.getEndDayMessage());
        view.updateButtonCloseDay();
    }

    @Override
    public void setLeavingTime() {
        view.setLeavingTime();
    }

    @Override
    public void createExcelSummary() {
        new SubtasksCorrectorForExcelSummary(publicPoint.getAbsolutePath()).updateSubTasks();
        view.initData();
        FileSummary summary = new ExcelSummary(modelTasksSummary);
        summary.writeFile();
    }

    @Override
    public void archiveOfHourTaskFiles(double x, double y, String currentWorkWeek) {
        JFrame simpleWindow = new JFrame();
        simpleWindow.setAlwaysOnTop(true);
        simpleWindow.setSize(250, 160);
        simpleWindow.setTitle("Archive...");
        simpleWindow.setLocation((int) x -50, (int) y + 70);

        JLabel wwStartLabel = new JLabel("from WW");
        JLabel wwEndLabel = new JLabel("to WW");
        wwStartLabel.setBounds(30, 5, 100, 20);
        wwEndLabel.setBounds(125, 5, 100, 20);

        JTextField wwStart = new JTextField("1");
        JTextField wwEnd = new JTextField(currentWorkWeek);
        wwStart.setBounds(30, 30, 70, 23);
        wwEnd.setBounds(125, 30, 70, 23);

        JButton archive = new JButton("ARCHIVE");
        archive.setBounds(30, 60, 165, 40);
        archive.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String start = wwStart.getText();
                String end = wwEnd.getText();

                if(!start.matches("[0-9]{1,2}") || !end.matches("[0-9]{1,2}")) {
                    wwStart.setText("1");
                    wwEnd.setText(currentWorkWeek);
                    return;
                }
                int startWorkWeekForArchivization = Integer.parseInt(start);
                int endWorkWeekForArchivization = Integer.parseInt(end);

                Archivizer archivizer = new Archivizer(publicPoint.getAbsolutePath());
                archivizer.move(startWorkWeekForArchivization, endWorkWeekForArchivization);
                simpleWindow.dispose();
                view.initData();
            }
        });

        simpleWindow.add(wwStartLabel);
        simpleWindow.add(wwEndLabel);
        simpleWindow.add(wwStart);
        simpleWindow.add(wwEnd);
        simpleWindow.add(archive);
        simpleWindow.setLayout(null);
        simpleWindow.setVisible(true);
    }
}