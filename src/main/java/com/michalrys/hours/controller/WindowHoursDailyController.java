/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.controller;

import com.michalrys.hours.tools.JSONMySimplestParser;
import com.michalrys.hours.view.hoursdaily.MVCDaily;
import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.tasks.TaskContainer;
import com.michalrys.hours.model.tasks.TasksPublicPoint;
import com.michalrys.hours.view.hoursdaily.RadioButtonType;
import org.json.JSONObject;

import java.io.File;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class WindowHoursDailyController implements MVCDaily.Controller {
    private final MVCDaily.View windowHoursDailyView;
    private TaskContainer modelTasks;
    private HourTask modelTask;
    private File publicPoint;

    public WindowHoursDailyController(MVCDaily.View windowHoursDailyView) {
        this.windowHoursDailyView = windowHoursDailyView;
    }

    @Override
    public void setModel(HourTask modelTask, TaskContainer modelTasks, File publicPoint) {
        this.modelTask = modelTask;
        this.modelTasks = modelTasks;
        this.publicPoint = publicPoint;
    }

    @Override
    public void startUpModel() {
        modelTasks = new TasksPublicPoint(publicPoint.getAbsolutePath());
        modelTasks.read();
    }

    @Override
    public void getTasks() {
        Set<String> tasks = modelTasks.get();
        windowHoursDailyView.setFieldTaskContextMenu(tasks);
    }

    @Override
    public boolean existsInTasks(String task) {
        Set<String> set = modelTasks.get();
        return set.contains(task);
    }

    @Override
    public void setTaskDescriptionsWithinCurrentWorkweek(String task) {
        LocalDate now = LocalDate.now();
        int dayOfWeek = now.getDayOfWeek().getValue();

        File[] files = publicPoint.listFiles();
        List<File> filesInTheSameWorkWeek = new ArrayList<>();
        for (File file : files) {
            String name = file.getName();
            if (!name.contains("HourTask_")) {
                continue;
            }
            String fileDate = name.substring(9, 19);
            LocalDate parsedDay = LocalDate.parse(fileDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            int difference = (int) Duration.between(now.atStartOfDay(), parsedDay.atStartOfDay()).toDays();
            if (difference == 0) {
                filesInTheSameWorkWeek.add(file);
            } else if (difference < 0 && difference > -dayOfWeek) {
                filesInTheSameWorkWeek.add(file);
            } else if (difference > 0 && difference < 7 - dayOfWeek) {
                filesInTheSameWorkWeek.add(file);
            }
        }
        Set<String> taskDescriptions = new TreeSet<>();
        for (File file : filesInTheSameWorkWeek) {
            JSONObject hourTask = JSONMySimplestParser.read(file);
            String taskFromFile = hourTask.getString("task");
            if (task.equals(taskFromFile)) {
                String description = hourTask.getString("description");
                taskDescriptions.add(description);
            }
        }
        windowHoursDailyView.setFieldDescription(taskDescriptions);
    }

    @Override
    public void bSaveClicked(RadioButtonType radioButtonType, String fTask, String fDescription) {
        if (fTask.isEmpty()) {
            fTask = "empty";
        }

        modelTasks.add(fTask);
        modelTasks.write();

        TaskType taskType = TaskType.parseRadioButtonType(radioButtonType);
        modelTask = new HourTaskImpl(taskType, publicPoint.getAbsolutePath());

        modelTask.setTimeStamp(LocalDateTime.now());
        modelTask.setTask(fTask);
        modelTask.setDescription(fDescription);
        modelTask.write();

        if (windowHoursDailyView.isInTray()) {
            windowHoursDailyView.hide();
        } else {
            System.exit(0);
        }
    }

    @Override
    public void bTaskRemoveClicked(String fTask) {
        modelTasks.remove(fTask);
        modelTasks.write();
        getTasks();
    }

    @Override
    public void editDraggedAndDroppedFile(String[] inputArg) {
        String filePath = inputArg[0];
        File file = new File(filePath);
        String fileName = file.getName();
        if (!fileName.matches(HourTaskImpl.FILE_NAME_VALIDATION_REGEX)) {
            return;
        }
        JSONObject json = JSONMySimplestParser.read(file);
        String task = json.getString(HourTaskImpl.KEY_TASK);
        String timeStamp = json.getString(HourTaskImpl.KEY_TIME_STAMP);
        String description = json.getString(HourTaskImpl.KEY_DESCRIPTION);
        String type = json.getString(HourTaskImpl.KEY_TYPE);

        windowHoursDailyView.updateDraggedAndDroppedFile(file, task, timeStamp, description, type);
    }
}
