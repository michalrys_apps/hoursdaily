/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.tools;

import java.time.LocalDate;

public class LocalDateDescendingComparator {

    public static int compareTo(LocalDate o1, LocalDate o2) {
        if (o1.isBefore(o2)) {
            return 1;
        } else if (o1.isAfter(o2)) {
            return -1;
        } else {
            return 0;
        }
    }
}