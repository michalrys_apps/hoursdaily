/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.tools;

import java.time.format.DateTimeFormatter;

public class TimestampFormatter {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd,HH:mm:ss");
    private static final DateTimeFormatter dateTimeForFileFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final DateTimeFormatter timeHHmmFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static DateTimeFormatter getDateTime() {
        return dateTimeFormatter;
    }

    public static DateTimeFormatter getTime() {
        return timeFormatter;
    }

    public static DateTimeFormatter getDate() {
        return dateFormatter;
    }

    public static DateTimeFormatter getDateTimeForFileName() {
        return dateTimeForFileFormatter;
    }

    public static DateTimeFormatter getTimeHHmmFormatter() {
        return timeHHmmFormatter;
    }
}
