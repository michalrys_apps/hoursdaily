package com.michalrys.hours.model.tasks;

import java.util.Set;

public interface TaskContainer {
    Set<String> get();

    void read();

    void readTasksFromAllHourTasks();

    void readTasksFromFile();

    void write();

    void add(String fTask);

    void remove(String fTask);
}
