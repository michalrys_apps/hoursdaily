/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.tasks;

import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.tools.JSONMySimplestParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TasksPublicPoint implements TaskContainer {
    private static final String KEY_IN_HOURTASK_FOR_TASK = "task";
    private static final String KEY_TASKS = "tasks";
    private static final String TASKS_FILE_NAME = "Tasks.json";
    private static final String NO_TASKS_YET = "No tasks yet";
    private final File publicPoint;
    private File tasksFile;
    private Set<String> tasks = new LinkedHashSet<>();

    public TasksPublicPoint(String publicPointPath) {
        publicPoint = new File(publicPointPath);
        setTasksFile();
        tasks.add(NO_TASKS_YET);
    }

    private void setTasksFile() {
        tasksFile = new File(publicPoint.getAbsolutePath() + "/" + TASKS_FILE_NAME);
    }

    @Override
    public Set<String> get() {
        return tasks;
    }

    @Override
    public void read() {
        if(tasksFile.exists()) {
            readTasksFromFile();
        } else {
            readTasksFromAllHourTasks();
        }
    }

    @Override
    public void readTasksFromAllHourTasks() {
        tasks = new LinkedHashSet<>();
        File[] files = publicPoint.listFiles();

        for (File file : files) {
            if(!hasHourTaskFileProperName(file)) {
                continue;
            }
            JSONObject readFile = JSONMySimplestParser.read(file);

            Set<String> availableKeys = readFile.keySet();
            if (!availableKeys.contains(KEY_IN_HOURTASK_FOR_TASK)) {
                continue;
            }
            String task = readFile.getString(KEY_IN_HOURTASK_FOR_TASK);

            if (task.length() > 0) {
                tasks.add(task);
            }
        }
        if(tasks.isEmpty()) {
            tasks.add(NO_TASKS_YET);
        }
    }

    private boolean hasHourTaskFileProperName(File file) {
        String name = file.getName();
        Pattern pattern = Pattern.compile(HourTaskImpl.FILE_NAME_VALIDATION_REGEX);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    @Override
    public void readTasksFromFile() {
        JSONObject tasksRead = JSONMySimplestParser.read(tasksFile);
        Set<String> keys = tasksRead.keySet();

        if (!keys.contains(KEY_TASKS)) {
            return;
        }

        JSONArray jsonArray = tasksRead.getJSONArray(KEY_TASKS);
        tasks = new LinkedHashSet<>();
        for (Object o : jsonArray) {
            tasks.add(o.toString());
        }
    }

    @Override
    public void add(String fTask) {
        tasks.add(fTask);
    }

    @Override
    public void remove(String fTask) {
        if(tasks.contains(fTask)) {
            tasks.remove(fTask);
        }
    }

    @Override
    public void write() {
        if(tasks.size() >= 2) {
            tasks.remove(NO_TASKS_YET);
        }
        JSONMySimplestParser.write(tasksFile, KEY_TASKS, tasks);
    }
}