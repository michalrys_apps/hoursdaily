package com.michalrys.hours.model.hourtask.container;

import com.michalrys.hours.model.hourtask.HourTask;

import java.time.LocalDate;
import java.util.List;

public interface HourTaskContainer {
    void read();

    List<LocalDate> getDays();

    List<HourTask> get(LocalDate date);
}
