package com.michalrys.hours.model.hourtask;

import java.time.LocalDateTime;

public interface HourTask {
    TaskType getType();

    void setTimeStamp(LocalDateTime timeStamp);

    LocalDateTime getTimeStamp();

    void setTask(String task);

    String getTask();

    void setDescription(String description);

    String getDescription();

    void write();

//    List<String> getCurrentDayTasks();
}
