package com.michalrys.hours.model.hourtask.summary;

import com.michalrys.hours.model.hourtask.HourTask;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface HourTasksSummary {
    LocalDate getCurrentDay();

    void setPreviousDay();

    void setNextDay();

    LocalTime getStartTime();

    LocalTime getEndTime();

    LocalTime getTotalTime();

    LocalTime getWorkTime();

    LocalTime getBreakTime();

    String getEndDayMessage();

    void closeDay();

    String getSummary();

    String getSummaryInHourMinuteStyle();

    String getSummaryDetails();

    String getSummaryDetailsInHourMinuteStyle();

    int getHowManyDaysAreInPublicPoint();

    HourTask getLatestHourTask();

    int getWorkWeek();

    List<String> getCurrentDayTasks();

    String getPublicPointPath();

    HourTask getBeforeLatestHourTask();
}
