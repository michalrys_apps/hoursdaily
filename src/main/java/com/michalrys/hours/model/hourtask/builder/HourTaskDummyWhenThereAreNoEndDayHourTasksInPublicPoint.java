/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask.builder;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;

import java.io.File;
import java.time.LocalDateTime;

public class HourTaskDummyWhenThereAreNoEndDayHourTasksInPublicPoint implements HourTaskBuilder {
    private final File publicPoint;
    public static final String THIS_DAY_IS_NOT_CLOSED_YET = "this day is not closed yet";

    public HourTaskDummyWhenThereAreNoEndDayHourTasksInPublicPoint(File publicPoint) {
        this.publicPoint = publicPoint;
    }

    @Override
    public HourTask build() {
        HourTask hourTask = new HourTaskImpl(TaskType.ENDDAY, publicPoint.getAbsolutePath());
        hourTask.setTimeStamp(LocalDateTime.now());
        hourTask.setTask(THIS_DAY_IS_NOT_CLOSED_YET);
        hourTask.setDescription("In public point: " + publicPoint.getAbsolutePath() + " there are no " +
                "tasks yet. Please run hoursDaily first and generate some hourTask.");
        return hourTask;
    }
}
