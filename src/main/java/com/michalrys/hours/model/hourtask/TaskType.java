package com.michalrys.hours.model.hourtask;

import com.michalrys.hours.view.hoursdaily.RadioButtonType;

public enum TaskType {
    WORK,
    BREAK,
    ENDDAY;

    public static TaskType parseRadioButtonType(RadioButtonType radioButtonType) {
        switch (radioButtonType) {
            case WORK:
                return TaskType.WORK;
            case BREAK:
                return TaskType.BREAK;
            case ENDDAY:
                return TaskType.ENDDAY;
            default:
                return TaskType.WORK;
        }
    }

    public static TaskType valueOfOrDefaultAsWork(String name) {
        switch (name.toUpperCase()) {
            case "WORK":
                return TaskType.WORK;
            case "BREAK":
                return TaskType.BREAK;
            case "ENDDAY":
                return TaskType.ENDDAY;
            default:
                return TaskType.WORK;
        }
    }
}
