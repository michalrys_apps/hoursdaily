package com.michalrys.hours.model.hourtask.builder;

import com.michalrys.hours.model.hourtask.HourTask;

public interface HourTaskBuilder {
    HourTask build() ;
}
