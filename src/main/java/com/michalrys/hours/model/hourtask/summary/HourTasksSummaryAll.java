/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask.summary;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.hourtask.builder.HourTaskDummyWhenThereAreNoEndDayHourTasksInPublicPoint;
import com.michalrys.hours.model.hourtask.builder.HourTaskDummyWhenThereAreNoHourTasksInPublicPoint;
import com.michalrys.hours.model.hourtask.container.HourTaskContainer;
import com.michalrys.hours.model.hourtask.container.HourTasksAll;
import com.michalrys.hours.tools.TimestampFormatter;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class HourTasksSummaryAll implements HourTasksSummary {
    public static final String THIS_DAY_IS_CLOSED = "this day is closed";
    public static final String THIS_DAY_IS_NOT_CLOSED_YET = "this day is not closed yet";
    private final String publicPointPath;
    private HourTaskContainer tasks;
    private int currentDayId = 0;
    private int dayIdLast;
    private String endDayMessage;
    private List<HourTask> currentHourTasks;
    private List<HourTask> currentWorkHourTasks;
    private List<HourTask> currentBreakHourTasks;
    private HourTask currentEndDayHourTask;

    private Map<String, List<String>> currentWorkHourTasksSummary = new HashMap<>();
    private Map<HourTask, Double> currentWorkHourTasksSummary2 = new LinkedHashMap<>();
    private Map<String, List<String>> currentBreakHourTasksSummary = new HashMap<>();
    private Map<HourTask, Double> currentBreakHourTasksSummary2 = new LinkedHashMap<>();

    public HourTasksSummaryAll(String publicPointPath) {
        this.publicPointPath = publicPointPath;
        tasks = new HourTasksAll(publicPointPath);
        tasks.read();
        setDayIdLast();
        setCurrentTasks();
    }

    private void setCurrentTasks() {//FIXME later
        LocalDate currentDay = tasks.getDays().get(currentDayId);
        currentHourTasks = tasks.get(currentDay);
        setWorkBreakEndDayHourTasks();
    }

    private void setWorkBreakEndDayHourTasks() {
        currentWorkHourTasks = new ArrayList<>();
        currentBreakHourTasks = new ArrayList<>();

        for (HourTask hourTask : currentHourTasks) {
            TaskType type = hourTask.getType();
            switch (type) {
                case WORK:
                    currentWorkHourTasks.add(hourTask);
                    break;
                case BREAK:
                    currentBreakHourTasks.add(hourTask);
                    break;
                case ENDDAY:
                    currentEndDayHourTask = hourTask;
                    endDayMessage = THIS_DAY_IS_CLOSED;
                    return;
            }
        }
        endDayMessage = THIS_DAY_IS_CLOSED;

        if (currentHourTasks.size() == 0) {
            currentEndDayHourTask = new HourTaskDummyWhenThereAreNoHourTasksInPublicPoint(
                    new File(publicPointPath)).build();
            endDayMessage = THIS_DAY_IS_NOT_CLOSED_YET;
        } else {
            currentEndDayHourTask = new HourTaskDummyWhenThereAreNoEndDayHourTasksInPublicPoint(
                    new File(publicPointPath)).build();
            endDayMessage = THIS_DAY_IS_NOT_CLOSED_YET;
        }
        //FIXME later - auto close tasks from previous days
        currentHourTasks.add(currentEndDayHourTask);

        HourTask lastHourTask = currentHourTasks.get(currentHourTasks.size() - 2);
        LocalDateTime lastTimeStamp = lastHourTask.getTimeStamp();
        if(LocalDate.now().isAfter(lastTimeStamp.toLocalDate())) {
            LocalDate localDate = lastTimeStamp.toLocalDate();
            String date = localDate.format(TimestampFormatter.getDate());
            currentEndDayHourTask.setTimeStamp(
                    LocalDateTime.parse(date + ",23:59:59", TimestampFormatter.getDateTime()));
            currentEndDayHourTask.setTask("autoclose");
            currentEndDayHourTask.setDescription("this day was autoclosed");
            currentEndDayHourTask.write();
        }
    }

    @Override
    public String getEndDayMessage() {
        return endDayMessage;
    }

    private void setDayIdLast() {
        dayIdLast = tasks.getDays().size() - 1;
    }

    @Override
    public LocalDate getCurrentDay() {
        return tasks.getDays().get(currentDayId);
    }

    @Override
    public void setPreviousDay() {
        if (currentDayId == dayIdLast) {
            currentDayId = 0;
            setCurrentTasks();
        } else {
            currentDayId++;
            setCurrentTasks();
        }
    }

    @Override
    public void setNextDay() {
        if (currentDayId == 0) {
            currentDayId = dayIdLast;
            setCurrentTasks();
        } else {
            currentDayId--;
            setCurrentTasks();
        }
    }

    @Override
    public LocalTime getStartTime() {
        HourTask hourTask = currentHourTasks.get(0);
        LocalDateTime timeStamp = hourTask.getTimeStamp();
        return timeStamp.toLocalTime();
    }

    @Override
    public LocalTime getEndTime() {
        LocalDateTime timeStamp = currentEndDayHourTask.getTimeStamp();
        return timeStamp.toLocalTime();
    }

    @Override
    public LocalTime getTotalTime() {
        if (currentHourTasks.size() == 1) {
            return LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        }
        LocalTime startHour = getStartTime();
        LocalTime endTime = getEndTime();
        long timeIncrement = ChronoUnit.SECONDS.between(startHour, endTime);
        LocalTime totalTime = LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        return totalTime.plusSeconds(timeIncrement);
    }

    @Override
    public LocalTime getWorkTime() {
        currentWorkHourTasksSummary = new HashMap<>();
        currentWorkHourTasksSummary2 = new LinkedHashMap<>();
        return getTotalTimeForTaskType(currentHourTasks, TaskType.WORK, currentWorkHourTasksSummary);
    }

    @Override
    public LocalTime getBreakTime() {
        currentBreakHourTasksSummary = new HashMap<>();
        currentBreakHourTasksSummary2 = new LinkedHashMap<>();
        return getTotalTimeForTaskType(currentBreakHourTasks, TaskType.BREAK, currentBreakHourTasksSummary);
    }

    private LocalTime getTotalTimeForTaskType(List<HourTask> currentTypeTasks, TaskType type,
                                              Map<String, List<String>> currentTypeHourTasksSummary) {
        if (currentTypeTasks.isEmpty()) {
            return LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        }
        long totalSecondsOfWork = 0;
        for (int i = 0; i < currentHourTasks.size() - 1; i++) {
            HourTask start = currentHourTasks.get(i);
            if (!start.getType().equals(type)) {
                continue;
            }
            HourTask end = currentHourTasks.get(i + 1);

            LocalDateTime startTime = start.getTimeStamp();
            LocalDateTime endTime = end.getTimeStamp();
            long timeDuration = ChronoUnit.SECONDS.between(startTime.toLocalTime(), endTime.toLocalTime());

            totalSecondsOfWork += timeDuration;

            String task = start.getTask();
            if (currentTypeHourTasksSummary.containsKey(task)) {
                List<String> summary = currentTypeHourTasksSummary.get(task);
                long time = Long.parseLong(summary.get(0));
                time += timeDuration;
                summary.set(0, String.valueOf(time));
                summary.add(start.getDescription());
                currentTypeHourTasksSummary.put(task, summary);
            } else {
                List<String> summary = new ArrayList<>();
                summary.add(String.valueOf(timeDuration));
                summary.add(start.getDescription());
                currentTypeHourTasksSummary.put(task, summary);
            }

            if (type.equals(TaskType.WORK)) {
                currentWorkHourTasksSummary2.put(currentHourTasks.get(i), (double) timeDuration);
            } else if (type.equals(TaskType.BREAK)) {
                currentBreakHourTasksSummary2.put(currentHourTasks.get(i), (double) timeDuration);
            }
        }
        LocalTime totalTime = LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        return totalTime.plusSeconds(totalSecondsOfWork);
    }

    @Override
    public void closeDay() {
        int size = currentHourTasks.size();
        HourTask hourTaskBeforeTemporaryEndDay = currentHourTasks.get(size - 2);
        String lastTaskName = hourTaskBeforeTemporaryEndDay.getTask();
        HourTask endDay = new HourTaskImpl(TaskType.ENDDAY, publicPointPath);
        endDay.setTask(lastTaskName);
        endDay.setDescription("end day");
        endDay.setTimeStamp(currentEndDayHourTask.getTimeStamp()); //TODO check if now has the same day
        endDay.write();

        tasks.read();
        setDayIdLast();
        setCurrentTasks();
    }

    @Override
    public String getSummary() {
        StringBuffer summary = new StringBuffer();

        LocalTime workTime = getWorkTime();
        long totalWorkTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), workTime);
        Set<String> tasks = currentWorkHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentWorkHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalWorkTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%.2f", taskDuration / 3600).replaceAll(",", "."));
            summary.append("h | ");
            summary.append(task);
            summary.append("\n");
        }

        // break
        summary.append("\n");
        LocalTime breakTime = getBreakTime();
        totalWorkTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), breakTime);
        tasks = currentBreakHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentBreakHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalWorkTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%.2f", (double) taskDuration / 3600).replaceAll(",", "."));
            summary.append("h | ");
            summary.append(task);
            summary.append("\n");
        }
        summary.replace(summary.length() - 1, summary.length(), "");

        return summary.toString();
    }

    @Override
    public String getSummaryInHourMinuteStyle() {
        StringBuffer summary = new StringBuffer();

        LocalTime workTime = getWorkTime();
        long totalWorkTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), workTime);
        Set<String> tasks = currentWorkHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentWorkHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalWorkTime) * 100;
            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%2d", (int) taskDuration / 3600));
            summary.append(":");
            int minutes = ((int) taskDuration / 60) % 60;
            if (minutes <= 9) {
                summary.append("0");
                summary.append(String.format("%1d", minutes));
            } else {
                summary.append(String.format("%2d", minutes));
            }
            summary.append(" | ");
            summary.append(task);
            summary.append("\n");
        }

        // break
        summary.append("\n");
        LocalTime breakTime = getBreakTime();
        totalWorkTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), breakTime);
        tasks = currentBreakHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentBreakHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalWorkTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%2d", (int) taskDuration / 3600));
            summary.append(":");
            int minutes = ((int) taskDuration / 60) % 60;
            if (minutes <= 9) {
                summary.append("0");
                summary.append(String.format("%1d", minutes));
            } else {
                summary.append(String.format("%2d", minutes));
                //FIXME later: this is not a perfect solution.
            }
            summary.append(" | ");
            summary.append(task);
            summary.append("\n");
        }
        summary.replace(summary.length() - 1, summary.length(), "");

        return summary.toString();
    }

    @Override
    public String getSummaryDetails() {
        //TODO fix this later
        StringBuffer summary = new StringBuffer();

        LocalTime workTime = getWorkTime();
        long totalWorkTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), workTime);
        Set<String> tasks = currentWorkHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentWorkHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalWorkTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%.2f", taskDuration / 3600).replaceAll(",", "."));
            summary.append("h | ");
            summary.append(task);
            summary.append("\n");
            for (HourTask hourTask : currentWorkHourTasksSummary2.keySet()) {
                if (hourTask.getTask().equals(task)) {
                    Double seconds = currentWorkHourTasksSummary2.get(hourTask);
                    double percentage = (seconds / taskDuration) * 100;
                    double hour = seconds / 3600;
                    summary.append(String.format("%3.0f", percentage));
                    summary.append("% | ");
                    summary.append(String.format("%2.2fh", hour).replaceAll(",", "."));
                    summary.append(String.format(" | %s\n", hourTask.getDescription()));
                }
            }
            summary.append("\n");
        }

        // break
        summary.append("\n");
        LocalTime breakTime = getBreakTime();
        long totalBrakeTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), breakTime);
        tasks = currentBreakHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentBreakHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalBrakeTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%.2f", taskDuration / 3600).replaceAll(",", "."));
            summary.append("h | ");
            summary.append(task);
            summary.append("\n");
            for (HourTask hourTask : currentBreakHourTasksSummary2.keySet()) {
                if (hourTask.getTask().equals(task)) {
                    Double seconds = currentBreakHourTasksSummary2.get(hourTask);
                    double percentage = (seconds / taskDuration) * 100;
                    double hour = seconds / 3600;
                    summary.append(String.format("%3.0f", percentage));
                    summary.append("% | ");
                    summary.append(String.format("%2.2fh", hour).replaceAll(",", "."));
                    summary.append(String.format(" | %s\n", hourTask.getDescription()));
                }
            }
            summary.append("\n");
        }
        summary.replace(summary.length() - 2, summary.length(), "");

        return summary.toString();
    }

    @Override
    public String getSummaryDetailsInHourMinuteStyle() {
        //TODO fix this later
        StringBuffer summary = new StringBuffer();

        LocalTime workTime = getWorkTime();
        long totalWorkTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), workTime);
        Set<String> tasks = currentWorkHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentWorkHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalWorkTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%2d", (int) taskDuration / 3600));
            summary.append(":");

            int minutes = ((int) taskDuration / 60) % 60;
            if (minutes <= 9) {
                summary.append("0");
                summary.append(String.format("%1d", minutes));
            } else {
                summary.append(String.format("%2d", minutes));
            }
            summary.append(" | ");
            summary.append(task);
            summary.append("\n");
            for (HourTask hourTask : currentWorkHourTasksSummary2.keySet()) {
                if (hourTask.getTask().equals(task)) {
                    double seconds = currentWorkHourTasksSummary2.get(hourTask);
                    double percentage = (seconds / taskDuration) * 100;
                    summary.append(String.format("%3.0f", percentage));

                    summary.append("% | ");
                    summary.append(String.format("%2d", (int) seconds / 3600));
                    summary.append(":");
                    minutes = ((int) seconds / 60) % 60;
                    if (minutes <= 9) {
                        summary.append("0");
                        summary.append(String.format("%1d", minutes));
                    } else {
                        summary.append(String.format("%2d", minutes));
                    }
                    summary.append(String.format(" | %s\n", hourTask.getDescription()));
                }
            }
            summary.append("\n");
        }

        // break
        summary.append("\n");
        LocalTime breakTime = getBreakTime();
        long totalBrakeTime = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00", TimestampFormatter.getTime()), breakTime);
        tasks = currentBreakHourTasksSummary.keySet();

        for (String task : tasks) {
            List<String> taskContent = currentBreakHourTasksSummary.get(task);
            double taskDuration = Long.parseLong(taskContent.get(0));
            double taskPercentage = (taskDuration / totalBrakeTime) * 100;

            summary.append(String.format("%3.0f", taskPercentage));
            summary.append("% | ");
            summary.append(String.format("%2d", (int) taskDuration / 3600));
            summary.append(":");

            int minutes = ((int) taskDuration / 60) % 60;
            if (minutes <= 9) {
                summary.append("0");
                summary.append(String.format("%1d", minutes));
            } else {
                summary.append(String.format("%2d", minutes));
            }
            summary.append(" | ");
            summary.append(task);
            summary.append("\n");
            for (HourTask hourTask : currentBreakHourTasksSummary2.keySet()) {
                if (hourTask.getTask().equals(task)) {
                    double seconds = currentBreakHourTasksSummary2.get(hourTask);
                    double percentage = (seconds / taskDuration) * 100;
                    summary.append(String.format("%3.0f", percentage));
                    summary.append("% | ");
                    summary.append(String.format("%2d", (int) seconds / 3600));
                    summary.append(":");
                    minutes = ((int) seconds / 60) % 60;
                    if (minutes <= 9) {
                        summary.append("0");
                        summary.append(String.format("%1d", minutes));
                    } else {
                        summary.append(String.format("%2d", minutes));
                    }
                    summary.append(String.format(" | %s\n", hourTask.getDescription()));
                }
            }
            summary.append("\n");
        }
        summary.replace(summary.length() - 2, summary.length(), "");

        return summary.toString();
    }

    @Override
    public int getHowManyDaysAreInPublicPoint() {
        List<LocalDate> days = tasks.getDays();
        int howManyDays = days.size();
        if (howManyDays == 1 && currentHourTasks.size() > 1) {
            return 1;
        } else if (howManyDays == 1 && currentHourTasks.size() == 1) {
            return 0;
        } else {
            return howManyDays;
        }
    }

    @Override
    public HourTask getLatestHourTask() {
        return currentHourTasks.get(currentHourTasks.size() - 1);
    }

    @Override
    public HourTask getBeforeLatestHourTask() {
        return currentHourTasks.get(currentHourTasks.size() - 2);
    }

    @Override
    public int getWorkWeek() {
        HourTask latestHourTask = getLatestHourTask();
        LocalDateTime timeStamp = latestHourTask.getTimeStamp();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(timeStamp.toLocalDate().toString());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.WEEK_OF_YEAR);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 99;
    }

    @Override
    public List<String> getCurrentDayTasks() {
        Set<String> tasksOrderedAndUnique = new LinkedHashSet<>();

        for(HourTask hourTask : currentWorkHourTasks) {
            String task = hourTask.getTask();
            tasksOrderedAndUnique.add(task);
        }
        return tasksOrderedAndUnique.stream().collect(Collectors.toList());
    }

    @Override
    public String getPublicPointPath() {
        return publicPointPath;
    }
}