/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask;

import com.michalrys.hours.tools.JSONMySimplestParser;
import com.michalrys.hours.tools.TimestampFormatter;
import org.json.JSONObject;

import java.io.*;
import java.time.LocalDateTime;

public class HourTaskImpl implements HourTask {
    public static final String KEY_TYPE = "type";
    public static final String KEY_TIME_STAMP = "timeStamp";
    public static final String KEY_TASK = "task";
    public static final String KEY_DESCRIPTION = "description";
    public static final String FILE_NAME_VALIDATION_REGEX = "^(HourTask_)[0-9_-]{10,}[0-9](.json)$";

    private final TaskType type;
    private final String publicPointPath;
    private LocalDateTime timeStamp;
    private String task;
    private String description;

    public HourTaskImpl(TaskType type, String publicPointPath) {
        this.type = type;
        this.publicPointPath = publicPointPath;
    }

    @Override
    public TaskType getType() {
        return type;
    }

    @Override
    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    @Override
    public void setTask(String task) {
        this.task = task;
    }

    @Override
    public String getTask() {
        return task;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void write() {
        File file = setFileToWrite();
        JSONObject json = setJsonToWrite();
        JSONMySimplestParser.write(file, json);
    }

    private File setFileToWrite() {
        if (timeStamp == null) {
            setTimeStamp(LocalDateTime.now());
        }
        String timeStampForFileName = this.timeStamp.format(TimestampFormatter.getDateTimeForFileName());
        File file = new File(publicPointPath + "/HourTask_" + timeStampForFileName + ".json");
        return file;
    }


    private JSONObject setJsonToWrite(){
        JSONObject json = new JSONObject();
        json.put(KEY_TYPE, type);
        json.put(KEY_TIME_STAMP, timeStamp.format(TimestampFormatter.getDateTime()));
        json.put(KEY_TASK, task);
        json.put(KEY_DESCRIPTION, description);
        return json;
    }

    @Override
    public String toString() {
        return type + "-" + timeStamp.format(TimestampFormatter.getDateTimeForFileName());
    }
}