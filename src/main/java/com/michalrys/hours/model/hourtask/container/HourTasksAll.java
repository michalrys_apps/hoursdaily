/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask.container;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.builder.HourTaskBuilderFromFile;
import com.michalrys.hours.model.hourtask.builder.HourTaskDummyWhenThereAreNoHourTasksInPublicPoint;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.tools.DataParser;
import com.michalrys.hours.tools.LocalDateDescendingComparator;

import java.io.File;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class HourTasksAll implements HourTaskContainer {
    private final File publicPoint;
    private Map<LocalDate, List<HourTask>> hourTasks = new HashMap<>();

    public HourTasksAll(String publicPointPath) {
        this.publicPoint = new File(publicPointPath);
    }

    @Override
    public void read() {
        File[] files = getFiles();
        if (files.length == 0) {
            setHourTasksAsDummyForNoTasksInPublicPoint();
        } else {
            setHourTasks(files);
        }
    }

    private File[] getFiles() {
        return publicPoint.listFiles((dir, name) -> name.matches(HourTaskImpl.FILE_NAME_VALIDATION_REGEX));
    }

    private void setHourTasksAsDummyForNoTasksInPublicPoint() {
        HourTask hourTaskNoTasks = new HourTaskDummyWhenThereAreNoHourTasksInPublicPoint(publicPoint).build();
        hourTasks.put(LocalDate.now(), Arrays.asList(hourTaskNoTasks));
    }

    private void setHourTasks(File[] files) {
        for (File file : files) {
            HourTask hourTask = new HourTaskBuilderFromFile(file).build();
            LocalDate dayKey = DataParser.extractDateFromHourTaskFile(file);

            List<HourTask> hourTasksFromDay = hourTasks.getOrDefault(dayKey, new ArrayList<>());
            hourTasksFromDay.add(hourTask);
            hourTasks.put(dayKey, hourTasksFromDay);
        }
    }

    @Override
    public List<LocalDate> getDays() {
        Set<LocalDate> days = hourTasks.keySet();
        List<LocalDate> daysSortedInDescendingOrder = days.stream()
                .sorted(LocalDateDescendingComparator::compareTo)
                .collect(Collectors.toList());
        return daysSortedInDescendingOrder;
    }

    @Override
    public List<HourTask> get(LocalDate date) {
        return hourTasks.get(date);
    }
}