/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Archivizer {
    private static final String PATH_ARCHIVE = "/ARCHIVE";
    private static final String PATH_WW = "/WW";
    private static final String FOLDER_SEPARATOR = "/";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String HOUR_TASK_FILE_PATTERN =
            "HourTask_20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9].json";
    private static final String DEFAULT_WRONG_WORK_WEEK = "99";
    private final String publicPointPath;
    private String archivePath;

    public Archivizer(String publicPointPath) {
        this.publicPointPath = publicPointPath;
        setArchive();
    }

    private void setArchive() {
        archivePath = publicPointPath + PATH_ARCHIVE;
    }

    public void move(int fromWW, int toWW) {
        File publicPoint = new File(publicPointPath);
        File[] files = publicPoint.listFiles();
        List<File> hourTasksOnly = getHourTasksOnly(files);

        for (File file : hourTasksOnly) {
            String name = file.getName();
            String workWeek = getWorkWeek(name);
            if (Integer.parseInt(workWeek) < fromWW || Integer.parseInt(workWeek) > toWW) {
                continue;
            }
            String year = getYear(name);
            File archiveWorkWeek = new File(archivePath + FOLDER_SEPARATOR + year + PATH_WW + workWeek);

            if (!archiveWorkWeek.exists()) {
                archiveWorkWeek.mkdirs();
            }

            try {
                Files.move(
                        Paths.get(file.getAbsolutePath()),
                        Paths.get(archiveWorkWeek.getAbsolutePath() + FOLDER_SEPARATOR + file.getName()),
                        StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void moveAll() {
        move(1, 99);
    }

    private String getYear(String name) {
        String[] splitted = name.split("_");
        String datePart = splitted[1];
        LocalDate date = LocalDate.parse(datePart, DateTimeFormatter.ofPattern(DATE_PATTERN));
        return String.valueOf(date.getYear());
    }

    private String getWorkWeek(String name) {
        String[] splitted = name.split("_");
        String datePart = splitted[1];
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        try {
            Date date = dateFormat.parse(datePart);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int workWeek = calendar.get(Calendar.WEEK_OF_YEAR);
            if (workWeek <= 9) {
                return "0" + workWeek;
            } else {
                return String.valueOf(workWeek);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return DEFAULT_WRONG_WORK_WEEK;
    }

    private List<File> getHourTasksOnly(File[] files) {
        List<File> hourTaskFilesOnly = new ArrayList<>();
        for (File file : files) {
            String name = file.getName();
            if (name.matches(HOUR_TASK_FILE_PATTERN)) {
                hourTaskFilesOnly.add(file);
            }
        }
        return hourTaskFilesOnly;
    }
}