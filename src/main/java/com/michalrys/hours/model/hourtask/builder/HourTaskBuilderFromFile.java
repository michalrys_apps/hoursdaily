/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask.builder;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.tools.JSONMySimplestParser;
import com.michalrys.hours.tools.TimestampFormatter;
import org.json.JSONObject;

import java.io.File;
import java.time.LocalDateTime;

public class HourTaskBuilderFromFile implements HourTaskBuilder {
    private final File file;
    private final File publicPoint;

    public HourTaskBuilderFromFile(File file) {
        this.file = file;
        publicPoint = file.getParentFile();
    }

    @Override
    public HourTask build() {
        JSONObject readFile = JSONMySimplestParser.read(file);
        String type = readFile.getString(HourTaskImpl.KEY_TYPE);
        TaskType taskType = TaskType.valueOfOrDefaultAsWork(type);

        HourTask hourTask = new HourTaskImpl(taskType, publicPoint.getAbsolutePath());

        String task = readFile.getString(HourTaskImpl.KEY_TASK);
        hourTask.setTask(task);

        String description = readFile.getString(HourTaskImpl.KEY_DESCRIPTION);
        hourTask.setDescription(description);

        String timeStamp = readFile.getString(HourTaskImpl.KEY_TIME_STAMP);
        LocalDateTime timeStampToSave = LocalDateTime.parse(timeStamp, TimestampFormatter.getDateTime());
        hourTask.setTimeStamp(timeStampToSave);

        return hourTask;
    }
}