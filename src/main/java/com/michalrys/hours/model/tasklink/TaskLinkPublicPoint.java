/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.tasklink;

import com.michalrys.hours.tools.JSONMySimplestParser;
import org.json.JSONObject;

import java.io.File;
import java.util.*;

public class TaskLinkPublicPoint implements TaskLinkContainer {
    private static final String TASKS_FILE_NAME = "TaskLink.json";
    private static final String LINK_FOR_NOT_EXISTING_TASK_IN_TASKLINKFILE = new File("c:/").getAbsolutePath();
    private final File publicPoint;
    private File taskLinkFile;
    private Map<String, String> taskLink;

    public TaskLinkPublicPoint(String publicPointPath) {
        publicPoint = new File(publicPointPath);
        setTaskLinkFile();
        createTaskLinkFileIfNotExist();
        readTaskLinkFile();
    }

    private void setTaskLinkFile() {
        taskLinkFile = new File(publicPoint.getAbsolutePath() + "/" + TASKS_FILE_NAME);
    }

    private void createTaskLinkFileIfNotExist() {
        if(!taskLinkFile.exists()) {
            JSONObject jsonObject = new JSONObject();
            JSONMySimplestParser.write(taskLinkFile, jsonObject);
        }
    }

    private void readTaskLinkFile() {
        taskLink = new LinkedHashMap<>();
        JSONObject read = JSONMySimplestParser.read(taskLinkFile);
        if(read.isEmpty()) {
            return;
        }
        Set<String> tasks = read.keySet();
        for(String task : tasks) {
            taskLink.put(task, read.getString(task));
        }
    }

    private void writeTaskLinkFile() {
        JSONObject json = new JSONObject();
        for(String task : taskLink.keySet()) {
            json.put(task, taskLink.get(task));
        }
        JSONMySimplestParser.write(taskLinkFile, json);
    }

    @Override
    public String getPublicPointFilePath() {
        return publicPoint.getAbsolutePath();
    }

    @Override
    public String getTaskLinkFilePath() {
        return taskLinkFile.getAbsolutePath();
    }

    @Override
    public Map<String, String> get(List<String> listOfTasksNotExistingInTaskLinkFile) {
        updateTaskListForGivenTaskList(listOfTasksNotExistingInTaskLinkFile);
        return getTasksLinksForGivenTasksOnly(listOfTasksNotExistingInTaskLinkFile);
    }

    private void updateTaskListForGivenTaskList(List<String> listOfTasksNotExistingInTaskLinkFile) {
        boolean taskLinkWasChanged = false;
        for(String task : listOfTasksNotExistingInTaskLinkFile) {
            if(!taskLink.containsKey(task)) {
                taskLink.put(task, LINK_FOR_NOT_EXISTING_TASK_IN_TASKLINKFILE);
                taskLinkWasChanged = true;
            }
        }
        if(taskLinkWasChanged) {
            writeTaskLinkFile();
        }
    }

    private Map<String, String> getTasksLinksForGivenTasksOnly(List<String> listOfTasksNotExistingInTaskLinkFile) {
        Map<String, String> taskLinkForGivenTasksOnly = new LinkedHashMap<>();
        for(String task : listOfTasksNotExistingInTaskLinkFile) {
            taskLinkForGivenTasksOnly.put(task, taskLink.get(task));
        }
        return taskLinkForGivenTasksOnly;
    }

    @Override
    public void update(String task, String link) {
        taskLink.put(task, link);
        writeTaskLinkFile();
    }

    @Override
    public Map<String, String> getAllWithoutLink() {
        Map<String, String> taskLinksWithoutLink = new TreeMap<>();
        for(String task: taskLink.keySet()) {
            String link = taskLink.get(task);
            if(link.equals("c:\\") || link.equals("c:/")) {
                taskLinksWithoutLink.put(task, link);
            }
        }
        return taskLinksWithoutLink;
    }
}