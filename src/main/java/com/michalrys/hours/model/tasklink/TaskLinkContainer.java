package com.michalrys.hours.model.tasklink;

import java.util.List;
import java.util.Map;

public interface TaskLinkContainer {
    String getPublicPointFilePath();

    String getTaskLinkFilePath();

    Map<String, String> get(List<String> listOfTasksNotExistingInTaskLinkFile);

    void update(String task, String link);

    Map<String, String> getAllWithoutLink();
}
