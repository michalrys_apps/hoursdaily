package com.michalrys.hours.model.excel;

import java.io.File;

public interface ExcelFile {
    void setUpStyles();

    void setDataFromHoursSummary();

    void setUpData();

    void write(File file);
}
