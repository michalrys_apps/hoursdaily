/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.tools.JSONMySimplestParser;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import java.io.*;
import java.util.*;
import java.net.URI;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ExcelFileImpl implements ExcelFile {
    private HoursSummaryDTO hoursSummaryDTO;
    private static final String WORK_TYPE_OF_TASKS = "work";
    private static final String BREAK_TYPE_OF_TASKS = "break";

    private static final int CELL_ID_LEFT_MARGIN = 0;
    private static final int COLUMN_WIDHT_LEFT_MARGIN = 700;
    private static final int CELL_ID_HEADERS = 1;
    private static final int COLUMN_WIDHT_HEADERS = 6_000;
    private static final int CELL_ID_MONDAY = 2;
    private static final int CELL_ID_TUESDAY = 3;
    private static final int CELL_ID_WEDNESDAY = 4;
    private static final int CELL_ID_THURSDAY = 5;
    private static final int CELL_ID_FRIDAY = 6;
    private static final int CELL_ID_SATURDAY = 7;
    private static final int CELL_ID_SUNDAY = 8;
    private static final int COLUMN_WIDTH_DEFAULT = 2_350;
    private final int CELL_ID_SUM = 9;
    private final int CELL_ID_LAST_MARGIN = 20;

    //     wrong<<< |-0.25h| <<<correct<<< |8h| >>>warning>>> |+4h| >>>wrong
    private final double HOURS_EXPECTED_DIFFERENCE_WRONG_BELOW = -0.25;
    private final double HOURS_EXPECTED_DIFFERENCE_WRONG_ABOVE = 4.0;
    private final double HOURS_EXPECTED_DIFFERENCE_WARNING_BELOW = 4.0;
    private final double HOURS_EXPECTED_DIFFERENCE_WARNING_ABOVE = 0.0;

    private final HourTasksSummary htSummary;
    private ExcelStyles excelStyles;
    private Workbook workbook = new XSSFWorkbook();
    private CreationHelper workbookCreationHelper;
    private Sheet sheet = workbook.createSheet("Summary"); //this keep current sheet
    private Row row;   //this keep current row
    private Cell cell; //this keep current cell
    private int rowId; //this keep current row id
    private int rowIdHeader;
    private int rowIdDate;
    private int rowIdWeekDay;
    private int rowIdExpected;
    private int rowIdStart;
    private int rowIdEnd;
    private int rowIdDifference;
    private int rowIdBreak;
    private int rowIdTotal;
    private int rowIdWork;
    private int rowIdRemarks;
    private int rowIdTaskFirst;
    private int rowIdTaskLast;
    private int rowIdSubTaskFirst;
    private int rowIdSubTaskLast;
    private int rowIdSubBreakFirst;
    private int rowIdSubBreakLast;

    public ExcelFileImpl(HourTasksSummary hourTasksSummary) {
        this.htSummary = hourTasksSummary;
        workbookCreationHelper = workbook.getCreationHelper();
    }

    @Override
    public void setUpStyles() {
        excelStyles = new ExcelStyles(workbook);
    }

    @Override
    public void setDataFromHoursSummary() {
        hoursSummaryDTO = new HoursSummaryDTO(htSummary);
    }

    public void setUpData() {
        rowId = 0;
        setColumnsWidth();

        rowId = addEmptyRowSeparator(rowId);
        for (Integer workWeek : hoursSummaryDTO.getWorkWeeks()) {
            rowId = setTableHeaders(rowId);
            setTableDaysData(workWeek, hoursSummaryDTO.getDays(workWeek));
            rowId = setTableMainWorkTasksData(rowId, workWeek, hoursSummaryDTO.getMainTasks(WORK_TYPE_OF_TASKS, workWeek));
            rowId = setTableMainWorkTasksSum(rowId);
            int rowIdForStartingGroup = rowId;
            rowId = setTableDetailedWorkTasksData(rowId, workWeek, hoursSummaryDTO.getMainTasks(WORK_TYPE_OF_TASKS, workWeek));
            rowId = setTableDetailedWorkTasksSum(rowId);
            rowId = setTableDetailedBreakTasksData(rowId, workWeek, hoursSummaryDTO.getMainTasks(BREAK_TYPE_OF_TASKS, workWeek));
            rowId = setTableDetailedBreakTasksSum(rowId);
            int rowIdForEndingGroup = rowId;
            sheet.groupRow(rowIdForStartingGroup, rowIdForEndingGroup);
            sheet.setRowGroupCollapsed(rowIdForStartingGroup, true);
            rowId = addEmptyRowSeparator(rowId);
            rowId = addEmptyRowSeparator(rowId);
            rowId = addEmptyRowSeparator(rowId);
        }
        rowId = addEmptyRowSeparator(rowId);
        rowId = addEmptyRowSeparator(rowId);
        rowId = addEmptyRowSeparator(rowId);
        rowId = addEmptyRowSeparator(rowId);
        rowId = addEmptyRowSeparator(rowId);
    }

    // step 1
    private void setColumnsWidth() {
        sheet.setColumnWidth(CELL_ID_LEFT_MARGIN, COLUMN_WIDHT_LEFT_MARGIN);
        sheet.setColumnWidth(CELL_ID_HEADERS, COLUMN_WIDHT_HEADERS);
        for (int i = 2; i <= CELL_ID_LAST_MARGIN; i++) {
            sheet.setColumnWidth(i, COLUMN_WIDTH_DEFAULT);
        }
        sheet.setColumnWidth(CELL_ID_SUM + 1, 500);
    }


    // step 2
    private int setTableHeaders(int rowId) {
        rowId = setRowIdsForForCurrentWorkWeekTable(rowId);
        setHeaderForTable("daily hours summary");
        setHeadersForDays();

        return rowId;
    }

    private int setRowIdsForForCurrentWorkWeekTable(int rowId) {
        rowIdHeader = rowId++;
        rowIdDate = rowId++;
        rowIdWeekDay = rowId++;
        rowIdExpected = rowId++;
        rowIdStart = rowId++;
        rowIdEnd = rowId++;
        rowIdDifference = rowId++;
        rowIdBreak = rowId++;
        rowIdTotal = rowId++;
        rowIdWork = rowId++;
        rowIdRemarks = rowId++;
        //points at the next row below last header
        return rowId;
    }

    private void setHeaderForTable(String headerDescription) {
        row = sheet.createRow(rowIdHeader);
        setDefaultStyleForCurrentRow();

        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getHeaderBlack());
        cell.setCellValue(headerDescription);

        createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUM, excelStyles.getHeaderBlack());
    }

    private void createCellsAndSetStyleForCurrentRow(int cellIdStart, int cellIdEnd, CellStyle cellStyle) {
        for (int i = cellIdStart; i <= cellIdEnd; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyle);
        }
    }

    private void setHeadersForDays() {
        // row -> WW, DATE, ...
        row = sheet.createRow(rowIdDate);
        setDefaultStyleForCurrentRow();

        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getWorkWeek());

        createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUNDAY, excelStyles.getDaysDate());

        cell = row.createCell(CELL_ID_SUM);
        cell.setCellStyle(excelStyles.getSumForDays());

        // row -> TR, MON, TUE, ...
        row = sheet.createRow(rowIdWeekDay);
        setDefaultStyleForCurrentRow();
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getWorkWeekLabelOnly());
        cell.setCellValue("WW");

        for (int id = CELL_ID_MONDAY; id <= CELL_ID_SUM; id++) {
            cell = row.createCell(id);
            cell.setCellStyle(excelStyles.getWorkWeekLabelOnly());
            if (id == CELL_ID_MONDAY) cell.setCellValue("MON");
            if (id == CELL_ID_TUESDAY) cell.setCellValue("TUE");
            if (id == CELL_ID_WEDNESDAY) cell.setCellValue("WED");
            if (id == CELL_ID_THURSDAY) cell.setCellValue("THU");
            if (id == CELL_ID_FRIDAY) cell.setCellValue("FRI");
            if (id == CELL_ID_SATURDAY) cell.setCellValue("SAT");
            if (id == CELL_ID_SUNDAY) cell.setCellValue("SUN");
            if (id == CELL_ID_SUM) cell.setCellValue("SUM");
        }

        //row expected
        setCellsForDaysSummaryHourStyle(rowIdExpected, "expected", excelStyles.getDaysHours());

        //row start, end, difference
        setCellsForDaysSummaryTimeStyle(rowIdStart, "start");
        setCellsForDaysSummaryTimeStyle(rowIdEnd, "end");
        setCellsForDaysSummaryTimeStyle(rowIdDifference, "difference");
        setCellsForDaysSummaryTimeStyle(rowIdBreak, "break");

        //row total, work, break
        setCellsForDaysSummaryHourStyle(rowIdTotal, "total", excelStyles.getDaysHours());
        setCellsForDaysSummaryHourStyle(rowIdWork, "work", excelStyles.getDaysHours());

        //row remarks
        row = sheet.createRow(rowIdRemarks);
        setDefaultStyleForCurrentRow();
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getWorkWeekLabelOnly());
        cell.setCellValue("remarks");

        createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUM, excelStyles.getWorkWeekLabelOnly());
    }

    private void setDefaultStyleForCurrentRow() {
        row.setRowStyle(excelStyles.getVisualWhiteMarginsLeftShort());
    }

    private void setCellsForDaysSummaryTimeStyle(int rowId, String tagLabel) {
        row = sheet.createRow(rowId);
        setDefaultStyleForCurrentRow();
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getHeadersForHours());
        cell.setCellValue(tagLabel);
        for (int id = CELL_ID_MONDAY; id <= CELL_ID_SUNDAY; id++) {
            cell = row.createCell(id);
            cell.setCellStyle(excelStyles.getDaysHoursInHourStyle());
            if (tagLabel.equals("difference")) {
                cell.setCellStyle(excelStyles.getDaysHoursInHourStyleDifferenceOnly());
            }
        }
        cell = row.createCell(CELL_ID_SUM);
        cell.setCellStyle(excelStyles.getSumForHoursGray());
        cell.setCellValue("-");
    }

    private void setCellsForDaysSummaryHourStyle(int rowId, String tagLabel, CellStyle cellStyle) {
        //row total
        row = sheet.createRow(rowId);
        setDefaultStyleForCurrentRow();
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getHeadersForHours());
        cell.setCellValue(tagLabel);

        createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUNDAY, cellStyle);

        cell = row.createCell(CELL_ID_SUM);
        cell.setCellStyle(excelStyles.getSumForHoursGray());
        String formula = "SUM(C" + (rowId + 1) + ":I" + (rowId + 1 + ")");
        cell.setCellFormula(formula);

    }

    private int addEmptyRowSeparator(int rowId) {
        row = sheet.createRow(rowId);
        row.setRowStyle(excelStyles.getVisualWhiteMarginsLeftShort());

        return ++rowId;
    }


    // step 3
    private void setTableDaysData(Integer workWeek, List<DayDTO> days) {
        //set workweek number
        row = sheet.getRow(rowIdDate);
        cell = row.getCell(CELL_ID_HEADERS);
        cell.setCellValue(workWeek);

        //set amount of days in workweek
        cell = row.getCell(CELL_ID_SUM);
        cell.setCellValue(days.size());

        //set days
        for (DayDTO day : days) {
            LocalDate currentDay = day.getCurrentDay();
            DayOfWeek dayOfWeek = currentDay.getDayOfWeek();
            int dayCellId = 0;
            if (dayOfWeek.equals(DayOfWeek.MONDAY)) dayCellId = CELL_ID_MONDAY;
            if (dayOfWeek.equals(DayOfWeek.TUESDAY)) dayCellId = CELL_ID_TUESDAY;
            if (dayOfWeek.equals(DayOfWeek.WEDNESDAY)) dayCellId = CELL_ID_WEDNESDAY;
            if (dayOfWeek.equals(DayOfWeek.THURSDAY)) dayCellId = CELL_ID_THURSDAY;
            if (dayOfWeek.equals(DayOfWeek.FRIDAY)) dayCellId = CELL_ID_FRIDAY;
            if (dayOfWeek.equals(DayOfWeek.SATURDAY)) dayCellId = CELL_ID_SATURDAY;
            if (dayOfWeek.equals(DayOfWeek.SUNDAY)) dayCellId = CELL_ID_SUNDAY;

            //set date
            row = sheet.getRow(rowIdDate);
            cell = row.getCell(dayCellId);
            String currentDayDate = day.getCurrentDay().format(DateTimeFormatter.ofPattern("dd.MM"));
            cell.setCellValue(Double.valueOf(currentDayDate));

            //set expected - default value
            row = sheet.getRow(rowIdExpected);
            cell = row.getCell(dayCellId);
            cell.setCellValue(8);

            //set start in time
            row = sheet.getRow(rowIdStart);
            cell = row.getCell(dayCellId);
            String[] startTimeSplited = day.getStartTime().toString().split(":");
            cell.setCellValue(startTimeSplited[0] + ":" + startTimeSplited[1]);

            //set end in time
            row = sheet.getRow(rowIdEnd);
            cell = row.getCell(dayCellId);
            String[] endTimeSplited = day.getEndTime().toString().split(":");
            cell.setCellValue(endTimeSplited[0] + ":" + endTimeSplited[1]);

            //set difference in time
            row = sheet.getRow(rowIdDifference);
            cell = row.getCell(dayCellId);
            // day.getTotalTime() --> this is total hours
            String formula = getColumnLetter(dayCellId) + (rowId - 5) + "-" + getColumnLetter(dayCellId) + (rowId - 6);
            cell.setCellFormula(formula);

            //set break in time
            row = sheet.getRow(rowIdBreak);
            cell = row.getCell(dayCellId);
            double breakHoursInHours = day.getBreakHours();
            int brakeTimeHours = (int) breakHoursInHours;
            int brakeTimeMinutes = (int) Math.round((breakHoursInHours - (int) breakHoursInHours) * 60);
            if (brakeTimeMinutes == 60) {
                brakeTimeMinutes = 0;
            }
            String brakeHourMinutes = String.format("%02d:%02d", brakeTimeHours, brakeTimeMinutes);
            cell.setCellValue(brakeHourMinutes);

            //set total in hours
            row = sheet.getRow(rowIdTotal);
            cell = row.getCell(dayCellId);
            cell.setCellValue(day.getTotalHours());

            //set work in hours
            row = sheet.getRow(rowIdWork);
            cell = row.getCell(dayCellId);
            cell.setCellValue(day.getWorkHours());

            //verification working hours with expected hours
            row = sheet.getRow(rowIdExpected);
            cell = row.getCell(dayCellId);
            double expectedWorkingHours = cell.getNumericCellValue();

            row = sheet.getRow(rowIdWork);
            cell = row.getCell(dayCellId);
            double currentWorkingHours = cell.getNumericCellValue();
            double differenceInWorkingHours = currentWorkingHours - expectedWorkingHours;

            //     wrong<<< |-0.25h| <<<correct<<< |8h| >>>warning>>> |+4h| >>>wrong
            if (differenceInWorkingHours <= HOURS_EXPECTED_DIFFERENCE_WRONG_BELOW
                    || differenceInWorkingHours >= HOURS_EXPECTED_DIFFERENCE_WRONG_ABOVE) {
                cell.setCellStyle(excelStyles.getDaysHoursWrong());
            } else if (differenceInWorkingHours < HOURS_EXPECTED_DIFFERENCE_WARNING_BELOW &&
                    differenceInWorkingHours > HOURS_EXPECTED_DIFFERENCE_WARNING_ABOVE) {
                cell.setCellStyle(excelStyles.getDaysHoursWarning());
            } else {
                cell.setCellStyle(excelStyles.getDaysHoursCorrect());
            }
        }
    }


    // step 4
    private int setTableMainWorkTasksData(int rowId, Integer workWeek, List<HourTaskDTO> mainTasks) {
        int rowIdFirst = rowId;
        int rowIdLast;

        //read taskLinks.json for links
        String publicPointPath = htSummary.getPublicPointPath();
        File fileToRead = new File(publicPointPath + "\\TaskLink.json");
        JSONObject taskLinks = new JSONObject();
        if (fileToRead.exists()) {
            taskLinks = JSONMySimplestParser.read(fileToRead);
        } else {
            taskLinks.put("No tasklink file", "c:\\");
        }

        //set style and list of main tasks
        for (HourTaskDTO taskDTO : mainTasks) {
            row = sheet.createRow(rowId);
            setDefaultStyleForCurrentRow();

            cell = row.createCell(CELL_ID_HEADERS);
            cell.setCellStyle(excelStyles.getMainTasksLabel());
            cell.setCellValue(taskDTO.getName());

            createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUM, excelStyles.getMainTasksHours());

            cell = row.getCell(CELL_ID_SUM);
            String formula = "SUM(C" + (rowId + 1) + ":I" + (rowId + 1 + ")");
            cell.setCellFormula(formula);

            //set hyperlink to task link
            cell = row.createCell(CELL_ID_SUM + 2);
            cell.setCellStyle(excelStyles.getMainTasksLink());

            String link;
            try {
                link = taskLinks.get(taskDTO.getName()).toString();
            } catch (Exception e) {
                link = "c:\\";
            }
            cell.setCellValue(link);
            XSSFHyperlink hyperLink = (XSSFHyperlink) workbookCreationHelper.createHyperlink(HyperlinkType.FILE);
            URI uri = Paths.get(link).toUri();
            hyperLink.setAddress(uri.toString());
            cell.setHyperlink(hyperLink);

            //set summation formula
            cell = row.createCell(CELL_ID_SUM + 3);
            cell.setCellStyle(excelStyles.getMainTasksSummationFormula());
            formula = "\"" +
                    "(\"&IF(C" + (rowId + 1) + "=\"\",0,C" + (rowId + 1) + ")&\" + 0)" +
                    " + (\"&IF(D" + (rowId + 1) + "=\"\",0,D" + (rowId + 1) + ")&\" + 0)" +
                    " + (\"&IF(E" + (rowId + 1) + "=\"\",0,E" + (rowId + 1) + ")&\" + 0)" +
                    " + (\"&IF(F" + (rowId + 1) + "=\"\",0,F" + (rowId + 1) + ")&\" + 0)" +
                    " + (\"&IF(G" + (rowId + 1) + "=\"\",0,G" + (rowId + 1) + ")&\" + 0)\"";
            cell.setCellFormula(formula);

            rowId++;
        }
        rowIdLast = --rowId;

        //set hours for main tasks for each day in workweek
        List<DayDTO> days = hoursSummaryDTO.getDays(workWeek);

        for (DayDTO day : days) {
            List<HourTaskDTO> mainTasksPerDay = hoursSummaryDTO.getMainTasks(WORK_TYPE_OF_TASKS, workWeek, day);

            int dayCellId = getCellId(day);

            for (HourTaskDTO mainTaskInDayId : mainTasksPerDay) {
                int rowIdOffset = mainTasks.indexOf(mainTaskInDayId);

                row = sheet.getRow(rowIdFirst + rowIdOffset);
                cell = row.getCell(dayCellId);
                cell.setCellValue(mainTaskInDayId.getTime());
            }
        }

        rowIdTaskFirst = rowIdFirst;
        rowIdTaskLast = rowIdLast;
        return ++rowIdLast;
    }

    private int getCellId(DayDTO day) {
        int cellId = 0;
        DayOfWeek dayId = day.getCurrentDay().getDayOfWeek();
        if (dayId.equals(DayOfWeek.MONDAY)) cellId = CELL_ID_MONDAY;
        if (dayId.equals(DayOfWeek.TUESDAY)) cellId = CELL_ID_TUESDAY;
        if (dayId.equals(DayOfWeek.WEDNESDAY)) cellId = CELL_ID_WEDNESDAY;
        if (dayId.equals(DayOfWeek.THURSDAY)) cellId = CELL_ID_THURSDAY;
        if (dayId.equals(DayOfWeek.FRIDAY)) cellId = CELL_ID_FRIDAY;
        if (dayId.equals(DayOfWeek.SATURDAY)) cellId = CELL_ID_SATURDAY;
        if (dayId.equals(DayOfWeek.SUNDAY)) cellId = CELL_ID_SUNDAY;
        return cellId;
    }


    // step 5
    private int setTableMainWorkTasksSum(int rowId) {
        row = sheet.createRow(rowId);
        setDefaultStyleForCurrentRow();

        //set styles
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getSumOfTasksLabel());
        cell.setCellValue("SUM of tasks");

        //set sum formulas
        for (int id = CELL_ID_MONDAY; id <= CELL_ID_SUM; id++) {
            cell = row.createCell(id);
            cell.setCellStyle(excelStyles.getSumOfTasksValues());
            String columnLetter = getColumnLetter(id);

            String formula = "SUM(" + columnLetter + (rowIdTaskFirst + 1) + ":" + columnLetter + (rowIdTaskLast + 1) + ")";
            cell.setCellFormula(formula);
        }

        return ++rowId;
    }

    private String getColumnLetter(int cellId_FromMondayToSumIsRecognizedOnly) {
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_MONDAY) return "C";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_TUESDAY) return "D";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_WEDNESDAY) return "E";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_THURSDAY) return "F";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_FRIDAY) return "G";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_SATURDAY) return "H";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_SUNDAY) return "I";
        if (cellId_FromMondayToSumIsRecognizedOnly == CELL_ID_SUM) return "J";
        return "K";//default if wrong
    }


    // step 6
    private int setTableDetailedWorkTasksData(int rowId, int workWeek, List<HourTaskDTO> mainTasks) {
        //TODO refactor this code - extract 2x help methods from here, and reuse it in BreakTasks
        Map<String, Integer> rowIdForTask = new HashMap<>();
        rowIdSubTaskFirst = rowId;

        //set all main and subtasks in the table and set up styles
        List<HourTaskDTO> allMainAndSubTasks = hoursSummaryDTO.getMainAndSubTasks(WORK_TYPE_OF_TASKS, workWeek);

        int styleCounter = 1;
        HourTaskDTO currentMainTask = null;
        for (HourTaskDTO task : allMainAndSubTasks) {
            row = sheet.createRow(rowId);
            setDefaultStyleForCurrentRow();
            cell = row.createCell(CELL_ID_HEADERS);
            cell.setCellValue(task.getName());

            //set up styles
            boolean isMainTask = mainTasks.contains(task);
            if (isMainTask) {
                currentMainTask = task;
                styleCounter++;
                if (styleCounter % 2 == 0) {
                    cell.setCellStyle(excelStyles.getSubTasksLabelColorALeft());
                    createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUNDAY, excelStyles.getSubTasksLabelColorAMid());

                    cell = row.createCell(CELL_ID_SUM);
                    cell.setCellStyle(excelStyles.getSubTasksLabelColorARight());
                } else {
                    cell.setCellStyle(excelStyles.getSubTasksLabelColorBLeft());
                    createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUNDAY, excelStyles.getSubTasksLabelColorBMid());

                    cell = row.createCell(CELL_ID_SUM);
                    cell.setCellStyle(excelStyles.getSubTasksLabelColorBRight());
                }

            } else {
                if (styleCounter % 2 == 0) {
                    cell.setCellStyle(excelStyles.getSubTasksColorA());
                    createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUM, excelStyles.getSubTasksColorA());
                } else {
                    cell.setCellStyle(excelStyles.getSubTasksColorB());
                    createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUM, excelStyles.getSubTasksColorB());
                }
                String formula = "SUM(C" + (rowId + 1) + ":I" + (rowId + 1 + ")");
                cell.setCellFormula(formula);

                rowIdForTask.put(currentMainTask.getName() + "-" + task.getName(), rowId);
            }
            rowIdSubTaskLast = rowId;
            rowId++;
        }

        //set hours for each sub tasks
        List<DayDTO> days = hoursSummaryDTO.getDays(workWeek);
        for (DayDTO day : days) {
            List<HourTaskDTO> subTasks = hoursSummaryDTO.getMainAndSubTasks(WORK_TYPE_OF_TASKS, workWeek, day);
            for (HourTaskDTO task : subTasks) {
                if (mainTasks.contains(task)) {
                    //do not put hours for main tasks
                    currentMainTask = task;
                    continue;
                }
                int rowIdForSubTask = rowIdForTask.get(currentMainTask.getName() + "-" + task.getName());
                row = sheet.getRow(rowIdForSubTask);

                int cellIdForSubTask = getCellId(day);

                cell = row.getCell(cellIdForSubTask);
                cell.setCellValue(task.getTime());
            }
        }

        return rowId;
    }


    // step 7
    private int setTableDetailedWorkTasksSum(int rowId) {
        //TODO: extract more universal method valid also for BreakTasks
        row = sheet.createRow(rowId);
        setDefaultStyleForCurrentRow();

        //set styles
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getSumOfSubTasksLabel());
        cell.setCellValue("SUM of tasks details");

        //set sum formulas
        for (int id = CELL_ID_MONDAY; id <= CELL_ID_SUM; id++) {
            cell = row.createCell(id);
            cell.setCellStyle(excelStyles.getSumOfSubTasksValues());
            String columnLetter = getColumnLetter(id);

            String formula = "SUM(" + columnLetter
                    + (rowIdSubTaskFirst + 1) + ":" + columnLetter + (rowIdSubTaskLast + 1) + ")";
            cell.setCellFormula(formula);
        }

        return ++rowId;
    }


    // step 8
    private int setTableDetailedBreakTasksData(int rowId, int workWeek, List<HourTaskDTO> mainTasks) {
        //FIXME : this code is copied from step 7 and cell foreground division removed - do DRY
        Map<String, Integer> rowIdForTask = new HashMap<>();
        rowIdSubBreakFirst = rowId;

        //set all main and subtasks in the table and set up styles
        List<HourTaskDTO> allMainAndSubTasks = hoursSummaryDTO.getMainAndSubTasks(BREAK_TYPE_OF_TASKS, workWeek);

        HourTaskDTO currentMainTask = null;
        for (HourTaskDTO task : allMainAndSubTasks) {
            row = sheet.createRow(rowId);
            setDefaultStyleForCurrentRow();
            cell = row.createCell(CELL_ID_HEADERS);
            cell.setCellValue(task.getName());

            //set up styles
            boolean isMainTask = mainTasks.contains(task);
            if (isMainTask) {
                currentMainTask = task;

                cell.setCellStyle(excelStyles.getSubTasksLabelColorBLeft());
                createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUNDAY, excelStyles.getSubTasksLabelColorBMid());

                cell = row.createCell(CELL_ID_SUM);
                cell.setCellStyle(excelStyles.getSubTasksLabelColorBRight());
            } else {
                cell.setCellStyle(excelStyles.getSubTasksColorB());
                createCellsAndSetStyleForCurrentRow(CELL_ID_MONDAY, CELL_ID_SUM, excelStyles.getSubTasksColorB());

                String formula = "SUM(C" + (rowId + 1) + ":I" + (rowId + 1 + ")");
                cell.setCellFormula(formula);

                rowIdForTask.put(currentMainTask.getName() + "-" + task.getName(), rowId);
            }
            rowIdSubBreakLast = rowId;
            rowId++;
        }

        //set hours for each sub tasks
        List<DayDTO> days = hoursSummaryDTO.getDays(workWeek);
        for (DayDTO day : days) {
            List<HourTaskDTO> subTasks = hoursSummaryDTO.getMainAndSubTasks(BREAK_TYPE_OF_TASKS, workWeek, day);
            for (HourTaskDTO task : subTasks) {
                if (mainTasks.contains(task)) {
                    //do not put hours for main tasks
                    currentMainTask = task;
                    continue;
                }
                int rowIdForSubTask = rowIdForTask.get(currentMainTask.getName() + "-" + task.getName());
                row = sheet.getRow(rowIdForSubTask);

                int cellIdForSubTask = getCellId(day);

                cell = row.getCell(cellIdForSubTask);
                cell.setCellValue(task.getTime());
            }
        }

        return rowId;
    }


    // step 9
    private int setTableDetailedBreakTasksSum(int rowId) {
        //FIXME later: this code is the same like for sum sub tasks - do DRY
        row = sheet.createRow(rowId);
        setDefaultStyleForCurrentRow();

        //set styles
        cell = row.createCell(CELL_ID_HEADERS);
        cell.setCellStyle(excelStyles.getSumOfSubTasksLabel());
        cell.setCellValue("SUM of breaks details");

        //set sum formulas
        for (int id = CELL_ID_MONDAY; id <= CELL_ID_SUM; id++) {
            cell = row.createCell(id);
            cell.setCellStyle(excelStyles.getSumOfSubTasksValues());
            String columnLetter = getColumnLetter(id);

            String formula = "SUM(" + columnLetter
                    + (rowIdSubBreakFirst + 1) + ":" + columnLetter + (rowIdSubBreakLast + 1) + ")";
            cell.setCellFormula(formula);
        }

        return ++rowId;
    }

    @Override
    public void write(File file) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}