/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import java.time.LocalDate;
import java.time.LocalTime;

public class DayDTO implements Comparable<DayDTO>{
    private LocalDate currentDay;
    private LocalTime startTime;
    private LocalTime endTime;
    private LocalTime totalTime;
    private int workWeek;
    private double workHours;
    private double breakHours;
    private double totalHours;

    public void setCurrentDay(LocalDate currentDay) {
        this.currentDay = currentDay;
    }

    private void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    private void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    private void setTotalTime(LocalTime totalTime) {
        this.totalTime = totalTime;
    }

    public void setWorkWeek(int workWeek) {
        this.workWeek = workWeek;
    }

    public void setWorkHours(LocalTime workTime) {
        int hour = workTime.getHour();
        int minute = workTime.getMinute();
        this.workHours = (double) hour + minute/60.0;
    }

    public void setBreakHours(LocalTime breakTime) {
        int hour = breakTime.getHour();
        int minute = breakTime.getMinute();
        this.breakHours = (double) hour + minute/60.0;
    }

    public void setTotalHours() {
        this.totalHours = workHours + breakHours;
    }

    public LocalDate getCurrentDay() {
        return currentDay;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public LocalTime getTotalTime() {
        return totalTime;
    }

    public int getWorkWeek() {
        return workWeek;
    }

    public double getWorkHours() {
        return workHours;
    }

    public double getBreakHours() {
        return breakHours;
    }

    public double getTotalHours() {
        return totalHours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DayDTO dayDTO = (DayDTO) o;

        return currentDay != null ? currentDay.equals(dayDTO.currentDay) : dayDTO.currentDay == null;
    }

    @Override
    public int hashCode() {
        return currentDay != null ? currentDay.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DayDTO{" + currentDay + "}";
    }

    @Override
    public int compareTo(DayDTO other) {
        return this.currentDay.compareTo(other.getCurrentDay());
    }

    public static DayDTO getCurrent(HourTasksSummary hourTasksSummary) {
        DayDTO dayDTO = new DayDTO();
        dayDTO.setWorkWeek(hourTasksSummary.getWorkWeek());

        dayDTO.setCurrentDay(hourTasksSummary.getCurrentDay());
        dayDTO.setStartTime(hourTasksSummary.getStartTime());
        dayDTO.setEndTime(hourTasksSummary.getEndTime());
        dayDTO.setTotalTime(hourTasksSummary.getTotalTime());

        dayDTO.setWorkHours(hourTasksSummary.getWorkTime());
        dayDTO.setBreakHours(hourTasksSummary.getBreakTime());
        dayDTO.setTotalHours();

        return dayDTO;
    }
}
