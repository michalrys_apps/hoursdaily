/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.tools.TimestampFormatter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ExcelSummary implements FileSummary {
    private final HourTasksSummary hourTasksSummary;
    private File file;
    private ExcelFile excelFile;

    public ExcelSummary(HourTasksSummary hourTasksSummary) {
        this.hourTasksSummary = hourTasksSummary;
        setFile();
        setUpExcelFile();
    }

    private void setFile() {
        String publicPointPath = hourTasksSummary.getPublicPointPath();
        String dateTime = LocalDateTime.now().format(TimestampFormatter.getDateTimeForFileName());
        String username = System.getProperty("user.name");
        file = new File(publicPointPath
                + "/DailyHoursSummary_" + dateTime + "_" + username + ".xlsx");
    }

    private void setUpExcelFile() {
        excelFile = new ExcelFileImpl(hourTasksSummary);
        excelFile.setUpStyles();
        excelFile.setDataFromHoursSummary();
        excelFile.setUpData();
    }

    @Override
    public List<LocalDate> getDays() {
        List<LocalDate> days = new ArrayList<>();
        int howManyDaysAreInPublicPoint = hourTasksSummary.getHowManyDaysAreInPublicPoint();
        for (int i = 1; i <= howManyDaysAreInPublicPoint; i++) {
            hourTasksSummary.setNextDay();
            LocalDate currentDay = hourTasksSummary.getCurrentDay();
            days.add(currentDay);
        }
        return days;
    }

    @Override
    public void writeFile() {
        excelFile.write(file);
        showInfoWindow();
    }

    private void showInfoWindow() {
        String message = "<html>Hours summary was written in file:<br><br><b>" + file.getName() + "</html>";
        JFrame infoWindow = new JFrame("Excel...");
        JLabel messageInWindow = new JLabel(message, SwingConstants.CENTER);
        messageInWindow.setPreferredSize(new Dimension(500, 80));
        messageInWindow.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int button = e.getButton();
                if (button == MouseEvent.BUTTON1) {
                    try {
                        Runtime runtime = Runtime.getRuntime();
                        runtime.exec(new String[]{"cmd.exe", "/c", "start", file.getAbsolutePath()});
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                } else {
                    try {
                        Runtime runtime = Runtime.getRuntime();
                        runtime.exec(new String[]{"cmd.exe", "/c", "start", file.getParent()});
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                }
                infoWindow.dispose();
            }
        });
        messageInWindow.setCursor(new Cursor(Cursor.HAND_CURSOR));
        messageInWindow.setToolTipText("<html>" +
                "[<b>LEFT MOUSE BUTTON</b>] = open file (but could not work)" +
                "<br>[<b>RIGHT MOUSE BUTTON</b>] = open folder" +
                "<br><i><font size = -2>this window will be closed after 20 seconds</html>");
        infoWindow.getContentPane().add(messageInWindow, BorderLayout.CENTER);
        infoWindow.setLocation(300, 50);
        infoWindow.setAlwaysOnTop(true);
        infoWindow.pack();
        infoWindow.setVisible(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Consumer<Integer> autoClose = t -> {
                    for (int i = t; i >= 1; i--) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        infoWindow.setTitle("Excel... " + i);
                    }
                };
                autoClose.accept(20);
                infoWindow.dispose();
            }
        }).start();
    }
}