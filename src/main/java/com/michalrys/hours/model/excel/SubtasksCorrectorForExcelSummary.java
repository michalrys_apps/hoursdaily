/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.tools.JSONMySimplestParser;
import org.json.JSONObject;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubtasksCorrectorForExcelSummary {
    private final String publicPointPath;
    private final Set<String> mainTasks = new HashSet<>();
    private final Set<String> subTasks = new HashSet<>();
    private final Map<String, List<File>> subTasksAndFiles = new HashMap<>();

    public SubtasksCorrectorForExcelSummary(String publicPointPath) {
        this.publicPointPath = publicPointPath;
        setDataFromHourTaskFiles();
    }

    private void setDataFromHourTaskFiles() {
        File filePublicPoint = new File(publicPointPath);
        File[] files = filePublicPoint.listFiles();
        for (File file : files) {
            String fileName = file.getName();
            if (!isHourTask(fileName)) {
                continue;
            }
            JSONObject taskHour = JSONMySimplestParser.read(file);
            mainTasks.add(taskHour.getString("task"));

            String subTaskName = taskHour.getString("description");
            subTasks.add(subTaskName);

            List<File> listOfFiles = subTasksAndFiles.getOrDefault(subTaskName, new ArrayList<>());
            listOfFiles.add(file);
            subTasksAndFiles.put(subTaskName, listOfFiles);
        }
    }

    private boolean isHourTask(String fileName) {
        Pattern pattern = Pattern.compile(
                "HourTask_20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9].json");
        Matcher matcher = pattern.matcher(fileName);
        return matcher.matches();
    }

    public Set<String> getMainTasks() {
        return mainTasks;
    }

    public Set<String> getSubTasks() {
        return subTasks;
    }

    public void updateSubTasks() {
        List<String> subTasksWithWrongName = getSubTasksWithWrongName();
        if (subTasksWithWrongName.size() == 0) {
            return;
        }
        for (String subTask : subTasksWithWrongName) {
            List<File> files = subTasksAndFiles.get(subTask);
            for (File file : files) {
                JSONObject task = JSONMySimplestParser.read(file);
                task.put("description", subTask + "_");
                JSONMySimplestParser.write(file, task);
            }
        }
    }

    private List<String> getSubTasksWithWrongName() {
        List<String> subTasksWithWrongName = new ArrayList<>();
        for (String subtask : subTasks) {
            if (mainTasks.contains(subtask)) {
                subTasksWithWrongName.add(subtask);
            }
        }
        return subTasksWithWrongName;
    }
}