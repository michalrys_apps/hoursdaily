/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;

import java.util.*;

public class HoursSummaryDTO {
    private static final String WORK_TYPE_OF_TASKS = "work";
    private static final String BREAK_TYPE_OF_TASKS = "break";
    private final HourTasksSummary htSummary;
    private Map<Integer, Map<DayDTO, Map<String, Map<HourTaskDTO, Map<HourTaskDTO, Double>>>>> dto = new TreeMap<>();

    public HoursSummaryDTO(HourTasksSummary htSummary) {
        this.htSummary = htSummary;
        setUpValues();
    }

    public Map<Integer, Map<DayDTO, Map<String, Map<HourTaskDTO, Map<HourTaskDTO, Double>>>>> getDto() {
        return dto;
    }

    private void setUpValues() {
        int amountOfDays = htSummary.getHowManyDaysAreInPublicPoint();

        for (int day = 1; day <= amountOfDays; day++) {
            htSummary.setNextDay();
            int workWeek = htSummary.getWorkWeek();
            //conversion current day to dayDTO in order to keep all data related to this day, i.a. hours, times.
            DayDTO currentDay = DayDTO.getCurrent(htSummary);

            //reading summary details for further extracting tasks
            String summaryDetails = htSummary.getSummaryDetails();

            //extract work tasks and brake tasks from summary details
            Map<String, Map<HourTaskDTO, Map<HourTaskDTO, Double>>> workAndBrakeTasksToAdd = new TreeMap<>();
            workAndBrakeTasksToAdd.put(WORK_TYPE_OF_TASKS, HourTaskDTO.getAllWorkTasks(currentDay, summaryDetails));
            workAndBrakeTasksToAdd.put(BREAK_TYPE_OF_TASKS, HourTaskDTO.getAllBreakTasks(currentDay, summaryDetails));

            //get workweek container
            Map<DayDTO, Map<String, Map<HourTaskDTO, Map<HourTaskDTO, Double>>>> workWeekCollection = dto.get(workWeek);
            if (workWeekCollection == null) {
                //if there is no workweek so there is no day - so it will be added. Create day container.
                Map<DayDTO, Map<String, Map<HourTaskDTO, Map<HourTaskDTO, Double>>>> dayToAdd = new TreeMap<>();

                //add tasks to day
                dayToAdd.put(currentDay, workAndBrakeTasksToAdd);

                //add workweek with day to workweek container
                dto.put(workWeek, dayToAdd);
            } else {
                //there is such work week container, so get day container for this work week
                Map<DayDTO, Map<String, Map<HourTaskDTO, Map<HourTaskDTO, Double>>>> daysForWorkWeek = dto.get(workWeek);
                //and put current day in day container
                daysForWorkWeek.put(currentDay, workAndBrakeTasksToAdd);
            }
        }
    }

    public List<Integer> getWorkWeeks() {
        Set<Integer> integers = dto.keySet();
        List<Integer> workWeeks = new ArrayList<>(integers);
        workWeeks.sort(Integer::compareTo);
        return workWeeks;
    }

    public List<DayDTO> getDays() {
        List<DayDTO> days = new ArrayList<>();
        for (Integer workweek : dto.keySet()) {
            for (DayDTO day : dto.get(workweek).keySet()) {
                days.add(day);
            }
        }
        return days;
    }

    public List<DayDTO> getDays(int workWeek) {
        List<DayDTO> days = new ArrayList<>();
        for (DayDTO day : dto.get(workWeek).keySet()) {
            days.add(day);
        }
        return days;
    }

    /**
     * @param taskType put work or break. In case of any other taskType - work will be used.
     */
    public List<HourTaskDTO> getMainTasks(String taskType, int workWeek) {
        Set<HourTaskDTO> mainTasks = new TreeSet<>();

        if (!dto.containsKey(workWeek)) {
            System.out.println("No such work week in dto.");
            return null;
        }

        Set<DayDTO> days = dto.get(workWeek).keySet();

        for (DayDTO day : days) {

            Map<HourTaskDTO, Map<HourTaskDTO, Double>> brakeTasks;
            //get break or work tasks for current day
            if (taskType.equals(BREAK_TYPE_OF_TASKS)) {
                brakeTasks = dto.get(workWeek).get(day).get(BREAK_TYPE_OF_TASKS);
            } else {
                brakeTasks = dto.get(workWeek).get(day).get(WORK_TYPE_OF_TASKS);
            }
            //get main tasks
            mainTasks.addAll(brakeTasks.keySet());
        }

        List<HourTaskDTO> mainTasksForGivenWorkWeekSorted = new ArrayList<>(mainTasks);
        mainTasksForGivenWorkWeekSorted.sort(HourTaskDTO::compareTo);
        return mainTasksForGivenWorkWeekSorted;
    }

    /**
     * @param taskType put work or break. In case of any other taskType - work will be used.
     */
    public List<HourTaskDTO> getMainTasks(String taskType, int workWeek, DayDTO dayDTO) {
        Set<HourTaskDTO> mainTasks = new TreeSet<>();

        if (!dto.containsKey(workWeek)) {
            System.out.println("No such work week in dto.");
            return null;
        }

        Map<HourTaskDTO, Map<HourTaskDTO, Double>> brakeTasks;
        //get break or work tasks for current day
        if (taskType.equals(BREAK_TYPE_OF_TASKS)) {
            brakeTasks = dto.get(workWeek).get(dayDTO).get(BREAK_TYPE_OF_TASKS);
        } else {
            brakeTasks = dto.get(workWeek).get(dayDTO).get(WORK_TYPE_OF_TASKS);
        }
        //get main tasks
        mainTasks.addAll(brakeTasks.keySet());

        List<HourTaskDTO> mainTasksForGivenWorkWeekSorted = new ArrayList<>(mainTasks);
        mainTasksForGivenWorkWeekSorted.sort(HourTaskDTO::compareTo);
        return mainTasksForGivenWorkWeekSorted;
    }

    /**
     * @param taskType put work or break. In case of any other taskType - work will be used.
     */
    public List<HourTaskDTO> getMainAndSubTasks(String taskType, int workWeek) {
        List<HourTaskDTO> allTasks = new ArrayList<>();
        Map<HourTaskDTO, Map<HourTaskDTO, Double>> allTasksNotDuplicatedSorted = new TreeMap<>();

        if (!dto.containsKey(workWeek)) {
            System.out.println("No such work week in dto.");
            return null;
        }

        Set<DayDTO> days = dto.get(workWeek).keySet();

        for (DayDTO day : days) {
            Map<HourTaskDTO, Map<HourTaskDTO, Double>> tasks;

            if (taskType.equals(BREAK_TYPE_OF_TASKS)) {
                tasks = dto.get(workWeek).get(day).get(BREAK_TYPE_OF_TASKS);
            } else {
                tasks = dto.get(workWeek).get(day).get(WORK_TYPE_OF_TASKS);
            }

            for (HourTaskDTO mainTask : tasks.keySet()) {
                if(allTasksNotDuplicatedSorted.containsKey(mainTask)) {
                    Map<HourTaskDTO, Double> subTasksInResults = allTasksNotDuplicatedSorted.get(mainTask);
                    Map<HourTaskDTO, Double> subTasksToAdd = tasks.get(mainTask);
                    subTasksInResults.putAll(subTasksToAdd);
                } else {
                    allTasksNotDuplicatedSorted.put(mainTask, new TreeMap<>(tasks.get(mainTask)));
                }
            }
        }

        //get list of main task + sub tasks + main task + sub tasks + so on
        for(HourTaskDTO mainTask : allTasksNotDuplicatedSorted.keySet()) {
            //add main task firstly
            allTasks.add(mainTask);
            Map<HourTaskDTO, Double> subTasks = allTasksNotDuplicatedSorted.get(mainTask);
            //because main task is also included in subtasks, it shall be removed. Subtasks are sorted.
            subTasks.remove(mainTask);
            allTasks.addAll(subTasks.keySet());
        }

        return allTasks;
    }


    /**
     * @param taskType put work or break. In case of any other taskType - work will be used.
     */
    public List<HourTaskDTO> getMainAndSubTasks(String taskType, int workWeek, DayDTO dayDTO) {
        List<HourTaskDTO> allTasks = new ArrayList<>();

        if (!dto.containsKey(workWeek)) {
            System.out.println("No such work week in dto.");
            return null;
        }

        Map<HourTaskDTO, Map<HourTaskDTO, Double>> tasks;

        if (taskType.equals(BREAK_TYPE_OF_TASKS)) {
            tasks = dto.get(workWeek).get(dayDTO).get(BREAK_TYPE_OF_TASKS);
        } else {
            tasks = dto.get(workWeek).get(dayDTO).get(WORK_TYPE_OF_TASKS);
        }

        for (HourTaskDTO mainTask : tasks.keySet()) {
            //add main task firstly
            allTasks.add(mainTask);
            Map<HourTaskDTO, Double> subStasks = tasks.get(mainTask);
            //because main task is also included in subtasks, it shall be removed. Subtasks are sorted.
            subStasks.remove(mainTask);
            allTasks.addAll(subStasks.keySet());
        }

        return allTasks;
    }


    @Override
    public String toString() {
        return dto.toString();
    }
}