/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;


import java.awt.Color;

public class ExcelStyles {
    private final Workbook workbook;
    private CellStyle visualWhiteMarginsLeftShort;
    private CellStyle headerBlack;
    private CellStyle workWeek;
    private CellStyle workWeekLabelOnly;
    private CellStyle sumOfTasksLabel;
    private CellStyle sumOfTasksValues;
    private CellStyle sumOfSubTasksLabel;
    private CellStyle sumOfSubTasksValues;
    private CellStyle mainTasksLabel;
    private CellStyle subTasksLabelColorALeft;
    private CellStyle subTasksLabelColorAMid;
    private CellStyle subTasksLabelColorARight;
    private CellStyle subTasksColorA;
    private CellStyle subTasksColorB;
    private CellStyle subTasksLabelColorBLeft;
    private CellStyle subTasksLabelColorBMid;
    private CellStyle subTasksLabelColorBRight;
    private CellStyle mainTasksHours;
    private CellStyle mainTasksLink;
    private CellStyle mainTasksSummationFormula;
    private CellStyle daysDate;
    private CellStyle daysHours;
    private CellStyle daysHoursBrakeOnly;
    private CellStyle daysHoursWrong;
    private CellStyle daysHoursCorrect;
    private CellStyle daysHoursWarning;
    private CellStyle daysHoursInHourStyle;
    private CellStyle daysHoursInHourStyleDifferenceOnly;
    private CellStyle sumForDays;
    private CellStyle sumForHoursGray;
    private CellStyle sumForHoursGrayBreakOnly;
    private CellStyle headersForHours;
    private CreationHelper creationHelper;

    public ExcelStyles(Workbook workbook) {
        this.workbook = workbook;
        creationHelper = workbook.getCreationHelper();
        setUpStyles();
    }

    private void setUpStyles() {
        byte[] blackAsByte = {(byte) 0, (byte) 0, (byte) 0};
        Color black = new Color(0, 0, 0);

        byte[] whiteAsByte = {(byte) 255, (byte) 255, (byte) 255};
        Color white = new Color(255, 255, 255);

        byte[] gray2AsByte = {(byte) 217, (byte) 217, (byte) 217};
        Color gray2 = new Color(217, 217, 217);

        byte[] gray3AsByte = {(byte) 191, (byte) 191, (byte) 191};
        Color gray3 = new Color(191, 191, 191);

        byte[] gray4AsByte = {(byte) 166, (byte) 166, (byte) 166};
        Color gray4 = new Color(166, 166, 166);

        byte[] gray5AsByte = {(byte) 128, (byte) 128, (byte) 128};
        Color gray5 = new Color(128, 128, 128);

        byte[] gray7AsByte = {(byte) 89, (byte) 89, (byte) 89};
        Color gray7 = new Color(89, 89, 89);

        byte[] wrongAsByte = {(byte) 255, (byte) 51, (byte) 0};
        Color wrong = new Color(255, 51, 0);

        byte[] waringAsByte = {(byte) 255, (byte) 255, (byte) 0};
        Color warning = new Color(255, 255, 0);

        byte[] corrrectAsByte = {(byte) 0, (byte) 255, (byte) 0};
        Color correct = new Color(0, 255, 0);

        byte[] linkBlueAsByte = {(byte) 103, (byte) 124, (byte) 219};
        Color linkBlue = new Color(103, 124, 219);


        XSSFFont fontNormal = (XSSFFont) workbook.createFont();
        fontNormal.setFontName("Calibri");
        fontNormal.setFontHeightInPoints((short) 11);
        fontNormal.setBold(false);
        fontNormal.setColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));

        XSSFFont fontLink = (XSSFFont) workbook.createFont();
        fontLink.setFontName("Calibri");
        fontLink.setFontHeightInPoints((short) 11);
        fontLink.setBold(false);
        fontLink.setUnderline(FontUnderline.SINGLE);
        fontLink.setColor(new XSSFColor(linkBlueAsByte, new DefaultIndexedColorMap()));

        XSSFFont fontSummationFormula = (XSSFFont) workbook.createFont();
        fontSummationFormula.setFontName("Calibri");
        fontSummationFormula.setFontHeightInPoints((short) 11);
        fontSummationFormula.setBold(false);
        fontSummationFormula.setColor(new XSSFColor(gray3AsByte, new DefaultIndexedColorMap()));

        XSSFFont fontHeader = (XSSFFont) workbook.createFont();
        fontHeader.setFontName("Calibri");
        fontHeader.setFontHeightInPoints((short) 11);
        fontHeader.setBold(false);
        fontHeader.setColor(new XSSFColor(gray5AsByte, new DefaultIndexedColorMap()));

        XSSFFont fontWorkWeek = (XSSFFont) workbook.createFont();
        fontWorkWeek.setFontName("Calibri");
        fontWorkWeek.setFontHeightInPoints((short) 11);
        fontWorkWeek.setBold(false);
        fontWorkWeek.setColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));

        XSSFFont fontSumOfSubTasks = (XSSFFont) workbook.createFont();
        fontSumOfSubTasks.setFontName("Calibri");
        fontSumOfSubTasks.setFontHeightInPoints((short) 8);
        fontSumOfSubTasks.setBold(false);
        fontSumOfSubTasks.setColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));

        XSSFFont fontWorkWeekLabelOnly = (XSSFFont) workbook.createFont();
        fontWorkWeekLabelOnly.setFontName("Calibri");
        fontWorkWeekLabelOnly.setFontHeightInPoints((short) 11);
        fontWorkWeekLabelOnly.setBold(true);
        fontWorkWeekLabelOnly.setColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));

        XSSFFont fontSumOfSubTasksLabelOnly = (XSSFFont) workbook.createFont();
        fontSumOfSubTasksLabelOnly.setFontName("Calibri");
        fontSumOfSubTasksLabelOnly.setFontHeightInPoints((short) 8);
        fontSumOfSubTasksLabelOnly.setBold(true);
        fontSumOfSubTasksLabelOnly.setColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));

        XSSFFont fontForSubTasks = (XSSFFont) workbook.createFont();
        fontForSubTasks.setFontName("Calibri");
        fontForSubTasks.setFontHeightInPoints((short) 8);
        fontForSubTasks.setBold(false);
        fontForSubTasks.setColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));

        visualWhiteMarginsLeftShort = workbook.createCellStyle();
        visualWhiteMarginsLeftShort.setFont(fontNormal);
        ((XSSFCellStyle) visualWhiteMarginsLeftShort).setFillForegroundColor(new XSSFColor(whiteAsByte, new DefaultIndexedColorMap()));
        visualWhiteMarginsLeftShort.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        visualWhiteMarginsLeftShort.setAlignment(HorizontalAlignment.LEFT);
        visualWhiteMarginsLeftShort.setBorderBottom(BorderStyle.valueOf((short) 0));
        visualWhiteMarginsLeftShort.setBorderTop(BorderStyle.valueOf((short) 0));
        visualWhiteMarginsLeftShort.setBorderLeft(BorderStyle.valueOf((short) 0));
        visualWhiteMarginsLeftShort.setBorderRight(BorderStyle.valueOf((short) 0));

        headerBlack = workbook.createCellStyle();
        headerBlack.setFont(fontHeader);
        ((XSSFCellStyle) headerBlack).setFillForegroundColor(new XSSFColor(blackAsByte, new DefaultIndexedColorMap()));
        headerBlack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerBlack.setAlignment(HorizontalAlignment.LEFT);
        headerBlack.setBorderBottom(BorderStyle.valueOf((short) 1));
        headerBlack.setBorderTop(BorderStyle.valueOf((short) 1));
        headerBlack.setBorderLeft(BorderStyle.valueOf((short) 1));
        headerBlack.setBorderRight(BorderStyle.valueOf((short) 1));

        workWeek = workbook.createCellStyle();
        workWeek.setFont(fontWorkWeek);
        ((XSSFCellStyle) workWeek).setFillForegroundColor(new XSSFColor(gray3AsByte, new DefaultIndexedColorMap()));
        workWeek.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        workWeek.setAlignment(HorizontalAlignment.RIGHT);
        workWeek.setBorderBottom(BorderStyle.valueOf((short) 1));
        workWeek.setBorderTop(BorderStyle.valueOf((short) 1));
        workWeek.setBorderLeft(BorderStyle.valueOf((short) 1));
        workWeek.setBorderRight(BorderStyle.valueOf((short) 1));

        mainTasksLabel = workbook.createCellStyle();
        mainTasksLabel.setFont(fontWorkWeek);
        ((XSSFCellStyle) mainTasksLabel).setFillForegroundColor(new XSSFColor(whiteAsByte, new DefaultIndexedColorMap()));
        mainTasksLabel.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        mainTasksLabel.setAlignment(HorizontalAlignment.RIGHT);
        mainTasksLabel.setBorderBottom(BorderStyle.valueOf((short) 1));
        mainTasksLabel.setBorderTop(BorderStyle.valueOf((short) 1));
        mainTasksLabel.setBorderLeft(BorderStyle.valueOf((short) 1));
        mainTasksLabel.setBorderRight(BorderStyle.valueOf((short) 1));

        subTasksLabelColorALeft = workbook.createCellStyle();
        subTasksLabelColorALeft.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksLabelColorALeft).setFillForegroundColor(new XSSFColor(gray2AsByte, new DefaultIndexedColorMap()));
        subTasksLabelColorALeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksLabelColorALeft.setAlignment(HorizontalAlignment.RIGHT);
        subTasksLabelColorALeft.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksLabelColorALeft.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksLabelColorALeft.setBorderLeft(BorderStyle.valueOf((short) 1));
        subTasksLabelColorALeft.setBorderRight(BorderStyle.valueOf((short) 0));

        subTasksLabelColorAMid = workbook.createCellStyle();
        subTasksLabelColorAMid.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksLabelColorAMid).setFillForegroundColor(new XSSFColor(gray2AsByte, new DefaultIndexedColorMap()));
        subTasksLabelColorAMid.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksLabelColorAMid.setAlignment(HorizontalAlignment.RIGHT);
        subTasksLabelColorAMid.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksLabelColorAMid.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksLabelColorAMid.setBorderLeft(BorderStyle.valueOf((short) 0));
        subTasksLabelColorAMid.setBorderRight(BorderStyle.valueOf((short) 0));

        subTasksLabelColorARight = workbook.createCellStyle();
        subTasksLabelColorARight.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksLabelColorARight).setFillForegroundColor(new XSSFColor(gray2AsByte, new DefaultIndexedColorMap()));
        subTasksLabelColorARight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksLabelColorARight.setAlignment(HorizontalAlignment.RIGHT);
        subTasksLabelColorARight.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksLabelColorARight.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksLabelColorARight.setBorderLeft(BorderStyle.valueOf((short) 0));
        subTasksLabelColorARight.setBorderRight(BorderStyle.valueOf((short) 1));

        subTasksColorA = workbook.createCellStyle();
        subTasksColorA.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksColorA).setFillForegroundColor(new XSSFColor(gray2AsByte, new DefaultIndexedColorMap()));
        subTasksColorA.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksColorA.setAlignment(HorizontalAlignment.RIGHT);
        subTasksColorA.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksColorA.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksColorA.setBorderLeft(BorderStyle.valueOf((short) 1));
        subTasksColorA.setBorderRight(BorderStyle.valueOf((short) 1));
        subTasksColorA.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        subTasksColorB = workbook.createCellStyle();
        subTasksColorB.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksColorB).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        subTasksColorB.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksColorB.setAlignment(HorizontalAlignment.RIGHT);
        subTasksColorB.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksColorB.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksColorB.setBorderLeft(BorderStyle.valueOf((short) 1));
        subTasksColorB.setBorderRight(BorderStyle.valueOf((short) 1));
        subTasksColorB.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        subTasksLabelColorBLeft = workbook.createCellStyle();
        subTasksLabelColorBLeft.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksLabelColorBLeft).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        subTasksLabelColorBLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksLabelColorBLeft.setAlignment(HorizontalAlignment.RIGHT);
        subTasksLabelColorBLeft.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBLeft.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBLeft.setBorderLeft(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBLeft.setBorderRight(BorderStyle.valueOf((short) 0));

        subTasksLabelColorBMid = workbook.createCellStyle();
        subTasksLabelColorBMid.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksLabelColorBMid).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        subTasksLabelColorBMid.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksLabelColorBMid.setAlignment(HorizontalAlignment.RIGHT);
        subTasksLabelColorBMid.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBMid.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBMid.setBorderLeft(BorderStyle.valueOf((short) 0));
        subTasksLabelColorBMid.setBorderRight(BorderStyle.valueOf((short) 0));

        subTasksLabelColorBRight = workbook.createCellStyle();
        subTasksLabelColorBRight.setFont(fontForSubTasks);
        ((XSSFCellStyle) subTasksLabelColorBRight).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        subTasksLabelColorBRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        subTasksLabelColorBRight.setAlignment(HorizontalAlignment.RIGHT);
        subTasksLabelColorBRight.setBorderBottom(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBRight.setBorderTop(BorderStyle.valueOf((short) 1));
        subTasksLabelColorBRight.setBorderLeft(BorderStyle.valueOf((short) 0));
        subTasksLabelColorBRight.setBorderRight(BorderStyle.valueOf((short) 1));

        mainTasksHours = workbook.createCellStyle();
        mainTasksHours.setFont(fontWorkWeek);
        ((XSSFCellStyle) mainTasksHours).setFillForegroundColor(new XSSFColor(whiteAsByte, new DefaultIndexedColorMap()));
        mainTasksHours.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        mainTasksHours.setAlignment(HorizontalAlignment.RIGHT);
        mainTasksHours.setBorderBottom(BorderStyle.valueOf((short) 1));
        mainTasksHours.setBorderTop(BorderStyle.valueOf((short) 1));
        mainTasksHours.setBorderLeft(BorderStyle.valueOf((short) 1));
        mainTasksHours.setBorderRight(BorderStyle.valueOf((short) 1));
        mainTasksHours.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        mainTasksLink = workbook.createCellStyle();
        mainTasksLink.setFont(fontLink);
        ((XSSFCellStyle) mainTasksLink).setFillForegroundColor(new XSSFColor(whiteAsByte, new DefaultIndexedColorMap()));
        mainTasksLink.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        mainTasksLink.setAlignment(HorizontalAlignment.LEFT);
        mainTasksLink.setBorderBottom(BorderStyle.valueOf((short) 0));
        mainTasksLink.setBorderTop(BorderStyle.valueOf((short) 0));
        mainTasksLink.setBorderLeft(BorderStyle.valueOf((short) 0));
        mainTasksLink.setBorderRight(BorderStyle.valueOf((short) 0));

        mainTasksSummationFormula = workbook.createCellStyle();
        mainTasksSummationFormula.setFont(fontSummationFormula);
        ((XSSFCellStyle) mainTasksSummationFormula).setFillForegroundColor(new XSSFColor(whiteAsByte, new DefaultIndexedColorMap()));
        mainTasksSummationFormula.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        mainTasksSummationFormula.setAlignment(HorizontalAlignment.LEFT);
        mainTasksSummationFormula.setBorderBottom(BorderStyle.valueOf((short) 0));
        mainTasksSummationFormula.setBorderTop(BorderStyle.valueOf((short) 0));
        mainTasksSummationFormula.setBorderLeft(BorderStyle.valueOf((short) 0));
        mainTasksSummationFormula.setBorderRight(BorderStyle.valueOf((short) 0));

        workWeekLabelOnly = workbook.createCellStyle();
        workWeekLabelOnly.setFont(fontWorkWeekLabelOnly);
        ((XSSFCellStyle) workWeekLabelOnly).setFillForegroundColor(new XSSFColor(gray7AsByte, new DefaultIndexedColorMap()));
        workWeekLabelOnly.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        workWeekLabelOnly.setAlignment(HorizontalAlignment.RIGHT);
        workWeekLabelOnly.setBorderBottom(BorderStyle.valueOf((short) 1));
        workWeekLabelOnly.setBorderTop(BorderStyle.valueOf((short) 1));
        workWeekLabelOnly.setBorderLeft(BorderStyle.valueOf((short) 1));
        workWeekLabelOnly.setBorderRight(BorderStyle.valueOf((short) 1));

        sumOfTasksLabel = workbook.createCellStyle();
        sumOfTasksLabel.setFont(fontWorkWeekLabelOnly);
        ((XSSFCellStyle) sumOfTasksLabel).setFillForegroundColor(new XSSFColor(gray5AsByte, new DefaultIndexedColorMap()));
        sumOfTasksLabel.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumOfTasksLabel.setAlignment(HorizontalAlignment.RIGHT);
        sumOfTasksLabel.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumOfTasksLabel.setBorderTop(BorderStyle.valueOf((short) 1));
        sumOfTasksLabel.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumOfTasksLabel.setBorderRight(BorderStyle.valueOf((short) 1));

        sumOfTasksValues = workbook.createCellStyle();
        sumOfTasksValues.setFont(fontWorkWeek);
        ((XSSFCellStyle) sumOfTasksValues).setFillForegroundColor(new XSSFColor(gray5AsByte, new DefaultIndexedColorMap()));
        sumOfTasksValues.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumOfTasksValues.setAlignment(HorizontalAlignment.RIGHT);
        sumOfTasksValues.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumOfTasksValues.setBorderTop(BorderStyle.valueOf((short) 1));
        sumOfTasksValues.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumOfTasksValues.setBorderRight(BorderStyle.valueOf((short) 1));
        sumOfTasksValues.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        sumOfSubTasksLabel = workbook.createCellStyle();
        sumOfSubTasksLabel.setFont(fontSumOfSubTasksLabelOnly);
        ((XSSFCellStyle) sumOfSubTasksLabel).setFillForegroundColor(new XSSFColor(gray5AsByte, new DefaultIndexedColorMap()));
        sumOfSubTasksLabel.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumOfSubTasksLabel.setAlignment(HorizontalAlignment.RIGHT);
        sumOfSubTasksLabel.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumOfSubTasksLabel.setBorderTop(BorderStyle.valueOf((short) 1));
        sumOfSubTasksLabel.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumOfSubTasksLabel.setBorderRight(BorderStyle.valueOf((short) 1));

        sumOfSubTasksValues = workbook.createCellStyle();
        sumOfSubTasksValues.setFont(fontSumOfSubTasks);
        ((XSSFCellStyle) sumOfSubTasksValues).setFillForegroundColor(new XSSFColor(gray5AsByte, new DefaultIndexedColorMap()));
        sumOfSubTasksValues.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumOfSubTasksValues.setAlignment(HorizontalAlignment.RIGHT);
        sumOfSubTasksValues.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumOfSubTasksValues.setBorderTop(BorderStyle.valueOf((short) 1));
        sumOfSubTasksValues.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumOfSubTasksValues.setBorderRight(BorderStyle.valueOf((short) 1));
        sumOfSubTasksValues.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        headersForHours = workbook.createCellStyle();
        headersForHours.setFont(fontWorkWeekLabelOnly);
        ((XSSFCellStyle) headersForHours).setFillForegroundColor(new XSSFColor(gray5, new DefaultIndexedColorMap()));
        headersForHours.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headersForHours.setAlignment(HorizontalAlignment.RIGHT);
        headersForHours.setBorderBottom(BorderStyle.valueOf((short) 1));
        headersForHours.setBorderTop(BorderStyle.valueOf((short) 1));
        headersForHours.setBorderLeft(BorderStyle.valueOf((short) 1));
        headersForHours.setBorderRight(BorderStyle.valueOf((short) 1));

        daysDate = workbook.createCellStyle();
        daysDate.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysDate).setFillForegroundColor(new XSSFColor(gray3AsByte, new DefaultIndexedColorMap()));
        daysDate.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysDate.setAlignment(HorizontalAlignment.RIGHT);
        daysDate.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysDate.setBorderTop(BorderStyle.valueOf((short) 1));
        daysDate.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysDate.setBorderRight(BorderStyle.valueOf((short) 1));
        daysDate.setDataFormat(creationHelper.createDataFormat().getFormat("0.00"));

        daysHours = workbook.createCellStyle();
        daysHours.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHours).setFillForegroundColor(new XSSFColor(gray3AsByte, new DefaultIndexedColorMap()));
        daysHours.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHours.setAlignment(HorizontalAlignment.RIGHT);
        daysHours.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHours.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHours.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHours.setBorderRight(BorderStyle.valueOf((short) 1));
        daysHours.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        daysHoursBrakeOnly = workbook.createCellStyle();
        daysHoursBrakeOnly.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHoursBrakeOnly).setFillForegroundColor(new XSSFColor(gray3AsByte, new DefaultIndexedColorMap()));
        daysHoursBrakeOnly.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHoursBrakeOnly.setAlignment(HorizontalAlignment.RIGHT);
        daysHoursBrakeOnly.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHoursBrakeOnly.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHoursBrakeOnly.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHoursBrakeOnly.setBorderRight(BorderStyle.valueOf((short) 1));
        daysHoursBrakeOnly.setDataFormat(creationHelper.createDataFormat().getFormat("HH:MM"));

        daysHoursWrong = workbook.createCellStyle();
        daysHoursWrong.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHoursWrong).setFillForegroundColor(new XSSFColor(wrongAsByte, new DefaultIndexedColorMap()));
        daysHoursWrong.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHoursWrong.setAlignment(HorizontalAlignment.RIGHT);
        daysHoursWrong.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHoursWrong.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHoursWrong.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHoursWrong.setBorderRight(BorderStyle.valueOf((short) 1));
        daysHoursWrong.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        daysHoursCorrect = workbook.createCellStyle();
        daysHoursCorrect.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHoursCorrect).setFillForegroundColor(new XSSFColor(corrrectAsByte, new DefaultIndexedColorMap()));
        daysHoursCorrect.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHoursCorrect.setAlignment(HorizontalAlignment.RIGHT);
        daysHoursCorrect.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHoursCorrect.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHoursCorrect.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHoursCorrect.setBorderRight(BorderStyle.valueOf((short) 1));
        daysHoursCorrect.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        daysHoursWarning = workbook.createCellStyle();
        daysHoursWarning.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHoursWarning).setFillForegroundColor(new XSSFColor(waringAsByte, new DefaultIndexedColorMap()));
        daysHoursWarning.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHoursWarning.setAlignment(HorizontalAlignment.RIGHT);
        daysHoursWarning.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHoursWarning.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHoursWarning.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHoursWarning.setBorderRight(BorderStyle.valueOf((short) 1));
        daysHoursWarning.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        daysHoursInHourStyle = workbook.createCellStyle();
        daysHoursInHourStyle.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHoursInHourStyle).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        daysHoursInHourStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHoursInHourStyle.setAlignment(HorizontalAlignment.RIGHT);
        daysHoursInHourStyle.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyle.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyle.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyle.setBorderRight(BorderStyle.valueOf((short) 1));
        // this style was tested for LocalTime.toString and it is working.
        daysHoursInHourStyle.setDataFormat(creationHelper.createDataFormat().getFormat("HH:MM"));

        daysHoursInHourStyleDifferenceOnly = workbook.createCellStyle();
        daysHoursInHourStyleDifferenceOnly.setFont(fontWorkWeek);
        ((XSSFCellStyle) daysHoursInHourStyleDifferenceOnly).setFillForegroundColor(new XSSFColor(whiteAsByte, new DefaultIndexedColorMap()));
        daysHoursInHourStyleDifferenceOnly.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        daysHoursInHourStyleDifferenceOnly.setAlignment(HorizontalAlignment.RIGHT);
        daysHoursInHourStyleDifferenceOnly.setBorderBottom(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyleDifferenceOnly.setBorderTop(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyleDifferenceOnly.setBorderLeft(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyleDifferenceOnly.setBorderRight(BorderStyle.valueOf((short) 1));
        daysHoursInHourStyleDifferenceOnly.setDataFormat(creationHelper.createDataFormat().getFormat("HH:MM"));

        sumForDays = workbook.createCellStyle();
        sumForDays.setFont(fontWorkWeek);
        ((XSSFCellStyle) sumForDays).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        sumForDays.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumForDays.setAlignment(HorizontalAlignment.RIGHT);
        sumForDays.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumForDays.setBorderTop(BorderStyle.valueOf((short) 1));
        sumForDays.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumForDays.setBorderRight(BorderStyle.valueOf((short) 1));
        sumForDays.setDataFormat(creationHelper.createDataFormat().getFormat("0"));

        sumForHoursGray = workbook.createCellStyle();
        sumForHoursGray.setFont(fontWorkWeek);
        ((XSSFCellStyle) sumForHoursGray).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        sumForHoursGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumForHoursGray.setAlignment(HorizontalAlignment.RIGHT);
        sumForHoursGray.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumForHoursGray.setBorderTop(BorderStyle.valueOf((short) 1));
        sumForHoursGray.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumForHoursGray.setBorderRight(BorderStyle.valueOf((short) 1));
        sumForHoursGray.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));

        sumForHoursGrayBreakOnly = workbook.createCellStyle();
        sumForHoursGrayBreakOnly.setFont(fontWorkWeek);
        ((XSSFCellStyle) sumForHoursGrayBreakOnly).setFillForegroundColor(new XSSFColor(gray4AsByte, new DefaultIndexedColorMap()));
        sumForHoursGrayBreakOnly.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        sumForHoursGrayBreakOnly.setAlignment(HorizontalAlignment.RIGHT);
        sumForHoursGrayBreakOnly.setBorderBottom(BorderStyle.valueOf((short) 1));
        sumForHoursGrayBreakOnly.setBorderTop(BorderStyle.valueOf((short) 1));
        sumForHoursGrayBreakOnly.setBorderLeft(BorderStyle.valueOf((short) 1));
        sumForHoursGrayBreakOnly.setBorderRight(BorderStyle.valueOf((short) 1));
        sumForHoursGrayBreakOnly.setDataFormat(creationHelper.createDataFormat().getFormat("HH:MM"));
    }

    public CellStyle getVisualWhiteMarginsLeftShort() {
        return visualWhiteMarginsLeftShort;
    }

    public CellStyle getHeaderBlack() {
        return headerBlack;
    }

    public CellStyle getWorkWeek() {
        return workWeek;
    }

    public CellStyle getDaysDate() {
        return daysDate;
    }

    public CellStyle getSumForDays() {
        return sumForDays;
    }

    public CellStyle getWorkWeekLabelOnly() {
        return workWeekLabelOnly;
    }

    public CellStyle getHeadersForHours() {
        return headersForHours;
    }

    public CellStyle getSumForHoursGray() {
        return sumForHoursGray;
    }

    public CellStyle getDaysHoursInHourStyle() {
        return daysHoursInHourStyle;
    }

    public CellStyle getDaysHoursWrong() {
        return daysHoursWrong;
    }

    public CellStyle getDaysHoursCorrect() {
        return daysHoursCorrect;
    }

    public CellStyle getDaysHoursWarning() {
        return daysHoursWarning;
    }

    public CellStyle getMainTasksLabel() {
        return mainTasksLabel;
    }

    public CellStyle getMainTasksHours() {
        return mainTasksHours;
    }

    public CellStyle getMainTasksLink() {
        return mainTasksLink;
    }

    public CellStyle getMainTasksSummationFormula() {
        return mainTasksSummationFormula;
    }

    public CellStyle getSumOfTasksLabel() {
        return sumOfTasksLabel;
    }

    public CellStyle getSumOfTasksValues() {
        return sumOfTasksValues;
    }

    public CellStyle getSubTasksLabelColorALeft() {
        return subTasksLabelColorALeft;
    }

    public CellStyle getSubTasksLabelColorAMid() {
        return subTasksLabelColorAMid;
    }

    public CellStyle getSubTasksLabelColorARight() {
        return subTasksLabelColorARight;
    }

    public CellStyle getSubTasksColorA() {
        return subTasksColorA;
    }

    public CellStyle getSubTasksColorB() {
        return subTasksColorB;
    }

    public CellStyle getSubTasksLabelColorBLeft() {
        return subTasksLabelColorBLeft;
    }

    public CellStyle getSubTasksLabelColorBMid() {
        return subTasksLabelColorBMid;
    }

    public CellStyle getSubTasksLabelColorBRight() {
        return subTasksLabelColorBRight;
    }

    public CellStyle getSumOfSubTasksLabel() {
        return sumOfSubTasksLabel;
    }

    public CellStyle getSumOfSubTasksValues() {
        return sumOfSubTasksValues;
    }

    public CellStyle getDaysHours() {
        return daysHours;
    }

    public CellStyle getSumForHoursGrayBreakOnly() {
        return sumForHoursGrayBreakOnly;
    }

    public CellStyle getDaysHoursBrakeOnly() {
        return daysHoursBrakeOnly;
    }

    public CellStyle getDaysHoursInHourStyleDifferenceOnly() {
        return daysHoursInHourStyleDifferenceOnly;
    }
}