package com.michalrys.hours.model.excel;

import java.time.LocalDate;
import java.util.List;

public interface FileSummary {
    List<LocalDate> getDays();

    void writeFile();
}
