/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import java.time.LocalDate;
import java.util.Map;
import java.util.TreeMap;

public class HourTaskDTO implements Comparable<HourTaskDTO> {
    private final String name;
    private double time;
    private DayDTO day;

    public HourTaskDTO(String name, double time) {
        this.name = name;
        this.time = time;
    }
    public HourTaskDTO(String name, double time, DayDTO day) {
        this.name = name;
        this.time = time;
        this.day = day;
    }

    public void setDay(DayDTO day) {
        this.day = day;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public DayDTO getDay() {
        return day;
    }

    public double getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HourTaskDTO that = (HourTaskDTO) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HourTaskDTO{" +
                "name='" + name + '\'' +
                ", time=" + time +
                '}';
    }

    @Override
    public int compareTo(HourTaskDTO other) {
        return name.compareTo(other.name);
    }

    public static Map<HourTaskDTO, Map<HourTaskDTO, Double>> getAllWorkTasks(DayDTO currentDay, String summaryDetails) {
        String[] split = summaryDetails.split("\n");

        Map<HourTaskDTO, Map<HourTaskDTO, Double>> taskWork = new TreeMap<>();

        HourTaskDTO mainHourTaskDTO = null;
        HourTaskDTO hourTaskDTO = null;

        boolean firstLine = true;
        int separatorCount = 0;

        for (String message : split) {
            if (message.equals("")) {
                firstLine = true;
                separatorCount++;
                continue;
            }
            String[] line = message.split(" \\| ");
            String time = line[1].replaceAll("h", "");
            String name = "";
            if (line.length == 3) {
                name = line[2];
            }

            if (separatorCount == 2) {
                break;
            }

            if (firstLine) {
                mainHourTaskDTO = new HourTaskDTO(name, Double.valueOf(time), currentDay);
                Map<HourTaskDTO, Double> subTask = new TreeMap<>();
                subTask.put(mainHourTaskDTO, mainHourTaskDTO.getTime());
                taskWork.put(mainHourTaskDTO, subTask);
            } else {
                hourTaskDTO = new HourTaskDTO(name, Double.valueOf(time), currentDay);
                Map<HourTaskDTO, Double> subtasks = taskWork.get(mainHourTaskDTO);

                if (subtasks.containsKey(hourTaskDTO)) {
                    Double previousTime = subtasks.getOrDefault(hourTaskDTO, 0.0);
                    subtasks.remove(hourTaskDTO);
                    hourTaskDTO.setTime(hourTaskDTO.getTime() + previousTime);
                }
                subtasks.put(hourTaskDTO, hourTaskDTO.getTime());

                taskWork.put(mainHourTaskDTO, subtasks);
            }

            separatorCount = 0;
            firstLine = false;
        }

//        System.out.println("--------");
//        System.out.println("=== works ===");
//        for (HourTaskDTO mainTask : taskWork.keySet()) {
//            System.out.println(mainTask);
//            for (HourTaskDTO subTask : taskWork.get(mainTask).keySet()) {
//                if (subTask.equals(mainTask)) {
//                    continue;
//                }
//                System.out.println(" - " + subTask);
//            }
//        }
//
        return taskWork;
    }

    public static Map<HourTaskDTO, Map<HourTaskDTO, Double>> getAllBreakTasks(DayDTO currentDay, String summaryDetails) {
        String[] split = summaryDetails.split("\n");

        Map<HourTaskDTO, Map<HourTaskDTO, Double>> taskBreak = new TreeMap<>();

        HourTaskDTO mainHourTaskDTO = null;
        HourTaskDTO hourTaskDTO = null;

        boolean firstLine = true;
        boolean isBrakeTasks = false;
        int separatorCount = 0;

        for (String message : split) {
            if (message.equals("")) {
                firstLine = true;
                separatorCount++;
                continue;
            }
            String[] line = message.split(" \\| ");
            String time = line[1].replaceAll("h", "");
            String name = "";
            if (line.length == 3) {
                name = line[2];
            }

            if (separatorCount == 2) {
                isBrakeTasks = true;
            }

            if (firstLine & isBrakeTasks) {
                mainHourTaskDTO = new HourTaskDTO(name, Double.valueOf(time));
                Map<HourTaskDTO, Double> subTask = new TreeMap<>();
                subTask.put(mainHourTaskDTO, mainHourTaskDTO.getTime());
                taskBreak.put(mainHourTaskDTO, subTask);
            } else if (!firstLine & isBrakeTasks) {
                hourTaskDTO = new HourTaskDTO(name, Double.valueOf(time));
                Map<HourTaskDTO, Double> subtasks = taskBreak.get(mainHourTaskDTO);

                if (subtasks.containsKey(hourTaskDTO)) {
                    Double previousTime = subtasks.getOrDefault(hourTaskDTO, 0.0);
                    subtasks.remove(hourTaskDTO);
                    hourTaskDTO.setTime(hourTaskDTO.getTime() + previousTime);
                }

                subtasks.put(hourTaskDTO, hourTaskDTO.getTime());
                taskBreak.put(mainHourTaskDTO, subtasks);
            }

            separatorCount = 0;
            firstLine = false;
        }

//        System.out.println("--------");
//        System.out.println("=== brakes ===");
//
//        for (HourTaskDTO mainTask : taskBreak.keySet()) {
//            System.out.println(mainTask);
//            for (HourTaskDTO subTask : taskBreak.get(mainTask).keySet()) {
//                if (subTask.equals(mainTask)) {
//                    continue;
//                }
//                System.out.println(" - " + subTask);
//            }
//        }

        return taskBreak;
    }
}