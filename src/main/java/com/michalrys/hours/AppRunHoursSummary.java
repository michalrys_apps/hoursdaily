/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours;

import com.michalrys.hours.view.hourssummary.MVCSummary;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;
import java.io.InputStream;

public class AppRunHoursSummary extends Application {
    public static final String VERSION = "1.2.2";
    public static final String PRODUCTION_YEARS = "2019-2022";
    public static final String DATE = "2022-01-16";
    public static final String AUTHOR = "Michał Ryś";
    public static final String EMAIL = "michalrys@gmail.com";
    public static final String LICENSE = "\u00A9 " + PRODUCTION_YEARS + " " + AUTHOR + ".  All rights reserved.";
    public static final String FREEWARE_FOR = "Freeware for nobody at the moment :-)";
    public static Stage summaryHoursStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("view/hourssummary/WindowHoursSummary.fxml"));
        primaryStage.setTitle("Hours summary");
        primaryStage.setScene(new Scene(root, 520, 430));
        primaryStage.setResizable(false);
        primaryStage.setAlwaysOnTop(true);

        PointerInfo pointerInfo = MouseInfo.getPointerInfo();
        Point location = pointerInfo.getLocation();
        primaryStage.setX(location.getX() - 180); //500
        primaryStage.setY(100);

        InputStream resourceAsStream = MVCSummary.class.getResourceAsStream("dailySummary.png");
        primaryStage.getIcons().add(new Image(resourceAsStream));

        primaryStage.show();

        new Robot().mouseMove(
                (int) primaryStage.getX() + 180,
                (int) primaryStage.getY() + 15
        );

        summaryHoursStage = primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
