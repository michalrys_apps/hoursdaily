package com.michalrys.hours.view.hoursdaily;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.tasks.TaskContainer;

import java.io.File;
import java.util.Set;

public interface MVCDaily {
    interface Controller {
        void startUpModel();

        void setModel(HourTask modelTask, TaskContainer modelTasks, File publicPoint);

        void getTasks();

        void bSaveClicked(RadioButtonType radioButtonType, String fTask, String fDescription);

        void bTaskRemoveClicked(String fTask);

        void editDraggedAndDroppedFile(String[] inputArg);

        boolean existsInTasks(String task);

        void setTaskDescriptionsWithinCurrentWorkweek(String task);
    }

    interface View {
        void initData();

        void setFieldTaskContextMenu(Set<String> tasks);

        void updateDraggedAndDroppedFile(File file, String task, String timeStamp, String description, String type);

        void hide();

        void setIsInTray(boolean isInTray);

        boolean isInTray();

        void setFieldDescription(Set<String> taskDescriptions);
    }
}
