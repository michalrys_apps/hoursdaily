package com.michalrys.hours.view.hoursdaily;

public enum RadioButtonType {
    WORK,
    BREAK,
    ENDDAY;
}
