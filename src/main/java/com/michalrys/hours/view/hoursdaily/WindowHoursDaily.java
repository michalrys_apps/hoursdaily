/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.view.hoursdaily;

import com.michalrys.hours.AppRunHoursDaily;
import com.michalrys.hours.controller.WindowHoursDailyController;
import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.tasks.TaskContainer;
import com.michalrys.hours.tools.JSONMySimplestParser;
import com.michalrys.hours.tools.TimestampFormatter;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.awt.*;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Set;


public class WindowHoursDaily implements MVCDaily.View {
    private static final String PUBLIC_POINT_PATH = "d:/DailyHours";//TODO later do it better
    private boolean isInTray = false;
    private MVCDaily.Controller controller = new WindowHoursDailyController(this);
    private File publicPoint;
    private TaskContainer modelTasks;
    private HourTask modelTask;
    private RadioButtonType radioButtonType = RadioButtonType.WORK;
    private File fileToUpdate;

    @FXML
    private Label fVersion;

    @FXML
    private Label labelTask;

    @FXML
    private Label labelDescription;

    @FXML
    private TextField fTask;

    @FXML
    private TextField fDescription;

    @FXML
    private RadioButton rbWork;

    @FXML
    private RadioButton rbBreak;

    @FXML
    private RadioButton rbEndDay;

    @FXML
    private Button bTaskRemove;

    @FXML
    private Button bSave;

    @FXML
    private Button bUpdate;

    @FXML
    private TextField fDateTimeForUpdate;

    @FXML
    public void fileOver(DragEvent dragEvent) {
        dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        dragEvent.consume();
    }

    @FXML
    public void fileDropped(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        List<File> files = dragboard.getFiles();
        dragEvent.setDropCompleted(true);
        dragEvent.consume();

        File lastFile = files.get(files.size() - 1);
        String[] inputArg = new String[1];
        inputArg[0] = lastFile.getAbsolutePath();

        controller.editDraggedAndDroppedFile(inputArg);
    }

    @FXML
    void initialize() {
        bUpdate.setVisible(false);
        fVersion.setText("\u00A9");
        publicPoint = new File(PUBLIC_POINT_PATH);
        if (!publicPoint.exists()) {
            publicPoint.mkdirs();
        }
        controller.setModel(modelTask, modelTasks, publicPoint);
        controller.startUpModel();
        controller.getTasks();
    }

    @Override
    public void initData() {
        bUpdate.setVisible(false);
        fVersion.setText("\u00A9");
        publicPoint = new File(PUBLIC_POINT_PATH);
        if (!publicPoint.exists()) {
            publicPoint.mkdirs();
        }
        controller.setModel(modelTask, modelTasks, publicPoint);
        controller.startUpModel();
        controller.getTasks();
        fTask.requestFocus();
        fTask.setText("");
        fDescription.setText("");
        rbWorkAction();

        Stage window = (Stage) fDescription.getScene().getWindow();
        window.setHeight(170);
        bUpdate.setDisable(true);
        bUpdate.setVisible(false);
        bSave.setDisable(false);
        bSave.setVisible(true);

        try {
            new Robot().mouseMove(
                    (int) window.getX() + 180,
                    (int) window.getY() + 80
            );
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void rbWorkAction() {
        rbWork.setSelected(true);
        rbBreak.setSelected(false);
        rbEndDay.setSelected(false);
        radioButtonType = RadioButtonType.WORK;
    }

    public void rbBreakAction() {
        rbWork.setSelected(false);
        rbBreak.setSelected(true);
        rbEndDay.setSelected(false);
        radioButtonType = RadioButtonType.BREAK;
    }

    public void rbEndDayAction() {
        rbWork.setSelected(false);
        rbBreak.setSelected(false);
        rbEndDay.setSelected(true);
        radioButtonType = RadioButtonType.ENDDAY;
    }

    public void versionAuthorClicked() {
        Stage dailyHoursWindow = (Stage) fDescription.getScene().getWindow();

        double mainX = dailyHoursWindow.getX();
        double mainY = dailyHoursWindow.getY();
        double mainDeltaY = dailyHoursWindow.getHeight();
        dailyHoursWindow.setAlwaysOnTop(false);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("License and version information");

        StringBuffer text = new StringBuffer();
        text.append("Version: " + AppRunHoursDaily.VERSION + ", ");
        text.append("Date: " + AppRunHoursDaily.DATE + ".\n");
        text.append(AppRunHoursDaily.LICENSE + "\n");
        text.append(AppRunHoursDaily.FREEWARE_FOR + "\n");
        text.append("E-mail: " + AppRunHoursDaily.EMAIL + "\n");

        alert.setContentText(text.toString());
        alert.setY(mainY + mainDeltaY / 2);
        alert.setX(mainX + 10);

        alert.showAndWait();
        dailyHoursWindow.setAlwaysOnTop(true);
    }

    @Override
    public void setFieldTaskContextMenu(Set<String> tasks) {
        ContextMenu contextMenu = new ContextMenu();
        for (String task : tasks) {
            MenuItem menuItem = new MenuItem(task);
            menuItem.setOnAction(event -> fTask.setText(task));
            contextMenu.getItems().add(menuItem);
        }
        fTask.setContextMenu(contextMenu);
    }

    @Override
    public void updateDraggedAndDroppedFile(File file, String task, String timeStamp, String description, String type) {
//        AppRunHoursDaily.dailyHoursStage.setHeight(AppRunHoursDaily.WINDOW_HEIGHT_FOR_UPDATE);
        Stage window = (Stage) bSave.getScene().getWindow();
        window.setHeight(220);

        bUpdate.setVisible(true);
        bUpdate.setDisable(false);
        bSave.setVisible(false);
        bSave.setDisable(true);

        fileToUpdate = file;
        fTask.setText(task);
        fDateTimeForUpdate.setText(timeStamp);
        fDateTimeForUpdate.setDisable(false);
        fDateTimeForUpdate.setEditable(true);
        fDescription.setText(description);
        if (type.equals(TaskType.WORK.toString())) {
            rbWorkAction();
        } else if (type.equals(TaskType.BREAK.toString())) {
            rbBreakAction();
        } else {
            rbEndDayAction();
        }
    }

    @FXML
    public void bSaveClicked() {
        controller.bSaveClicked(radioButtonType, fTask.getText(), fDescription.getText());
    }

    @FXML
    public void bUpdateClicked() {
        String newTimeStamp = fDateTimeForUpdate.getText();
        LocalDateTime dateTime;
        try {
            dateTime = LocalDateTime.parse(newTimeStamp, TimestampFormatter.getDateTime());
        } catch (DateTimeParseException e) {
            double mainX = AppRunHoursDaily.dailyHoursStage.getX();
            double mainY = AppRunHoursDaily.dailyHoursStage.getY();
            double mainDeltaY = AppRunHoursDaily.dailyHoursStage.getHeight();
            AppRunHoursDaily.dailyHoursStage.setAlwaysOnTop(false);

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Wrong date time format");

            StringBuffer text = new StringBuffer();
            text.append("Given date and time is wrong. Please write it in following format:\n");
            text.append("2019-10-02,08:06:28\n");
            text.append("2019-03-02,23:55:28");

            alert.setContentText(text.toString());
            alert.setY(mainY + mainDeltaY / 2);
            alert.setX(mainX + 10);

            alert.showAndWait();
            AppRunHoursDaily.dailyHoursStage.setAlwaysOnTop(true);
            return;
        }

        JSONObject json = new JSONObject();

        json.put(HourTaskImpl.KEY_TASK, fTask.getText());
        json.put(HourTaskImpl.KEY_TIME_STAMP, fDateTimeForUpdate.getText());
        json.put(HourTaskImpl.KEY_DESCRIPTION, fDescription.getText());
        json.put(HourTaskImpl.KEY_TYPE, radioButtonType.toString());

        String absolutePath = fileToUpdate.getAbsolutePath();
        String fileName = fileToUpdate.getName();
        String newFileName = "HourTask_" + dateTime.format(TimestampFormatter.getDateTimeForFileName()) + ".json";
        absolutePath = absolutePath.replaceAll(fileName, newFileName);

        fileToUpdate.delete();
        fileToUpdate = new File(absolutePath);

        JSONMySimplestParser.write(fileToUpdate, json);
    }

    @FXML
    public void bTaskRemoveClicked() {
        controller.bTaskRemoveClicked(fTask.getText());
    }

    @FXML
    public void fTaskKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            if (!bSave.isDisable()) {
                bSaveClicked();
            } else {
                bUpdateClicked();
            }
        }
        if (keyEvent.getCode().equals(KeyCode.CONTROL)) {
//            double x = AppRunHoursDaily.dailyHoursStage.getX();
//            double y = AppRunHoursDaily.dailyHoursStage.getY();
            PointerInfo pointerInfo = MouseInfo.getPointerInfo();
            Point location = pointerInfo.getLocation();
            fTask.getContextMenu().show(fTask, location.getX(), location.getY());
        }
    }

    @FXML
    public void fDescriptionKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            if (!bSave.isDisable()) {
                bSaveClicked();
            } else {
                bUpdateClicked();
            }
        }
    }

    @FXML
    public void fDescriptionMousePressed(MouseEvent mouseEvent) {
        MouseButton button = mouseEvent.getButton();
        if (!button.equals(MouseButton.SECONDARY)) {
            return;
        }
        String task = fTask.getText();
        if (task.isEmpty()) {
            return;
        }
        boolean existInTasks = controller.existsInTasks(task);
        if(!existInTasks) {
            return;
        }
        controller.setTaskDescriptionsWithinCurrentWorkweek(task);
    }

    @Override
    public void setFieldDescription(Set<String> taskDescriptions) {
        ContextMenu contextMenu = new ContextMenu();
        for (String taskDescription : taskDescriptions) {
            MenuItem menuItem = new MenuItem(taskDescription);
            menuItem.setOnAction(event -> fDescription.setText(taskDescription));
            contextMenu.getItems().add(menuItem);
        }
        fDescription.setContextMenu(contextMenu);
    }

    @Override
    public void setIsInTray(boolean isInTray) {
        this.isInTray = isInTray;
    }

    @Override
    public boolean isInTray() {
        return isInTray;
    }

    @Override
    public void hide() {
        Stage stage = (Stage) bSave.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void labelTaskClicked(MouseEvent mouseEvent) {
        //TODO this is only quick implementation, update it later: build interactive window
        MouseButton button = mouseEvent.getButton();
        String tasksPath = PUBLIC_POINT_PATH + "/Tasks.json";
        if (button == MouseButton.SECONDARY) {
            try {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec(new String[]{"cmd.exe", "/c", "start", tasksPath});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void labelDescriptionClicked(MouseEvent mouseEvent) {
        //TODO this is only quick implementation, think about how to do it better
        MouseButton button = mouseEvent.getButton();
        if (button == MouseButton.SECONDARY) {
            try {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec(new String[]{"cmd.exe", "/c", "start", PUBLIC_POINT_PATH});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}