package com.michalrys.hours.view.hourssummary;

import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.tasklink.TaskLinkContainer;
import javafx.stage.Stage;

import java.util.Map;

public interface MVCTaskLinks {
    interface Controller {
        void setModel(HourTasksSummary modelTasksSummary);

        void setTasksInWindow();

        void setAllTasksWithNoLinkInWindow();
    }

    interface View {
        void setModelTaskLinks(TaskLinkContainer modelTaskLinks);

        void initDataFromSummaryWindow(Stage taskLinksStage, HourTasksSummary modelTasksSummary, Stage windowHoursSummaryStage);

        void initDataWithAllTaskLinksWithoutLink(Stage taskLinksStage, HourTasksSummary modelTasksSummary, Stage hoursSummaryStage);

        void setTaskLinksInWindow(Map<String, String> taskLink);

        void setWindowSize(int amountOfTasks);
    }
}
