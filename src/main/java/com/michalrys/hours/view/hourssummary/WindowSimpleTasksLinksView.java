/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.view.hourssummary;

import com.michalrys.hours.controller.WindowSimpleTasksLinksController;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.tasklink.TaskLinkContainer;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class WindowSimpleTasksLinksView implements MVCTaskLinks.View {
    private static final String PUBLIC_POINT_PATH = "d:/DailyHours";//TODO later do it better
    private MVCTaskLinks.Controller controller = new WindowSimpleTasksLinksController(this);
    private File publicPoint;
    private HourTasksSummary modelTasksSummary;
    private TaskLinkContainer modelTaskLinks;
    private Stage taskLinksStage;
    private Stage hoursSummaryStage;

    @FXML
    private VBox vBoxTask;

    @FXML
    private VBox vBoxLink;

    @FXML
    private VBox vBoxEdit;

    @FXML
    private Separator separatorTask;

    @FXML
    private Separator separatorLink;

    //--------------------------------------
    @FXML
    void initialize() {
        publicPoint = new File(PUBLIC_POINT_PATH);
    }

    @Override
    public void setModelTaskLinks(TaskLinkContainer modelTaskLinks) {
        this.modelTaskLinks = modelTaskLinks;
    }

    @Override
    public void initDataFromSummaryWindow(Stage taskLinksStage, HourTasksSummary modelTasksSummary, Stage hoursSummaryStage) {
        this.modelTasksSummary = modelTasksSummary;
        this.taskLinksStage = taskLinksStage;
        this.hoursSummaryStage = hoursSummaryStage;
        controller.setModel(modelTasksSummary);
        controller.setTasksInWindow();
    }

    @Override
    public void initDataWithAllTaskLinksWithoutLink(Stage taskLinksStage, HourTasksSummary modelTasksSummary, Stage hoursSummaryStage) {
        this.modelTasksSummary = modelTasksSummary;
        this.taskLinksStage = taskLinksStage;
        this.hoursSummaryStage = hoursSummaryStage;
        controller.setModel(modelTasksSummary);
        controller.setAllTasksWithNoLinkInWindow();
    }

    @Override
    public void setTaskLinksInWindow(Map<String, String> taskLink) {
        ObservableList<Node> childrenTask = vBoxTask.getChildren();
        ObservableList<Node> childrenLink = vBoxLink.getChildren();
        ObservableList<Node> childrenEdit = vBoxEdit.getChildren();
        Set<String> tasks = taskLink.keySet();

        for (String task : tasks) {
            //task
            Label taskName = new Label(task);
            taskName.setPadding(new Insets(5,5,0,0));
            childrenTask.add(taskName);

            //link
            Label link = new Label(taskLink.get(task));
            link.setTooltip(new Tooltip(link.getText()));
            link.setOnMouseClicked(event -> {
                try {
                    Runtime runtime = Runtime.getRuntime();
                    runtime.exec(new String[]{"cmd.exe","/c","start",link.getText()});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            link.setCursor(Cursor.HAND);
            link.setOnMouseEntered(event -> {
                link.setTextFill(Color.rgb(235,17,0));
            });
            link.setOnMouseExited(event -> {
                link.setTextFill(Color.rgb(0,0,0));
            });
            link.setPadding(new Insets(5,0,0,0));
            childrenLink.add(link);

            //edit
            Label edit = new Label("EDIT");
            edit.setCursor(Cursor.HAND);
            edit.setOnMouseEntered(event -> {
                edit.setTextFill(Color.rgb(0,46,255));
            });
            edit.setOnMouseExited(event -> {
                edit.setTextFill(Color.rgb(0,0,0));
            });

            edit.setOnMouseClicked(event -> {
                displayDialogWindow(taskName, link);
            });
            edit.setPadding(new Insets(5,0,0,0));
            childrenEdit.add(edit);
        }
    }

    private void displayDialogWindow(Label taskName, Label link) {
        TextInputDialog dialog = new TextInputDialog(link.getText());
        dialog.setTitle("Link edit for: " + taskName.getText());
        dialog.setHeaderText("Write new link to hour file. Give absolute path.");
        dialog.setContentText("Link: ");
        dialog.setX(taskLinksStage.getX() + taskLinksStage.getWidth() - 50);
        dialog.setY(taskLinksStage.getY() + 5);
        hoursSummaryStage.setAlwaysOnTop(false);
        taskLinksStage.setAlwaysOnTop(false);
        dialog.setOnCloseRequest(event1 -> {
            hoursSummaryStage.setAlwaysOnTop(true);
            taskLinksStage.setAlwaysOnTop(true);
        });
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            link.setText(result.get());
            updateTaskInFile(taskName.getText(), link.getText());
        }
    }

    private void updateTaskInFile(String taskName, String taskLink) {
        modelTaskLinks.update(taskName, taskLink);
    }

    @Override
    public void setWindowSize(int amountOfTasks) {
        int height = 50 + 22 * amountOfTasks;
        taskLinksStage.setHeight(height);
        vBoxTask.setPrefHeight(height);
        vBoxLink.setPrefHeight(height);
        vBoxEdit.setPrefHeight(height);
        separatorTask.setPrefHeight(height);
        separatorLink.setPrefHeight(height);
    }
}