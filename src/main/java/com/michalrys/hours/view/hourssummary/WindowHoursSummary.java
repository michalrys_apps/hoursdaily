/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours.view.hourssummary;

import com.michalrys.hours.AppRunHoursSummary;
import com.michalrys.hours.controller.WindowHoursSummaryController;
import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummaryAll;
import com.michalrys.hours.tools.TimestampFormatter;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class WindowHoursSummary implements MVCSummary.View {
    private static final String PUBLIC_POINT_PATH = "d:/DailyHours";//TODO later do it better
    private MVCSummary.Controller controller = new WindowHoursSummaryController(this);
    private File publicPoint;
    private HourTasksSummary modelTasksSummary;

    @FXML
    private Label fIsDayClosed;

    @FXML
    private Button bPreviousDay;

    @FXML
    private TextField fDate;

    @FXML
    private Button bNextDay;

    @FXML
    private TextField fWW;

    @FXML
    private TextField fStart;

    @FXML
    private TextField fEnd;

    @FXML
    private TextField fTotal;

    @FXML
    private TextField fWork;

    @FXML
    private Label labelWorkPercentage;

    @FXML
    private TextField fBreak;

    @FXML
    private Label labelBreakPercentage;

    @FXML
    private Button bEndDay;

    @FXML
    private TextArea taSummary;

    @FXML
    private TextArea taDetails;

    @FXML
    private Label labelCurrentTask;

    @FXML
    private Label fVersion;

    @FXML
    private Button bExcel;

    @FXML
    private Button bArchive;

    @FXML
    private Button bTaskLinks;

    @FXML
    private RadioButton rbTimeStyleH;

    @FXML
    private RadioButton rbTimeStyleHM;

    @FXML
    public void bPreviousDayClicked() {
        controller.setPreviousDay();
        controller.setLeavingTime();
    }

    @FXML
    public void bNextDayClicked() {
        controller.setNextDay();
        controller.setLeavingTime();
    }

    @FXML
    public void bEndDayClicked() {
        controller.closeDay();
    }

    @FXML
    public void bTaskLinksClicked(MouseEvent mouseEvent) {
        //TODO add here action for window
        try {
            Stage stageHoursSummary = (Stage) bTaskLinks.getScene().getWindow();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("WindowSimpleTasksLinks.fxml"));
            Parent rootFilters = fxmlLoader.load();
            Stage taskLinksStage = new Stage();

            taskLinksStage.setTitle("Task links");
            taskLinksStage.setScene(new Scene(rootFilters));
            taskLinksStage.setX(stageHoursSummary.getX() + 200);
            taskLinksStage.setY(stageHoursSummary.getY() + 100);

            taskLinksStage.initStyle(StageStyle.DECORATED);

            WindowSimpleTasksLinksView windowSimpleTasksLinksView = fxmlLoader.<WindowSimpleTasksLinksView>getController();
            Stage hoursSummaryStage = (Stage) fTotal.getScene().getWindow();

            MouseButton button = mouseEvent.getButton();
            if (button == MouseButton.SECONDARY) {
                windowSimpleTasksLinksView.initDataWithAllTaskLinksWithoutLink(taskLinksStage, modelTasksSummary, hoursSummaryStage);
            } else {
                windowSimpleTasksLinksView.initDataFromSummaryWindow(taskLinksStage, modelTasksSummary, hoursSummaryStage);
            }

            taskLinksStage.setAlwaysOnTop(true);
            taskLinksStage.setResizable(false);
            taskLinksStage.show();

            try {
                new Robot().mouseMove(
                        (int) stageHoursSummary.getX() + 200 + 200,
                        (int) stageHoursSummary.getY() + 100 + 10);
            } catch (AWTException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void bExcelClicked() {
        controller.createExcelSummary();
    }

    @FXML
    public void bArchiveClicked() {
        Stage stageHoursSummary = (Stage) bTaskLinks.getScene().getWindow();
        controller.archiveOfHourTaskFiles(stageHoursSummary.getX(), stageHoursSummary.getY(), fWW.getText());
    }

    @FXML
    public void rbTimeStyleHClicked() {
        rbTimeStyleH.setSelected(true);
        rbTimeStyleHM.setSelected(false);
        controller.getDataFromModelAndSetupWindow();
    }

    @FXML
    public void rbTimeStyleHMClicked() {
        rbTimeStyleHM.setSelected(true);
        rbTimeStyleH.setSelected(false);
        controller.getDataFromModelAndSetupWindow();
    }


    //--------------------------------------
    @FXML
    void initialize() {
        bTaskLinks.setTooltip(new Tooltip("LMB: for current day\nRMB: all tasks without links"));

        publicPoint = new File(PUBLIC_POINT_PATH);
        fVersion.setText("\u00A9");
        if (!publicPoint.exists()) {
            publicPoint.mkdirs();
        }
        controller.setUpController(publicPoint);
        controller.getDataFromModelAndSetupWindow();
        controller.setLeavingTime();
    }

    @Override
    public void setLeavingTime() {
        String endHour = fEnd.getText();
        String workingHours = fWork.getText();

        String[] workingHoursSplited = workingHours.split(":");
        int hours = Integer.valueOf(workingHoursSplited[0]);
        int minutes = Integer.valueOf(workingHoursSplited[1]);

        if (hours >= 8) {
            fWork.setTooltip(new Tooltip("8h done"));
            return;
        }
        LocalTime endTime = LocalTime.parse(endHour, DateTimeFormatter.ofPattern("HH:mm"));
        LocalTime extraTime = LocalTime.of(hours, minutes);
        endTime = endTime.plusHours(7 - extraTime.getHour());
        endTime = endTime.plusMinutes(60 - extraTime.getMinute());

        fWork.setTooltip(new Tooltip("end at " + endTime));
    }

    @Override
    public void initData() {
        publicPoint = new File(PUBLIC_POINT_PATH);
        fVersion.setText("\u00A9");
        if (!publicPoint.exists()) {
            publicPoint.mkdirs();
        }
        controller.setUpController(publicPoint);
        controller.getDataFromModelAndSetupWindow();

        Stage stage = (Stage) bTaskLinks.getScene().getWindow();
        try {
            new Robot().mouseMove(
                    (int) stage.getX() + 180,
                    (int) stage.getY() + 15
            );
        } catch (AWTException e) {
            e.printStackTrace();
        }
        controller.setLeavingTime();
    }

    @Override
    public void setModel(HourTasksSummary modelTasksSummary) {
        this.modelTasksSummary = modelTasksSummary;
    }

    public void versionAuthorClicked() {
        Stage windowHoursSummary = (Stage) bNextDay.getScene().getWindow();

        double mainX = windowHoursSummary.getX();
        double mainY = windowHoursSummary.getY();
        double mainDeltaY = windowHoursSummary.getHeight();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("License and version information");
        windowHoursSummary.setAlwaysOnTop(false);

        StringBuffer text = new StringBuffer();
        text.append("Version: " + AppRunHoursSummary.VERSION + ", ");
        text.append("Date: " + AppRunHoursSummary.DATE + ".\n");
        text.append(AppRunHoursSummary.LICENSE + "\n");
        text.append(AppRunHoursSummary.FREEWARE_FOR + "\n");
        text.append("E-mail: " + AppRunHoursSummary.EMAIL + "\n");

        alert.setContentText(text.toString());
        alert.setY(mainY + mainDeltaY / 2);
        alert.setX(mainX + 10);

        alert.showAndWait();
        windowHoursSummary.setAlwaysOnTop(true);
    }

    @Override
    public void updateButtonCloseDay() {
        if (fIsDayClosed.getText().equals(HourTasksSummaryAll.THIS_DAY_IS_CLOSED)) {
            bEndDay.setDisable(true);
        } else {
            bEndDay.setDisable(false);
        }
    }

    @Override
    public void setFDate(LocalDate currentDay) {
        fDate.setText(currentDay.format(TimestampFormatter.getDate()));
    }

    @Override
    public void setWW(String ww) {
        fWW.setText(ww);
    }

    @Override
    public void setEndDayMessage(String endDayMessage) {
        fIsDayClosed.setText(endDayMessage);
    }

    @Override
    public void setStartTime(LocalTime startTime) {
        fStart.setText(startTime.format(TimestampFormatter.getTimeHHmmFormatter()));
    }

    @Override
    public void setEndTime(LocalTime endTime) {
        fEnd.setText(endTime.format(TimestampFormatter.getTimeHHmmFormatter()));
    }

    @Override
    public void setTotalTime(LocalTime totalTime) {
        fTotal.setText(totalTime.format(TimestampFormatter.getTimeHHmmFormatter()));
    }

    @Override
    public void setWorkTime(LocalTime workTime) {
        fWork.setText(workTime.format(TimestampFormatter.getTimeHHmmFormatter()));
    }

    @Override
    public void setBreakTime(LocalTime breakTime) {
        fBreak.setText(breakTime.format(TimestampFormatter.getTimeHHmmFormatter()));
    }

    @Override
    public void setSummary(String summary) {
        taSummary.setText(summary);
    }

    @Override
    public void setSummaryDetails(String summaryDetails) {
        taDetails.setText(summaryDetails);
    }

    @Override
    public void setLabelWorkPercentage(LocalTime workTime, LocalTime totalTime) {
        double workSeconds = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00"), workTime);
        double totalSeconds = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00"), totalTime);
        double percentage = (workSeconds / totalSeconds) * 100;
        labelWorkPercentage.setText(String.format("%3.0f", percentage) + "%");
    }

    @Override
    public void setLabelBreakPercentage(LocalTime breakTime, LocalTime totalTime) {
        double breakSeconds = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00"), breakTime);
        double totalSeconds = ChronoUnit.SECONDS.between(LocalTime.parse("00:00:00"), totalTime);
        double percentage = (breakSeconds / totalSeconds) * 100;
        labelBreakPercentage.setText(String.format("%3.0f", percentage) + "%");
    }

    @Override
    public boolean isRbTimeStyleHSelected() {
        return rbTimeStyleH.isSelected();
    }

    @Override
    public void setLatestTaskFromCurrentDay(HourTask latestTask) {
        labelCurrentTask.setText("(" + latestTask.getType()
                + ": " + latestTask.getTask()
                + " -> " + latestTask.getDescription()
                + ")");
        TaskType type = latestTask.getType();
        switch (type) {
            case WORK:
                labelCurrentTask.setTextFill(Color.rgb(0, 0, 180));
                break;
            case BREAK:
                labelCurrentTask.setTextFill(Color.rgb(255, 0, 0));
                break;
            case ENDDAY:
                labelCurrentTask.setTextFill(Color.rgb(0, 125, 0));
                break;
        }
    }
}