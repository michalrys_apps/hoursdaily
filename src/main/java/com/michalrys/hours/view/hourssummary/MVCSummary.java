package com.michalrys.hours.view.hourssummary;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalTime;

public interface MVCSummary {
    interface Controller {
        void setUpController(File publicPoint);

        void setModelInView();

        void getDataFromModelAndSetupWindow();

        void setPreviousDay();

        void setNextDay();

        void closeDay();

        void setLeavingTime();

        void createExcelSummary();

        void archiveOfHourTaskFiles(double x, double y, String currentWorkWeek);
    }

    interface View {
        void setLeavingTime();

        void initData();

        void setModel(HourTasksSummary modelTasksSummary);

        void setFDate(LocalDate currentDay);

        void setWW(String ww);

        void setEndDayMessage(String endDayMessage);

        void setStartTime(LocalTime startTime);

        void setEndTime(LocalTime endTime);

        void setTotalTime(LocalTime totalTime);

        void setWorkTime(LocalTime workTime);

        void setBreakTime(LocalTime breakTime);

        void setSummary(String summary);

        void setSummaryDetails(String summaryDetails);

        void updateButtonCloseDay();

        void setLabelWorkPercentage(LocalTime workTime, LocalTime totalTime);

        void setLabelBreakPercentage(LocalTime breakTime, LocalTime totalTime);

        boolean isRbTimeStyleHSelected();

        void setLatestTaskFromCurrentDay(HourTask latestTask);
    }
}
