/*
Copyright (C) 2019-2022 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>.
Complied version of this project, written in jar file, can be used only up to 31.12.2021,
distributed to interested persons only by author of this application.
This is for now - it will be updated soon.
*/
package com.michalrys.hours;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.IntellitypeListener;
import com.melloware.jintellitype.JIntellitype;
import com.michalrys.hours.systemtrayicons.SystemTrayIcons;
import com.michalrys.hours.view.hoursdaily.MVCDaily;
import com.michalrys.hours.view.hoursdaily.WindowHoursDaily;
import com.michalrys.hours.view.hourssummary.MVCSummary;
import com.michalrys.hours.view.hourssummary.WindowHoursSummary;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/* in this application JIntellitype was used based on following license:
 *
 * JIntellitype
 * -----------------
 * Copyright 2005-2019 Emil A. Lefkof III, Melloware Inc.
 * <p>
 * I always give it my best shot to make a program useful and solid, but
 * remeber that there is absolutely no warranty for using this program as
 * stated in the following terms:
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class RunJavaFxAndSystemTray extends Application implements HotkeyListener, IntellitypeListener {
    private final SystemTray tray = SystemTray.getSystemTray();
    private TrayIcon trayIcon;
    private final URL defaultIconPath = SystemTrayIcons.class.getResource("dailyHour_default.gif");
    private final PopupMenu popup = new PopupMenu();
    private String tooltip = "Daily hours";

    private Stage stageWindowHoursDaily;
    private WindowHoursDaily windowHoursDaily;
    private final URL iconPathForWindowHoursDaily = SystemTrayIcons.class.getResource("dailyHour_animation2.gif");

    private Stage stageWindowHoursSummary;
    private WindowHoursSummary windowHoursSummary;
    private final URL iconPathForWindowHoursSummary = SystemTrayIcons.class.getResource("dailySummary_animation2.gif");

    // sets up the javafx application.
    // a tray icon is setup for the icon, but the main stageWindowHoursDaily remains invisible until the user
    // interacts with the tray icon.
    @Override
    public void start(final Stage stage) {
        // instructs the javafx system not to exit implicitly when the last application window is shut.
        Platform.setImplicitExit(false);

        // sets up the tray icon (using awt code run on the swing thread).
        SwingUtilities.invokeLater(this::addAppToTray);

        // setup stages
        try {
            setUpStageWindowHoursDaily(stage); // be careful with initializations
            setUpStageWindowHoursSummary();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUpStageWindowHoursDaily(Stage stage) throws IOException {
        this.stageWindowHoursDaily = stage;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view/hoursdaily/WindowHoursDaily.fxml"));
        Parent root = fxmlLoader.load();

        windowHoursDaily = fxmlLoader.<WindowHoursDaily>getController();
        windowHoursDaily.setIsInTray(true);

        stage.setTitle("Hours daily");
        stage.setScene(new Scene(root, 365, 134));
        stage.setHeight(170);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);

        PointerInfo pointerInfo = MouseInfo.getPointerInfo();
        Point location = pointerInfo.getLocation();
        stage.setX(location.getX() - 180); //500
        stage.setY(200);

        InputStream resourceAsStream = MVCDaily.class.getResourceAsStream("dailyHour.png");
        stage.getIcons().add(new javafx.scene.image.Image(resourceAsStream));
        resourceAsStream.close();

        try {
            new Robot().mouseMove(
                    (int) stage.getX() + 180,
                    (int) stage.getY() + 80
            );
        } catch (AWTException e) {
            e.printStackTrace();
        }

        stageWindowHoursDaily.hide();
        stageWindowHoursDaily.onHidingProperty().setValue(e -> trayIcon.setImage(new ImageIcon(defaultIconPath, "tray icon").getImage()));
    }

    private void setUpStageWindowHoursSummary() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view/hourssummary/WindowHoursSummary.fxml"));
        Parent root = fxmlLoader.load();
        windowHoursSummary = fxmlLoader.<WindowHoursSummary>getController();

        stageWindowHoursSummary = new Stage();
        stageWindowHoursSummary.setTitle("Hours summary");
        stageWindowHoursSummary.setScene(new Scene(root, 520, 430));
        stageWindowHoursSummary.setResizable(false);
        stageWindowHoursSummary.setAlwaysOnTop(true);

        PointerInfo pointerInfo = MouseInfo.getPointerInfo();
        Point location = pointerInfo.getLocation();
        stageWindowHoursSummary.setX(location.getX() - 180); //500
        stageWindowHoursSummary.setY(100);

        InputStream resourceAsStream = MVCSummary.class.getResourceAsStream("dailySummary.png");
        stageWindowHoursSummary.getIcons().add(new javafx.scene.image.Image(resourceAsStream));

        try {
            new Robot().mouseMove(
                    (int) stageWindowHoursSummary.getX() + 180,
                    (int) stageWindowHoursSummary.getY() + 15
            );
        } catch (AWTException e) {
            e.printStackTrace();
        }

        stageWindowHoursSummary.hide();
        stageWindowHoursSummary.onHidingProperty().setValue(e -> trayIcon.setImage(new ImageIcon(defaultIconPath, "tray icon").getImage()));
    }

    private void addAppToTray() {
        //setup data for system tray
        Image imageForTray = new ImageIcon(defaultIconPath, "tray icon").getImage();
        trayIcon = new TrayIcon(imageForTray);
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip(tooltip);
        if (!SystemTray.isSupported()) {
            throw new SystemTrayNotSupportedException();
        }

        //menus
        MenuItem exit = new MenuItem("Exit");
        MenuItem appSummaryHours = new MenuItem("Summary hours");
        MenuItem appDailyHours = new MenuItem("Daily hours");

        //Add components to pop-up menu
        popup.add(exit);
        popup.add(appSummaryHours);
        popup.add(appDailyHours);
        trayIcon.setPopupMenu(popup);

        // add shortcuts for Windows only
        //JIntellitype.getInstance().cleanUp();
        JIntellitype.getInstance().addHotKeyListener(this);
        JIntellitype.getInstance().registerHotKey(1, JIntellitype.MOD_ALT, (int) KeyEvent.VK_F1);
        JIntellitype.getInstance().registerHotKey(2, JIntellitype.MOD_ALT, (int) KeyEvent.VK_F2);

        //actions
        appDailyHours.addActionListener(event -> {
            Platform.runLater(this::showStageWindowHoursDaily);
        });

        appSummaryHours.addActionListener(event -> {
            Platform.runLater(this::showStageWindowHoursSummary);
        });

        exit.addActionListener(e -> System.exit(0));

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the application stageWindowHoursDaily and ensures that it is brought ot the front of all stages.
     */
    private void showStageWindowHoursDaily() {
        if (stageWindowHoursDaily != null && !stageWindowHoursDaily.isShowing()) {
            trayIcon.setImage(new ImageIcon(iconPathForWindowHoursDaily, "tray icon").getImage());

            windowHoursDaily.initData();
            stageWindowHoursDaily.show();
            stageWindowHoursDaily.toFront();
        }
    }

    private void showStageWindowHoursSummary() {
        if (stageWindowHoursSummary != null && !stageWindowHoursSummary.isShowing()) {
            trayIcon.setImage(new ImageIcon(iconPathForWindowHoursSummary, "tray icon").getImage());

            windowHoursSummary.initData();
            stageWindowHoursSummary.show();
            stageWindowHoursSummary.toFront();
        }
    }

    @Override
    public void onHotKey(int identifier) {
        if (identifier == 1) {
            Platform.runLater(this::showStageWindowHoursDaily);
        } else if (identifier == 2) {
            Platform.runLater(this::showStageWindowHoursSummary);
        }
    }

    @Override
    public void onIntellitype(int command) {
        //here shall be some special functional keys
    }

    public static void main(String[] args) throws IOException, AWTException {
        // Just launches the JavaFX application.
        // Due to way the application is coded, the application will remain running
        // until the user selects the Exit menu option from the tray icon.
        if (isLicenseCorrect()) {
            launch(args);
        } else {
            JOptionPane.showMessageDialog(null,
                    "This version was for tests only.\n\nAsk Michał Ryś about updates.\n",
                    "License is out",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    private static boolean isLicenseCorrect() {
        LocalDate licenseEndDate = LocalDate.parse("2023-01-10", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return LocalDate.now().isBefore(licenseEndDate);
    }
}