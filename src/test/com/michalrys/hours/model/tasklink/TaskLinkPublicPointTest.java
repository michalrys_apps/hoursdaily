/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.tasklink;

import com.michalrys.hours.tools.JSONMySimplestParser;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TaskLinkPublicPointTest {

    private static final String PUBLIC_POINT_PATH_FOR_TESTS = "d:\\temp_public_point";

    @Test
    public void shouldSetPublicPointAndPathToTaskLinkFile() {
        //given
        String fileName = "TaskLink.json";

        TaskLinkContainer taskLink = new TaskLinkPublicPoint(PUBLIC_POINT_PATH_FOR_TESTS);
        //when
        String publicPoint = taskLink.getPublicPointFilePath();
        String filePath = taskLink.getTaskLinkFilePath();

        //then
        Assert.assertEquals(PUBLIC_POINT_PATH_FOR_TESTS, publicPoint);
        Assert.assertEquals(PUBLIC_POINT_PATH_FOR_TESTS + "\\" + fileName, filePath);
    }

    @Test
    public void shouldGetEmptyLinksForGivenListOfTaskWhenNoTasksInLinkTaskFile() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointForTests();

        List<String> listOfTasksNotExistingInTaskLinkFile = Arrays.asList("Task 1", "Task 2", "Task 3");
        TaskLinkContainer taskLink = new TaskLinkPublicPoint(PUBLIC_POINT_PATH_FOR_TESTS);

        //when
        Map<String, String> tasksLinks = taskLink.get(listOfTasksNotExistingInTaskLinkFile);

        //then
        Assert.assertTrue(tasksLinks.containsKey("Task 1"));
        Assert.assertTrue(tasksLinks.containsKey("Task 2"));
        Assert.assertTrue(tasksLinks.containsKey("Task 3"));
        Assert.assertEquals("c:\\", tasksLinks.get("Task 1"));
        Assert.assertEquals("c:\\", tasksLinks.get("Task 2"));
        Assert.assertEquals("c:\\", tasksLinks.get("Task 3"));
    }

    @Test
    public void shouldGetExistingLinksAndNotExisting() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointForTests();
        createTaskLinkExampleForTest();

        List<String> listOfTasksNotExistingInTaskLinkFile = Arrays
                .asList("Task 1", "Task 2", "Task 3", "Task 4", "Task 5", "Task 6");
        TaskLinkContainer taskLink = new TaskLinkPublicPoint(PUBLIC_POINT_PATH_FOR_TESTS);

        //when
        Map<String, String> tasksLinks = taskLink.get(listOfTasksNotExistingInTaskLinkFile);

        //then
        Assert.assertTrue(tasksLinks.containsKey("Task 1"));
        Assert.assertTrue(tasksLinks.containsKey("Task 2"));
        Assert.assertTrue(tasksLinks.containsKey("Task 3"));
        Assert.assertTrue(tasksLinks.containsKey("Task 4"));
        Assert.assertTrue(tasksLinks.containsKey("Task 5"));
        Assert.assertTrue(tasksLinks.containsKey("Task 6"));
        Assert.assertEquals("c:\\test.txt", tasksLinks.get("Task 1"));
        Assert.assertEquals("d:\\test.txt", tasksLinks.get("Task 2"));
        Assert.assertEquals("d:\\test.txt", tasksLinks.get("Task 3"));
        Assert.assertEquals("d:\\some wrong path", tasksLinks.get("Task 4"));
        Assert.assertEquals("c:\\", tasksLinks.get("Task 5"));
        Assert.assertEquals("c:\\", tasksLinks.get("Task 6"));
    }

    @Test
    public void shouldGetTasksLinksOnlyForGivenListOfTask() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointForTests();
        createTaskLinkExampleForTest();

        List<String> listOfTasksNotExistingInTaskLinkFile = Arrays
                .asList("Task 1", "Task 2");
        TaskLinkContainer taskLink = new TaskLinkPublicPoint(PUBLIC_POINT_PATH_FOR_TESTS);

        //when
        Map<String, String> tasksLinks = taskLink.get(listOfTasksNotExistingInTaskLinkFile);

        //then
        System.out.println(tasksLinks);

        Assert.assertTrue(tasksLinks.containsKey("Task 1"));
        Assert.assertTrue(tasksLinks.containsKey("Task 2"));
        Assert.assertFalse(tasksLinks.containsKey("Task 3"));
        Assert.assertFalse(tasksLinks.containsKey("Task 4"));
        Assert.assertFalse(tasksLinks.containsKey("Task 5"));
        Assert.assertFalse(tasksLinks.containsKey("Task 6"));
        Assert.assertEquals("c:\\test.txt", tasksLinks.get("Task 1"));
        Assert.assertEquals("d:\\test.txt", tasksLinks.get("Task 2"));
    }

    @Test
    public void shouldUpdateLinkForGivenTaskName() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointForTests();
        createTaskLinkExampleForTest();

        List<String> listOfTasksNotExistingInTaskLinkFile = Arrays
                .asList("Task 1", "Task 2");
        TaskLinkContainer taskLink = new TaskLinkPublicPoint(PUBLIC_POINT_PATH_FOR_TESTS);

        //when
        taskLink.update("Task 1", "c:\\updated\\test.txt");
        Map<String, String> tasksLinks = taskLink.get(listOfTasksNotExistingInTaskLinkFile);

        //then
        System.out.println(tasksLinks);

        Assert.assertTrue(tasksLinks.containsKey("Task 1"));
        Assert.assertTrue(tasksLinks.containsKey("Task 2"));
        Assert.assertFalse(tasksLinks.containsKey("Task 3"));
        Assert.assertFalse(tasksLinks.containsKey("Task 4"));
        Assert.assertFalse(tasksLinks.containsKey("Task 5"));
        Assert.assertFalse(tasksLinks.containsKey("Task 6"));
        Assert.assertEquals("c:\\updated\\test.txt", tasksLinks.get("Task 1"));
        Assert.assertEquals("d:\\test.txt", tasksLinks.get("Task 2"));
    }

    private void createTaskLinkExampleForTest() {
        JSONObject json = new JSONObject();

        json.put("Task 1", new File("c:/test.txt").getAbsolutePath());
        json.put("Task 2", new File("d:/test.txt").getAbsolutePath());
        json.put("Task 3", new File("d:/test.txt").getAbsolutePath());
        json.put("Task 4", new File("d:/some wrong path").getAbsolutePath());

        File publicPoint = new File(PUBLIC_POINT_PATH_FOR_TESTS);
        File taskLinkFile = new File(publicPoint.getAbsoluteFile() + "/TaskLink.json");

        JSONMySimplestParser.write(taskLinkFile, json);
    }

    private void createPublicPointForTests() {
        File publicPointFile = new File(PUBLIC_POINT_PATH_FOR_TESTS);
        publicPointFile.mkdirs();
    }

    private void deleteTemporaryPublicPoint() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH_FOR_TESTS);

        //FIXME comment if you want to see these files
        File[] files = temporaryPublicPoint.listFiles();
        try {
            for (File file : files) {
                file.delete();
            }
        } catch (Exception e) {
            System.out.println("(TEST: There was no files)");
        }
        temporaryPublicPoint.delete();
    }
}