/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.tools.TimestampFormatter;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.time.LocalDateTime;

public class HourTaskTest {
    private static final String KEY_TYPE = "type";
    private static final String KEY_TIME_STAMP = "timeStamp";
    private static final String KEY_TASK = "task";
    private static final String KEY_DESCRIPTION = "description";

    private final static String PUBLIC_POINT_PATH = "d:/temp_public_point";

    @Test
    public void shouldHaveProperType() {
        //given
        HourTask hourTaskWork = new HourTaskImpl(TaskType.WORK, PUBLIC_POINT_PATH);
        HourTask hourTaskBreak = new HourTaskImpl(TaskType.BREAK, PUBLIC_POINT_PATH);
        HourTask hourTasEndday = new HourTaskImpl(TaskType.ENDDAY, PUBLIC_POINT_PATH);

        //when
        TaskType hourTaskWorkType = hourTaskWork.getType();
        TaskType hourTaskBreakType = hourTaskBreak.getType();
        TaskType hourTasEnddayType = hourTasEndday.getType();

        //then
        Assert.assertEquals(TaskType.WORK, hourTaskWorkType);
        Assert.assertEquals(TaskType.BREAK, hourTaskBreakType);
        Assert.assertEquals(TaskType.ENDDAY, hourTasEnddayType);
    }

    @Test
    public void shouldSetTimeStamp() {
        //given
        LocalDateTime givenDateTime = LocalDateTime.parse("2019-09-15,22:14:10", TimestampFormatter.getDateTime());
        HourTask hourTaskWork = new HourTaskImpl(TaskType.WORK, PUBLIC_POINT_PATH);
        HourTask hourTaskBreak = new HourTaskImpl(TaskType.BREAK, PUBLIC_POINT_PATH);
        HourTask hourTasEndday = new HourTaskImpl(TaskType.ENDDAY, PUBLIC_POINT_PATH);

        hourTaskWork.setTimeStamp(givenDateTime);
        hourTaskBreak.setTimeStamp(givenDateTime);
        hourTasEndday.setTimeStamp(givenDateTime);

        //when
        LocalDateTime resultDateTimeWork = hourTaskWork.getTimeStamp();
        LocalDateTime resultDateTimeBreak = hourTaskBreak.getTimeStamp();
        LocalDateTime resultDateTimeEndday = hourTasEndday.getTimeStamp();

        //then
        Assert.assertEquals(givenDateTime, resultDateTimeWork);
        Assert.assertEquals(givenDateTime, resultDateTimeBreak);
        Assert.assertEquals(givenDateTime, resultDateTimeEndday);
    }

    @Test
    public void shouldWriteToFileForGivenPublicPoint() {
        //given
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();

        HourTask hourTaskWork = new HourTaskImpl(TaskType.WORK, PUBLIC_POINT_PATH);
        HourTask hourTaskBreak = new HourTaskImpl(TaskType.BREAK, PUBLIC_POINT_PATH);
        HourTask hourTasEndday = new HourTaskImpl(TaskType.ENDDAY, PUBLIC_POINT_PATH);

        LocalDateTime givenDateTime1 = LocalDateTime.parse("2019-09-15,22:14:11", TimestampFormatter.getDateTime());
        hourTaskWork.setTimeStamp(givenDateTime1);

        LocalDateTime givenDateTime2 = LocalDateTime.parse("2019-09-15,22:14:12", TimestampFormatter.getDateTime());
        hourTaskBreak.setTimeStamp(givenDateTime2);

        LocalDateTime givenDateTime3 = LocalDateTime.parse("2019-09-15,22:14:13", TimestampFormatter.getDateTime());
        hourTasEndday.setTimeStamp(givenDateTime3);

        //when
        hourTaskWork.write();
        hourTaskBreak.write();
        hourTasEndday.write();

        //then
        File file1 = new File(PUBLIC_POINT_PATH + "/" + "HourTask_2019-09-15_22-14-11.json");
        File file2 = new File(PUBLIC_POINT_PATH + "/" + "HourTask_2019-09-15_22-14-12.json");
        File file3 = new File(PUBLIC_POINT_PATH + "/" + "HourTask_2019-09-15_22-14-13.json");
        System.out.println(file1.getAbsolutePath());

        Assert.assertTrue(file1.exists());
        Assert.assertTrue(file2.exists());
        Assert.assertTrue(file3.exists());

        file1.delete();
        file2.delete();
        file3.delete();
        temporaryPublicPoint.delete();
    }

    @Test
    public void shouldWriteToFileDateTaskTypeAndDescriptionForGivenPublicPoint() {
        //given
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();

        HourTask hourTaskWork = new HourTaskImpl(TaskType.WORK, PUBLIC_POINT_PATH);
        HourTask hourTaskBreak = new HourTaskImpl(TaskType.BREAK, PUBLIC_POINT_PATH);
        HourTask hourTasEndday = new HourTaskImpl(TaskType.ENDDAY, PUBLIC_POINT_PATH);

        LocalDateTime givenDateTime1 = LocalDateTime.parse("2019-09-15,22:14:11", TimestampFormatter.getDateTime());
        hourTaskWork.setTimeStamp(givenDateTime1);

        LocalDateTime givenDateTime2 = LocalDateTime.parse("2019-09-15,22:14:12", TimestampFormatter.getDateTime());
        hourTaskBreak.setTimeStamp(givenDateTime2);

        LocalDateTime givenDateTime3 = LocalDateTime.parse("2019-09-15,22:14:13", TimestampFormatter.getDateTime());
        hourTasEndday.setTimeStamp(givenDateTime3);

        hourTaskWork.setTask("task1");
        hourTaskBreak.setTask("task2");
        hourTasEndday.setTask("task3");

        hourTaskWork.setDescription("to do 1");
        hourTaskBreak.setDescription("to do 2");
        hourTasEndday.setDescription("to do 3");

        //when
        hourTaskWork.write();
        hourTaskBreak.write();
        hourTasEndday.write();

        //then
        File file1 = new File(PUBLIC_POINT_PATH + "/" + "HourTask_2019-09-15_22-14-11.json");
        File file2 = new File(PUBLIC_POINT_PATH + "/" + "HourTask_2019-09-15_22-14-12.json");
        File file3 = new File(PUBLIC_POINT_PATH + "/" + "HourTask_2019-09-15_22-14-13.json");

        JSONObject file1Json = readJsonFile(file1);
        JSONObject file2Json = readJsonFile(file2);
        JSONObject file3Json = readJsonFile(file3);

        Assert.assertTrue(file1.exists());
        Assert.assertTrue(file2.exists());
        Assert.assertTrue(file3.exists());
        Assert.assertEquals(TaskType.WORK.toString(), file1Json.get(KEY_TYPE));
        Assert.assertEquals(TaskType.BREAK.toString(), file2Json.get(KEY_TYPE));
        Assert.assertEquals(TaskType.ENDDAY.toString(), file3Json.get(KEY_TYPE));
        Assert.assertEquals("task1", file1Json.get(KEY_TASK));
        Assert.assertEquals("task2", file2Json.get(KEY_TASK));
        Assert.assertEquals("task3", file3Json.get(KEY_TASK));
        Assert.assertEquals("2019-09-15,22:14:11", file1Json.get(KEY_TIME_STAMP));
        Assert.assertEquals("2019-09-15,22:14:12", file2Json.get(KEY_TIME_STAMP));
        Assert.assertEquals("2019-09-15,22:14:13", file3Json.get(KEY_TIME_STAMP));
        Assert.assertEquals("to do 1", file1Json.get(KEY_DESCRIPTION));
        Assert.assertEquals("to do 2", file2Json.get(KEY_DESCRIPTION));
        Assert.assertEquals("to do 3", file3Json.get(KEY_DESCRIPTION));

        //FIXME comment if you want to see these files
        File[] files = temporaryPublicPoint.listFiles();
        for(File file :files) {
            file.delete();
        }
        temporaryPublicPoint.delete();
    }

    private JSONObject readJsonFile(File file) {
        StringBuffer result = new StringBuffer();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject(result.toString());
    }
}