/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummaryAll;
import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DayDTOTest {
    private final String PUBLIC_POINT_PATH_FOR_TEST_ONLY = "d:/temp_public_point";
    private static final String WORK_TYPE_OF_TASKS = "work";
    private static final String BREAK_TYPE_OF_TASKS = "break";

    @Test
    public void shouldGiveCurrentDay() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        hourTasksSummary.setNextDay();

        LocalDate expectedDay = LocalDate.parse("2018-10-07", DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        //when
        DayDTO dayDTO = DayDTO.getCurrent(hourTasksSummary);
        System.out.println(dayDTO);
        System.out.println(dayDTO.getWorkWeek());
        System.out.println(dayDTO.getCurrentDay());
        System.out.println(dayDTO.getStartTime());
        System.out.println(dayDTO.getEndTime());
        System.out.println(dayDTO.getTotalTime());
        System.out.println(dayDTO.getTotalHours());
        System.out.println(dayDTO.getWorkHours());
        System.out.println(dayDTO.getBreakHours());

        //then
        Assert.assertEquals(expectedDay, dayDTO.getCurrentDay());
    }

    private void preparePublicPoint(String publicPointPath) {
        File publicPoint = new File(publicPointPath);
        File[] files = publicPoint.listFiles();
        for (File file : files) {
            file.delete();
        }
        publicPoint.delete();
        publicPoint.mkdirs();
    }

    private List<LocalDate> createDummyHourTasks(String publicPointPath) {
        //day 1
        String dayStart1 = "2018-10-07,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,06:01:02",
                "12663 Important Client",
                "preparing mesh");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,08:01:42",
                "12663 Important Client",
                "making report");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-07,12:01:36",
                "kitchen",
                "supper");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,12:30:24",
                "15332 Important Client",
                "thinking");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,17:03:01",
                "12663 Important Client",
                "");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-07,17:08:01",
                "12663 Important Client",
                "");
        // day 2
        String dayStart2 = "2018-10-11,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,06:01:00",
                "12663 Important Client",
                "creating mock");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,09:01:42",
                "12663 Important Client",
                "telco");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-11,11:01:36",
                "kitchen",
                "eating sth");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,11:40:24",
                "15332 Important Client",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,15:55:01",
                "12663 Important Client",
                "sth else");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-11,19:03:55",
                "12663 Important Client",
                "FINITo");
        // day 3
        String dayStart3 = "2018-10-12,04:48:20";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,04:48:20",
                "78440 new super project",
                "meshing");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,05:55:52",
                "12663 Important Client",
                "discussion");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-12,10:12:36",
                "kitchen",
                "important meeting");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,11:10:24",
                "78440 new super project",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,15:23:01",
                "12663 Important Client",
                "i dont know");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-12,18:23:01",
                "12663 Important Client",
                "nothing special");

        return Arrays.asList(
                LocalDate.parse(dayStart1, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart2, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart3, TimestampFormatter.getDateTime())
        );
    }

    private void writeExampleHourTask(TaskType type, String publicPoint, String dateTime, String task, String description) {
        HourTask hourTaskToWrite;
        hourTaskToWrite = new HourTaskImpl(type, publicPoint);
        hourTaskToWrite.setTimeStamp(LocalDateTime.parse(dateTime, TimestampFormatter.getDateTime()));
        hourTaskToWrite.setTask(task);
        hourTaskToWrite.setDescription(description);
        hourTaskToWrite.write();
    }
}