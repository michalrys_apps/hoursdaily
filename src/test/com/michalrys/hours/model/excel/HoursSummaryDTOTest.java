/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummaryAll;
import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

public class HoursSummaryDTOTest {
    private final String PUBLIC_POINT_PATH_FOR_TEST_ONLY = "d:/temp_public_point";
    private static final String WORK_TYPE_OF_TASKS = "work";
    private static final String BREAK_TYPE_OF_TASKS = "break";

    @Test
    public void shouldGiveCorrectListOfWorkWeeks() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<Integer> expectedWorkWeeks = new ArrayList<>();
        expectedWorkWeeks.add(40);
        expectedWorkWeeks.add(41);

        //when
        List<Integer> workWeeks = hoursSummaryDTO.getWorkWeeks();
        System.out.println(workWeeks);

        //then
        Assert.assertEquals(expectedWorkWeeks, workWeeks);
    }

    @Test
    public void shouldGiveAllDays() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<LocalDate> expectedDays = new ArrayList<>();
        expectedDays.add(LocalDate.parse("2018-10-07", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        expectedDays.add(LocalDate.parse("2018-10-11", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        expectedDays.add(LocalDate.parse("2018-10-12", DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        System.out.println(hoursSummaryDTO);

        //when
        List<DayDTO> days = hoursSummaryDTO.getDays();
        System.out.println("Days DTO = " + days);

        List<LocalDate> daysDates = new ArrayList<>();
        for (DayDTO dayDTO : days) {
            daysDates.add(dayDTO.getCurrentDay());
        }
        System.out.println("Days dates = " + daysDates);

        //then
//        Assert.assertEquals(expectedDays, days);
        Assert.assertEquals(expectedDays, daysDates);
    }

    @Test
    public void shouldGiveDaysForDefinedWorkWeek() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<LocalDate> expectedDays = new ArrayList<>();
        expectedDays.add(LocalDate.parse("2018-10-11", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        expectedDays.add(LocalDate.parse("2018-10-12", DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        //when
        List<DayDTO> daysFor41WorkWeek = hoursSummaryDTO.getDays(41);
        System.out.println("DaysDTO for 41 week: " + daysFor41WorkWeek);

        List<LocalDate> daysFor41WorkWeekAsDate = new ArrayList<>();
        for (DayDTO dayDTO : daysFor41WorkWeek) {
            daysFor41WorkWeekAsDate.add(dayDTO.getCurrentDay());
        }
        System.out.println("Days as date for 41 week: " + daysFor41WorkWeekAsDate);

        //then
//        Assert.assertEquals(expectedDays, daysFor41WorkWeek);
        Assert.assertEquals(expectedDays, daysFor41WorkWeekAsDate);
    }

    @Test
    public void shouldGiveCorrectMainWorkTasksForGivenWorkWeek() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("12663 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("15332 Important Client", 7.7));

        //when
        List<HourTaskDTO> mainTasksDTO = hoursSummaryDTO.getMainTasks(WORK_TYPE_OF_TASKS, 40);

        for (HourTaskDTO task : mainTasksDTO) {
            System.out.println(task.getName() + " --> " + task.getTime()); //TODO ends here
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, mainTasksDTO);
    }

    @Test
    public void shouldGiveCorrectAllWorkMainTasksAndSubTasksForGivenWorkWeek() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("12663 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("making report", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("preparing mesh", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("15332 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("thinking", 7.7));

        //when
        List<HourTaskDTO> allWorkTasks = hoursSummaryDTO.getMainAndSubTasks(WORK_TYPE_OF_TASKS, 40);

        for (HourTaskDTO task : allWorkTasks) {
            System.out.println(task.getName() + " --> " + task.getTime()); //TODO ends here
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, allWorkTasks);
    }

    @Test
    public void shouldGiveCorrectAllWorkMainTasksAndSubTasksForGivenWorkWeekExample2() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("12663 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("creating mock", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("discussion", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("i dont know", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("sth else", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("telco", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("15332 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("report", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("78440 new super project", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("meshing", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("report", 7.7));

        //when
        List<HourTaskDTO> allWorkTasks = hoursSummaryDTO.getMainAndSubTasks(WORK_TYPE_OF_TASKS, 41);

        for (HourTaskDTO task : allWorkTasks) {
            System.out.println(task.getName() + " --> " + task.getTime()); //TODO ends here
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, allWorkTasks);
    }

    @Test
    public void shouldGiveCorrectMainBreakTasksForGivenWorkWeek() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("kitchen", 7.7));

        //when
        List<HourTaskDTO> mainBreakTasksDTO = hoursSummaryDTO.getMainTasks(BREAK_TYPE_OF_TASKS, 40);

        System.out.println(mainBreakTasksDTO);

        for (HourTaskDTO task : mainBreakTasksDTO) {
            System.out.println(task.getName() + " --> " + task.getTime()); //TODO ends here
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, mainBreakTasksDTO);
    }

    @Test
    public void shouldGiveCorrectAllBreakMainTasksAndSubTasksForGivenWorkWeek() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("kitchen", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("supper", 7.7));

        //when
        List<HourTaskDTO> allBreakTasks = hoursSummaryDTO.getMainAndSubTasks(BREAK_TYPE_OF_TASKS, 40);

        System.out.println(hoursSummaryDTO.getMainTasks(BREAK_TYPE_OF_TASKS, 40));
        for (HourTaskDTO task : allBreakTasks) {
            System.out.println(task.getName() + " --> " + task.getTime());
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, allBreakTasks);
    }

    @Test
    public void shouldGiveCorrectWorkMainTasksForGivenWorkWeekAndGivenDay() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("12663 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("15332 Important Client", 7.7));

        DayDTO dayDTOforHours = new DayDTO();
        dayDTOforHours.setCurrentDay(LocalDate.parse("2018-10-11", DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        //when
        List<HourTaskDTO> workTasksForDay = hoursSummaryDTO.getMainTasks(WORK_TYPE_OF_TASKS, 41, dayDTOforHours);

        System.out.println("===");
        for (HourTaskDTO task : expectedHourTasksDTO) {
            System.out.println(task.getName() + " --> " + task.getTime());
        }
        System.out.println("===");
        for (HourTaskDTO task : workTasksForDay) {
            System.out.println(task.getName() + " --> " + task.getTime());
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, workTasksForDay);
    }

    @Test
    public void shouldGiveCorrectWorkMainTasksAndSubTasksForGivenWorkWeekAndGivenDay() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        HoursSummaryDTO hoursSummaryDTO = new HoursSummaryDTO(hourTasksSummary);

        List<HourTaskDTO> expectedHourTasksDTO = new ArrayList<>();
        expectedHourTasksDTO.add(new HourTaskDTO("12663 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("creating mock", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("sth else", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("telco", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("15332 Important Client", 7.7));
        expectedHourTasksDTO.add(new HourTaskDTO("report", 7.7));

        DayDTO dayDTOforHours = new DayDTO();
        dayDTOforHours.setCurrentDay(LocalDate.parse("2018-10-11", DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        //when
        List<HourTaskDTO> workTasksForDay = hoursSummaryDTO.getMainAndSubTasks(WORK_TYPE_OF_TASKS, 41, dayDTOforHours);

        System.out.println("===");
        for (HourTaskDTO task : expectedHourTasksDTO) {
            System.out.println(task.getName() + " --> " + task.getTime());
        }
        System.out.println("===");
        for (HourTaskDTO task : workTasksForDay) {
            System.out.println(task.getName() + " --> " + task.getTime());
        }

        //then
        Assert.assertEquals(expectedHourTasksDTO, workTasksForDay);
    }


    private void preparePublicPoint(String publicPointPath) {
        File publicPoint = new File(publicPointPath);
        File[] files = publicPoint.listFiles();
        for (File file : files) {
            file.delete();
        }
        publicPoint.delete();
        publicPoint.mkdirs();
    }

    private List<LocalDate> createDummyHourTasks(String publicPointPath) {
        //day 1
        String dayStart1 = "2018-10-07,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,06:01:00",
                "12663 Important Client",
                "preparing mesh");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,08:01:42",
                "12663 Important Client",
                "making report");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-07,12:01:36",
                "kitchen",
                "supper");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,12:30:24",
                "15332 Important Client",
                "thinking");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,17:03:01",
                "12663 Important Client",
                "");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-07,17:08:01",
                "12663 Important Client",
                "");
        // day 2
        String dayStart2 = "2018-10-11,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,06:01:00",
                "12663 Important Client",
                "creating mock");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,09:01:42",
                "12663 Important Client",
                "telco");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-11,11:01:36",
                "kitchen",
                "eating sth");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,11:40:24",
                "15332 Important Client",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,15:55:01",
                "12663 Important Client",
                "sth else");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-11,19:03:55",
                "12663 Important Client",
                "FINITo");
        // day 3
        String dayStart3 = "2018-10-12,04:48:20";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,04:48:20",
                "78440 new super project",
                "meshing");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,05:55:52",
                "12663 Important Client",
                "discussion");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-12,10:12:36",
                "kitchen",
                "important meeting");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,11:10:24",
                "78440 new super project",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,15:23:01",
                "12663 Important Client",
                "i dont know");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-12,18:23:01",
                "12663 Important Client",
                "nothing special");

        return Arrays.asList(
                LocalDate.parse(dayStart1, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart2, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart3, TimestampFormatter.getDateTime())
        );
    }

    private void writeExampleHourTask(TaskType type, String publicPoint, String dateTime, String task, String description) {
        HourTask hourTaskToWrite;
        hourTaskToWrite = new HourTaskImpl(type, publicPoint);
        hourTaskToWrite.setTimeStamp(LocalDateTime.parse(dateTime, TimestampFormatter.getDateTime()));
        hourTaskToWrite.setTask(task);
        hourTaskToWrite.setDescription(description);
        hourTaskToWrite.write();
    }
}