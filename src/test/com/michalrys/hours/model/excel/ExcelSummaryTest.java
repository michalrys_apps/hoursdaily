/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummary;
import com.michalrys.hours.model.hourtask.summary.HourTasksSummaryAll;
import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class ExcelSummaryTest {
    private final String PUBLIC_POINT_PATH_FOR_TEST_ONLY = "d:/temp_public_point";

    @Test
    public void shouldGetDaysUsedInFileSummary() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        List<LocalDate> expectedDays = createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        FileSummary summary = new ExcelSummary(hourTasksSummary);

        //when
        List<LocalDate> days = summary.getDays();

        //then
        Assert.assertEquals(expectedDays, days);
    }

    @Test
    public void shouldCreateSummaryExcelFile() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        String dateTime = LocalDateTime.now().format(TimestampFormatter.getDateTimeForFileName());
        String username = System.getProperty("user.name");
        File fileExcel = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY
                + "/DailyHoursSummary_" + dateTime + "_" + username + ".xlsx");

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        FileSummary summary = new ExcelSummary(hourTasksSummary);
        //when
        summary.writeFile();

        //then
        Assert.assertTrue(fileExcel.exists());
    }

    @Test
    public void shouldCreateSummaryExcelFileWithExpectedContent() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasks(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        String dateTime = LocalDateTime.now().format(TimestampFormatter.getDateTimeForFileName());
        String username = System.getProperty("user.name");
        File fileExcel = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY
                + "/DailyHoursSummary_" + dateTime + "_" + username + ".xlsx");

        HourTasksSummary hourTasksSummary = new HourTasksSummaryAll(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        FileSummary summary = new ExcelSummary(hourTasksSummary);

        String fileContent = readSummaryExcelFile();
        String expectedContent = getExpectedFileContentBasedOnExampleHourTaskFiles();

        //when
        summary.writeFile();

        //then
        //Assert.assertEquals(fileContent, expectedContent);
    }

    private String readSummaryExcelFile() {
        //TODO do it later
        return "TODO: here should be real read file content";
    }

    private String getExpectedFileContentBasedOnExampleHourTaskFiles() {
        //TODO do it later
        return "TODO: here should be expected file content";
    }


    private void preparePublicPoint(String publicPointPath) {
        File publicPoint = new File(publicPointPath);
        File[] files = publicPoint.listFiles();
        for (File file : files) {
            file.delete();
        }
        publicPoint.delete();
        publicPoint.mkdirs();
    }

    private List<LocalDate> createDummyHourTasks(String publicPointPath) {
        //day 1
        String dayStart1 = "2018-10-07,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,06:01:00",
                "12663 Important Client",
                "preparing mesh");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,08:01:42",
                "12663 Important Client",
                "making report");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-07,12:01:36",
                "kitchen",
                "supper");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,12:30:24",
                "15332 Important Client",
                "thinking");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,17:03:01",
                "12663 Important Client",
                "");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-07,17:08:01",
                "12663 Important Client",
                "");
        // day 2
        String dayStart2 = "2018-10-11,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,06:01:00",
                "12663 Important Client",
                "creating mock");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,09:01:42",
                "12663 Important Client",
                "telco");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-11,11:01:36",
                "kitchen",
                "eating sth");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,11:40:24",
                "15332 Important Client",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,15:55:01",
                "12663 Important Client",
                "sth else");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-11,19:03:55",
                "12663 Important Client",
                "FINITo");
        // day 3
        String dayStart3 = "2018-10-12,04:48:20";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,04:48:20",
                "78440 new super project",
                "meshing");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,05:55:52",
                "12663 Important Client",
                "discussion");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-12,10:12:36",
                "kitchen",
                "important meeting");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,11:10:24",
                "78440 new super project",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,15:23:01",
                "12663 Important Client",
                "i dont know");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-12,18:23:01",
                "12663 Important Client",
                "nothing special");

        return Arrays.asList(
                LocalDate.parse(dayStart1, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart2, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart3, TimestampFormatter.getDateTime())
        );
    }

    private void writeExampleHourTask(TaskType type, String publicPoint, String dateTime, String task, String description) {
        HourTask hourTaskToWrite;
        hourTaskToWrite = new HourTaskImpl(type, publicPoint);
        hourTaskToWrite.setTimeStamp(LocalDateTime.parse(dateTime, TimestampFormatter.getDateTime()));
        hourTaskToWrite.setTask(task);
        hourTaskToWrite.setDescription(description);
        hourTaskToWrite.write();

        File taskLinksExample = new File(publicPoint + "\\TaskLink.json");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(taskLinksExample))) {
            writer.write("{\n" +
                    "    \"12663 Important Client\": \"d:\\\\test.txt\",\n" +
                    "    \"78440 new super project\": \"c:\\\\Windows\\\\notepad.exe\",\n" +
                    "    \"15332 Important Client\": \"c:\\\\test.txt\"\n" +
                    "}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}