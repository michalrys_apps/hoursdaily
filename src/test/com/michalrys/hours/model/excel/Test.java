/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.excel;

import java.util.*;

public class Test {
    public static void main(String[] args) {
        // 58% | 7.28h | 12663 Important Client
        // 59% | 4.28h | discussion
        // 41% | 3.00h | i dont know
        //
        // 42% | 5.34h | 78440 new super project
        // 21% | 1.13h | meshing
        // 79% | 4.21h | report
        //
        //
        //100% | 0.96h | kitchen
        //100% | 0.96h | important meeting

        String summaryDetails = "" +
                " 58% | 7.28h | 12663 Important Client\n" +
                " 59% | 4.28h | discussion\n" +
                " 59% | 4.00h | discussion\n" +
                " 41% | 3.00h | i dont know\n\n" +
                " 42% | 5.34h | 78440 new super project\n" +
                " 21% | 1.13h | meshing\n" +
                " 79% | 4.21h | report\n\n\n" +
                "100% | 0.96h | kitchen\n" +
                "100% | 0.96h | important meeting\n\n" +
                "100% | 0.96h | INNE\n" +
                "100% | 0.96h | jedzenie kanapki\n" +
                "100% | 0.02h | jedzenie kanapki";

        System.out.println(summaryDetails);
        System.out.println("--------------");

        String[] split = summaryDetails.split("\n");

        Map<HourTaskDTO, Map<HourTaskDTO, Double>> taskWork = new TreeMap<>();
        Map<HourTaskDTO, Map<HourTaskDTO, Double>> taskBreak = new TreeMap<>();

        HourTaskDTO mainHourTaskDTO = null;
        HourTaskDTO hourTaskDTO = null;

        boolean firstLine = true;
        boolean isBrakeTasks = false;
        int separatorCount = 0;

        for (String message : split) {
            if (message.equals("")) {
                firstLine = true;
                separatorCount++;
                continue;
            }
            String[] line = message.split(" \\| ");
            String time = line[1].replaceAll("h", "");
            String name = "";
            if (line.length == 3) {
                name = line[2];
            }

            if (separatorCount == 2) {
                isBrakeTasks = true;
            }

            if (firstLine & !isBrakeTasks) {
                mainHourTaskDTO = new HourTaskDTO(name, Double.valueOf(time));
                Map<HourTaskDTO, Double> subTask = new TreeMap<>();
                subTask.put(mainHourTaskDTO, mainHourTaskDTO.getTime());
                taskWork.put(mainHourTaskDTO, subTask);
            } else if (!firstLine & !isBrakeTasks) {
                hourTaskDTO = new HourTaskDTO(name, Double.valueOf(time));
                Map<HourTaskDTO, Double> subtasks = taskWork.get(mainHourTaskDTO);

                if (subtasks.containsKey(hourTaskDTO)) {
                    Double previousTime = subtasks.getOrDefault(hourTaskDTO, 0.0);
                    subtasks.remove(hourTaskDTO);
                    hourTaskDTO.setTime(hourTaskDTO.getTime() + previousTime);
                }
                subtasks.put(hourTaskDTO, hourTaskDTO.getTime());

                taskWork.put(mainHourTaskDTO, subtasks);

            } else if (firstLine & isBrakeTasks) {
                mainHourTaskDTO = new HourTaskDTO(name, Double.valueOf(time));
                Map<HourTaskDTO, Double> subTask = new TreeMap<>();
                subTask.put(mainHourTaskDTO, mainHourTaskDTO.getTime());
                taskBreak.put(mainHourTaskDTO, subTask);
            } else if (!firstLine & isBrakeTasks) {
                hourTaskDTO = new HourTaskDTO(name, Double.valueOf(time));
                Map<HourTaskDTO, Double> subtasks = taskBreak.get(mainHourTaskDTO);

                if (subtasks.containsKey(hourTaskDTO)) {
                    Double previousTime = subtasks.getOrDefault(hourTaskDTO, 0.0);
                    subtasks.remove(hourTaskDTO);
                    hourTaskDTO.setTime(hourTaskDTO.getTime() + previousTime);
                }

                subtasks.put(hourTaskDTO, hourTaskDTO.getTime());
                taskBreak.put(mainHourTaskDTO, subtasks);
            }

            separatorCount = 0;
            firstLine = false;
        }

        System.out.println("--------");
        System.out.println("=== works ===");
        for (HourTaskDTO mainTask : taskWork.keySet()) {
            System.out.println(mainTask);
            for (HourTaskDTO subTask : taskWork.get(mainTask).keySet()) {
                if (subTask.equals(mainTask)) {
                    continue;
                }
                System.out.println(" - " + subTask);
            }
        }
        System.out.println("=== brakes ===");

        for (HourTaskDTO mainTask : taskBreak.keySet()) {
            System.out.println(mainTask);
            for (HourTaskDTO subTask : taskBreak.get(mainTask).keySet()) {
                if (subTask.equals(mainTask)) {
                    continue;
                }
                System.out.println(" - " + subTask);
            }
        }


    }
}
