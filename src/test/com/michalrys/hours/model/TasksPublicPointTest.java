/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.model.tasks.TaskContainer;
import com.michalrys.hours.model.tasks.TasksPublicPoint;
import com.michalrys.hours.tools.JSONMySimplestParser;
import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TasksPublicPointTest {
    private final static String PUBLIC_POINT_PATH = "d:/temp_public_point";
    private final static String TASKS_FILE_PATH = "d:/temp_public_point/Tasks.json";

    //FIXME hide when you want to see created files for specified test method
    @After
    public void after() {
        deleteTemporaryPublicPoint();
    }

    @Test
    public void shouldReadAllHourTasksFromPublicPoint() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasksWith3DifferentTasks();
        Set<String> expectedTasks = new TreeSet<>();
        expectedTasks.add("task 45");
        expectedTasks.add("task 2");

        TaskContainer allTasks = new TasksPublicPoint(PUBLIC_POINT_PATH);
        allTasks.readTasksFromAllHourTasks();

        //when
        Set<String> tasks = allTasks.get();

        //then
        Assert.assertEquals(expectedTasks, tasks);
    }

    @Test
    public void shouldWriteTaskToPublicPoint() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasksWith3DifferentTasks();

        TaskContainer allTasks = new TasksPublicPoint(PUBLIC_POINT_PATH);
        allTasks.readTasksFromAllHourTasks();

        //when
        allTasks.write();

        //then
        Assert.assertTrue(new File(TASKS_FILE_PATH).exists());
    }

    @Test
    public void shouldReadTasksFromFile() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();
        Set<String> expectedTasks = new TreeSet<>();
        expectedTasks.add("task 45");
        expectedTasks.add("task 2");
        File file = new File(TASKS_FILE_PATH);
        JSONMySimplestParser.write(file, "tasks", expectedTasks);


        TaskContainer allTasks = new TasksPublicPoint(PUBLIC_POINT_PATH);
        allTasks.readTasksFromFile();
        //when

        Set<String> tasks = allTasks.get();

        //then
        Assert.assertEquals(expectedTasks, tasks);
    }

    @Test
    public void shouldReadTasksFromAllHourTasksFromPublicPointWhenTasksFileDoesNotExist() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasksWith3DifferentTasks();
        Set<String> expectedTasks = new TreeSet<>();
        expectedTasks.add("task 45");
        expectedTasks.add("task 2");

        TaskContainer allTasks = new TasksPublicPoint(PUBLIC_POINT_PATH);
        allTasks.read();

        //when
        Set<String> tasks = allTasks.get();

        //then
        Assert.assertEquals(expectedTasks, tasks);
    }

    @Test
    public void shouldSetTasksListToLabelEmptyWhenNothingWasFoundInTasksFile() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();

        File file = new File(TASKS_FILE_PATH);
        Set<String> set = new HashSet<>();
        JSONMySimplestParser.write(file, "tasks", set);

        TaskContainer allTasks = new TasksPublicPoint(PUBLIC_POINT_PATH);
        allTasks.readTasksFromFile();

        //when
        Set<String> tasks = allTasks.get();

        //then
        Set<String> expected = new TreeSet<>();
        expected.add("No tasks yet");
        Assert.assertEquals(expected, tasks);
    }

    private void createPublicPointOnly() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();
    }

    private void createExampleHourTasksWith3DifferentTasks() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();

        File exampleEmptyFolder = new File(PUBLIC_POINT_PATH + "/archive");
        exampleEmptyFolder.mkdirs();

        HourTask hourTaskWork = new HourTaskImpl(TaskType.WORK, PUBLIC_POINT_PATH);
        HourTask hourTaskBreak = new HourTaskImpl(TaskType.BREAK, PUBLIC_POINT_PATH);
        HourTask hourTasEndday = new HourTaskImpl(TaskType.ENDDAY, PUBLIC_POINT_PATH);

        LocalDateTime givenDateTime1 = LocalDateTime.parse("2019-09-15,22:14:11", TimestampFormatter.getDateTime());
        hourTaskWork.setTimeStamp(givenDateTime1);
        hourTaskWork.setTask("task 45");

        LocalDateTime givenDateTime2 = LocalDateTime.parse("2019-09-15,22:14:12", TimestampFormatter.getDateTime());
        hourTaskBreak.setTimeStamp(givenDateTime2);
        hourTaskBreak.setTask("task 2");

        LocalDateTime givenDateTime3 = LocalDateTime.parse("2019-09-15,22:14:13", TimestampFormatter.getDateTime());
        hourTasEndday.setTimeStamp(givenDateTime3);
        hourTasEndday.setTask("task 45");

        //write to public point
        hourTaskWork.write();
        hourTaskBreak.write();
        hourTasEndday.write();
    }

    private void deleteTemporaryPublicPoint() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);

        //FIXME comment if you want to see these files
        File[] files = temporaryPublicPoint.listFiles();
        try {
            for (File file : files) {
                file.delete();
            }
        } catch (Exception e) {
            System.out.println("(TEST: There was no files)");
        }
        temporaryPublicPoint.delete();
    }
}