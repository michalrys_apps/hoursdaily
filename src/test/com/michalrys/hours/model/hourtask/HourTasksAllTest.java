/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask;

import com.michalrys.hours.model.hourtask.container.HourTaskContainer;
import com.michalrys.hours.model.hourtask.container.HourTasksAll;
import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class HourTasksAllTest {
    private final static String PUBLIC_POINT_PATH = "d:/temp_public_point";

    @Test
    public void shouldGetAvailableDaysFromCollectedHourTasks() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();
        createExampleHourTasks();
        List<LocalDate> daysExpected = new ArrayList<>();
        daysExpected.add(LocalDate.parse("2019-09-15", TimestampFormatter.getDate()));
        daysExpected.add(LocalDate.parse("2019-09-14", TimestampFormatter.getDate()));
        daysExpected.add(LocalDate.parse("2019-09-13", TimestampFormatter.getDate()));

        HourTaskContainer hourTasksAll = new HourTasksAll(PUBLIC_POINT_PATH);
        hourTasksAll.read();

        //when
        List<LocalDate> days = hourTasksAll.getDays();

        //then
        Assert.assertEquals(daysExpected, days);
    }

    @Test
    public void shouldGetCurrentDayWhenNoTasksInPublicPoint() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();

        List<LocalDate> daysExpected = new ArrayList<>();
        daysExpected.add(LocalDate.now());

        HourTaskContainer hourTasksAll = new HourTasksAll(PUBLIC_POINT_PATH);
        hourTasksAll.read();

        //when
        List<LocalDate> days = hourTasksAll.getDays();

        //then
        Assert.assertEquals(daysExpected, days);
    }

    @Test
    public void shouldGetDummyHourTaskWhenNoTasksInPublicPoint() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();
        List<HourTask> expectedList = hourTasksWhenNothingInPublicPoint();

        List<LocalDate> daysExpected = new ArrayList<>();
        daysExpected.add(LocalDate.now());

        HourTaskContainer hourTasksAll = new HourTasksAll(PUBLIC_POINT_PATH);
        hourTasksAll.read();
        List<LocalDate> days = hourTasksAll.getDays();

        //when
        List<HourTask> resultHourTasks = hourTasksAll.get(LocalDate.now());

        //then
        Assert.assertEquals(expectedList.get(0).getTimeStamp().format(TimestampFormatter.getDateTime()),
                resultHourTasks.get(0).getTimeStamp().format(TimestampFormatter.getDateTime()));
        Assert.assertEquals(expectedList.get(0).getType(), resultHourTasks.get(0).getType());
        Assert.assertEquals(expectedList.get(0).getDescription(), resultHourTasks.get(0).getDescription());
        Assert.assertEquals(expectedList.get(0).getTask(), resultHourTasks.get(0).getTask());
    }


    private void createPublicPointOnly() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();
    }

    private void createExampleHourTasks() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();

        File exampleEmptyFolder = new File(PUBLIC_POINT_PATH + "/archive");
        exampleEmptyFolder.mkdirs();

        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-13,08:14:11", "12663 Important Client", "preparing mesh");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-13,10:14:11", "13543 Standard Client", "create load cases");
        writeExampleHourTask(TaskType.BREAK, PUBLIC_POINT_PATH,
                "2019-09-13,12:14:11", "Supper", "in kitchen");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-13,13:14:11", "12663 Important Client", "quality check");
        writeExampleHourTask(TaskType.ENDDAY, PUBLIC_POINT_PATH,
                "2019-09-13,20:14:11", "12663 Important Client", "preparing ppt");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-14,07:14:11", "13543 Standard Client", "writing report");
        writeExampleHourTask(TaskType.BREAK, PUBLIC_POINT_PATH,
                "2019-09-14,09:14:11", "Supper", "in snack bar");
        writeExampleHourTask(TaskType.BREAK, PUBLIC_POINT_PATH,
                "2019-09-14,12:14:11", "Tea", "with client, but not only");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-14,15:14:11", "12663 Important Client", "telco");
        writeExampleHourTask(TaskType.ENDDAY, PUBLIC_POINT_PATH,
                "2019-09-14,18:14:11", "13543 Standard Client", "importany meeting");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-15,10:14:11", "12663 Important Client", "finding new solution");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-15,12:14:11", "12663 Important Client", "improving construction");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-15,14:14:11", "13543 Standard Client", "optimization");
    }

    private void writeExampleHourTask(TaskType type, String publicPoint, String dateTime, String task, String description) {
        HourTask hourTaskToWrite;
        hourTaskToWrite = new HourTaskImpl(type, publicPoint);
        hourTaskToWrite.setTimeStamp(LocalDateTime.parse(dateTime, TimestampFormatter.getDateTime()));
        hourTaskToWrite.setTask(task);
        hourTaskToWrite.setDescription(description);
        hourTaskToWrite.write();
    }

    private List<HourTask> hourTasksWhenNothingInPublicPoint() {
        File publicPoint = new File(PUBLIC_POINT_PATH);

        HourTask hourTaskNoTasks = new HourTaskImpl(
                TaskType.ENDDAY, publicPoint.getAbsolutePath());
        hourTaskNoTasks.setTimeStamp(LocalDateTime.now());
        hourTaskNoTasks.setTask("NO TASK YET");
        hourTaskNoTasks.setDescription("In public point: " + publicPoint.getAbsolutePath() + " there are no " +
                "tasks yet. Please run hoursDaily first and generate some hourTask.");
        List<HourTask> noTasks = new ArrayList<>();
        noTasks.add(hourTaskNoTasks);

        return noTasks;
    }

    private void deleteTemporaryPublicPoint() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);

        //FIXME comment if you want to see these files
        File[] files = temporaryPublicPoint.listFiles();
        try {
            for (File file : files) {
                file.delete();
            }
        } catch (Exception e) {
            System.out.println("(TEST: There was no files)");
        }
        temporaryPublicPoint.delete();
    }
}