/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask.summary;

import com.michalrys.hours.model.hourtask.HourTask;
import com.michalrys.hours.model.hourtask.HourTaskImpl;
import com.michalrys.hours.model.hourtask.TaskType;
import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HourTasksSummaryAllTest {
    private final static String PUBLIC_POINT_PATH = "d:/temp_public_point";

    @Test
    public void shouldSetCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalDate currentDayExpected = LocalDate.parse("2019-09-15", TimestampFormatter.getDate());
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);

        //when
        LocalDate currentDay = summary.getCurrentDay();

        //then
        Assert.assertEquals(currentDayExpected, currentDay);
    }

    @Test
    public void shouldSetPreviousDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalDate currentDayExpected = LocalDate.parse("2019-09-14", TimestampFormatter.getDate());
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();

        //when
        LocalDate currentDay = summary.getCurrentDay();

        //then
        Assert.assertEquals(currentDayExpected, currentDay);
    }

    @Test
    public void shouldSetNextDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalDate currentDayExpected = LocalDate.parse("2019-09-13", TimestampFormatter.getDate());
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setNextDay();

        //when
        LocalDate currentDay = summary.getCurrentDay();

        //then
        Assert.assertEquals(currentDayExpected, currentDay);
    }

    @Test
    public void shouldSetActualDayWhenPublicPointIsEmpty() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();

        LocalDate currentDayExpected = LocalDate.now();
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setNextDay();

        //when
        LocalDate currentDay = summary.getCurrentDay();

        //then
        Assert.assertEquals(currentDayExpected, currentDay);
    }

    @Test
    public void shouldGetStartHourOfCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime expectedTime = LocalTime.parse("10:14:11", TimestampFormatter.getTime());
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);

        //when
        LocalTime startHour = summary.getStartTime();

        //then
        Assert.assertEquals(expectedTime, startHour);
    }

    @Test
    public void shouldGetActualHourAsAnEndHourOfCurrentDayWhenThereAreNoEndDayHourTask() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime expectedTime = LocalTime.now();
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);

        //when
        LocalTime endHour = summary.getEndTime();

        //then
        Assert.assertEquals(expectedTime.getHour(), endHour.getHour());
        Assert.assertEquals(expectedTime.getMinute(), endHour.getMinute());
        Assert.assertEquals(expectedTime.getSecond(), endHour.getSecond());
    }

    @Test
    public void shouldSetDummyStartHourAndDummyEndHourWhenThereAreNoTaskHoursInPublicPoint() {
        //given
        deleteTemporaryPublicPoint();
        createPublicPointOnly();

        LocalTime expectedTime = LocalTime.now();
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);

        //when
        LocalTime endHour = summary.getEndTime();
        LocalTime startTime = summary.getStartTime();

        //then
        Assert.assertEquals(expectedTime.getHour(), endHour.getHour());
        Assert.assertEquals(expectedTime.getMinute(), endHour.getMinute());
        Assert.assertEquals(expectedTime.getSecond(), endHour.getSecond());

        Assert.assertEquals(expectedTime.getHour(), startTime.getHour());
        Assert.assertEquals(expectedTime.getMinute(), startTime.getMinute());
        Assert.assertEquals(expectedTime.getSecond(), startTime.getSecond());
    }

    @Test
    public void shouldGetLatestTaskOfCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();
        //when

        HourTask latestHourTask = summary.getLatestHourTask();

        //then
        //"2019-09-14,18:14:11", "13543 Standard Client", "importany meeting"
        Assert.assertEquals("13543 Standard Client", latestHourTask.getTask());
        Assert.assertEquals(LocalDateTime.parse("2019-09-14,18:14:11", TimestampFormatter.getDateTime()),
                latestHourTask.getTimeStamp());
        Assert.assertEquals("importany meeting", latestHourTask.getDescription());
    }

    @Test
    public void shouldGetTotalTimeForCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime timeStart = LocalTime.parse("07:18:12", TimestampFormatter.getTime());
        LocalTime timeEnd = LocalTime.parse("18:14:11", TimestampFormatter.getTime());
        LocalTime expectedTime = getTimeDuration(timeStart, timeEnd);

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();

        //when
        LocalTime totalTime = summary.getTotalTime();

        //then
        Assert.assertEquals(expectedTime, totalTime);
    }

    @Test
    public void shouldGetTimeOfWorkForCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime timeStart = LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        LocalTime timeEnd = LocalTime.parse("04:55:59", TimestampFormatter.getTime());
        LocalTime expectedTime = getTimeDuration(timeStart, timeEnd);

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();

        //when
        LocalTime workTime = summary.getWorkTime();

        //then
        Assert.assertEquals(expectedTime, workTime);
    }

    @Test
    public void shouldGetTimeOfBreakForCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime timeStart = LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        LocalTime timeEnd = LocalTime.parse("06:00:00", TimestampFormatter.getTime());
        LocalTime expectedTime = getTimeDuration(timeStart, timeEnd);

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();

        //when
        LocalTime breakTime = summary.getBreakTime();

        //then
        Assert.assertEquals(expectedTime, breakTime);
    }

    @Test
    public void shouldSetInfoThatDayIsNotClosed() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime expectedTime = LocalTime.now();
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);

        LocalTime endHour = summary.getEndTime();
        //when
        String endDayMessage = summary.getEndDayMessage();

        //then
        Assert.assertEquals("this day is not closed yet", endDayMessage);
    }

    @Test
    public void shouldCloseDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        LocalTime expectedTime = LocalTime.now();
        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);

        LocalTime endHour = summary.getEndTime();
        //when
        summary.closeDay();
        String endDayMessage = summary.getEndDayMessage();

        //then
        Assert.assertEquals("this day is closed", endDayMessage);
    }

    @Test
    public void shouldGetWorkWeekForCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();
        //when
        int currentWorkWeek = summary.getWorkWeek();

        //then
        Assert.assertEquals(37, currentWorkWeek);
    }

    @Test
    public void shouldGetHourTasksSummary() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        summary.setPreviousDay();
        summary.getWorkTime();
        summary.getBreakTime();
        String summaryExpected = " 39% | 3.00h | 12663 Important Client\n" +
                " 61% | 1.93h | 13543 Standard Client\n" +
                "\n" +
                " 50% | 3.00h | Tea\n" +
                " 50% | 3.00h | Supper";
        String summaryDetailsExpected = " 39% | 3.00h | 12663 Important Client\n" +
                "telco\n" +
                "\n" +
                " 61% | 1.93h | 13543 Standard Client\n" +
                "writing report\n" +
                "\n" +
                "\n" +
                " 50% | 3.00h | Tea\n" +
                "with client, but not only\n" +
                "\n" +
                " 50% | 3.00h | Supper\n" +
                "in snack bar";

        //when
        String summaryPrintOut = summary.getSummary();
        String summaryDetailsPrintOut = summary.getSummaryDetails();

        //then
        Assert.assertEquals(summaryExpected, summaryPrintOut);
        Assert.assertEquals(summaryDetailsExpected, summaryDetailsPrintOut);
    }

    @Test
    public void shouldGetListOfTaskForHourTasksCurrentDay() {
        //given
        deleteTemporaryPublicPoint();
        createExampleHourTasks();

        HourTasksSummary summary = new HourTasksSummaryAll(PUBLIC_POINT_PATH);
        List<String> expectedTasks = Arrays.asList("12663 Important Client", "13543 Standard Client");

        //when
        List<String> tasks = summary.getCurrentDayTasks();

        //then
        Assert.assertEquals(expectedTasks, tasks);
    }


    private LocalTime getTimeDuration(LocalTime start, LocalTime end) {
        long duration = ChronoUnit.SECONDS.between(start, end);
        LocalTime expectedTime = LocalTime.parse("00:00:00", TimestampFormatter.getTime());
        expectedTime = expectedTime.plusSeconds(duration);

        return expectedTime;

    }


    private void createPublicPointOnly() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();
    }

    private void createExampleHourTasks() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);
        temporaryPublicPoint.mkdirs();

        File exampleEmptyFolder = new File(PUBLIC_POINT_PATH + "/archive");
        exampleEmptyFolder.mkdirs();

        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-13,08:14:11", "12663 Important Client", "preparing mesh");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-13,10:14:11", "13543 Standard Client", "create load cases");
        writeExampleHourTask(TaskType.BREAK, PUBLIC_POINT_PATH,
                "2019-09-13,12:14:11", "Supper", "in kitchen");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-13,13:14:11", "12663 Important Client", "quality check");
        writeExampleHourTask(TaskType.ENDDAY, PUBLIC_POINT_PATH,
                "2019-09-13,20:14:11", "12663 Important Client", "preparing ppt");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-14,07:18:12", "13543 Standard Client", "writing report");
        writeExampleHourTask(TaskType.BREAK, PUBLIC_POINT_PATH,
                "2019-09-14,09:14:11", "Supper", "in snack bar");
        writeExampleHourTask(TaskType.BREAK, PUBLIC_POINT_PATH,
                "2019-09-14,12:14:11", "Tea", "with client, but not only");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-14,15:14:11", "12663 Important Client", "telco");
        writeExampleHourTask(TaskType.ENDDAY, PUBLIC_POINT_PATH,
                "2019-09-14,18:14:11", "13543 Standard Client", "importany meeting");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-15,10:14:11", "12663 Important Client", "finding new solution");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-15,12:14:11", "12663 Important Client", "improving construction");
        writeExampleHourTask(TaskType.WORK, PUBLIC_POINT_PATH,
                "2019-09-15,14:14:11", "13543 Standard Client", "optimization");
    }

    private void writeExampleHourTask(TaskType type, String publicPoint, String dateTime, String task, String description) {
        HourTask hourTaskToWrite;
        hourTaskToWrite = new HourTaskImpl(type, publicPoint);
        hourTaskToWrite.setTimeStamp(LocalDateTime.parse(dateTime, TimestampFormatter.getDateTime()));
        hourTaskToWrite.setTask(task);
        hourTaskToWrite.setDescription(description);
        hourTaskToWrite.write();
    }

    private List<HourTask> hourTasksWhenNothingInPublicPoint() {
        File publicPoint = new File(PUBLIC_POINT_PATH);

        HourTask hourTaskNoTasks = new HourTaskImpl(
                TaskType.ENDDAY, publicPoint.getAbsolutePath());
        hourTaskNoTasks.setTimeStamp(LocalDateTime.now());
        hourTaskNoTasks.setTask("NO TASK YET");
        hourTaskNoTasks.setDescription("In public point: " + publicPoint.getAbsolutePath() + " there are no " +
                "tasks yet. Please run hoursDaily first and generate some hourTask.");
        List<HourTask> noTasks = new ArrayList<>();
        noTasks.add(hourTaskNoTasks);

        return noTasks;
    }

    private void deleteTemporaryPublicPoint() {
        File temporaryPublicPoint = new File(PUBLIC_POINT_PATH);

        //FIXME comment if you want to see these files
        File[] files = temporaryPublicPoint.listFiles();
        try {
            for (File file : files) {
                file.delete();
            }
        } catch (Exception e) {
            System.out.println("(TEST: There was no files)");
        }
        temporaryPublicPoint.delete();
    }
}