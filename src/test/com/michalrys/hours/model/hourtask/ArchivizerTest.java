/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.model.hourtask;

import com.michalrys.hours.tools.TimestampFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArchivizerTest {
    private final String PUBLIC_POINT_PATH_FOR_TEST_ONLY = "d:/temp_public_point";

    @Test
    public void shouldMoveAllTaskFilesToArchiveFolderSplittedByYearsAndWorkWeeks() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasksWithSomeSubTaskTheSameLikeMainTask(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        Archivizer archivizer = new Archivizer(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        File publicPoint = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        File[] files = publicPoint.listFiles();
        List<File> hourTasksFilesOnly = getHourTasksOnly(files);

        //when
        archivizer.moveAll();

        //then
        File archive = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE");
        File archive2018 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2018");
        File archive2018ww40 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2018/WW40");
        File archive2018ww41 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2018/WW41");
        File archive2019 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2019");
        File archive2019ww01 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2019/WW01");
        File archive2019ww42 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2019/WW42");
        List<File> archive2018ww40Files = Arrays.asList(archive2018ww40.listFiles());
        List<File> archive2018ww41Files = Arrays.asList(archive2018ww41.listFiles());
        List<File> archive2019ww01Files = Arrays.asList(archive2019ww01.listFiles());
        List<File> archive2019ww42Files = Arrays.asList(archive2019ww42.listFiles());

        Assert.assertTrue(archive.exists());
        Assert.assertTrue(archive2018.exists());
        Assert.assertTrue(archive2019.exists());
        Assert.assertEquals(archive2018ww40Files.size(), 6);
        Assert.assertEquals(archive2018ww41Files.size(), 21);
        Assert.assertEquals(archive2019ww01Files.size(), 9);
        Assert.assertEquals(archive2019ww42Files.size(), 9);
    }

    @Test
    public void shouldMoveToArchiveTasksFromGivenWorkWeekOnly() {
        //given
        preparePublicPoint(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        createDummyHourTasksWithSomeSubTaskTheSameLikeMainTask(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        Archivizer archivizer = new Archivizer(PUBLIC_POINT_PATH_FOR_TEST_ONLY);

        File publicPoint = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY);
        File[] files = publicPoint.listFiles();
        List<File> hourTasksFilesOnly = getHourTasksOnly(files);

        //when
        archivizer.move(1, 40);

        //then
        File archive = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE");
        File archive2018 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2018");
        File archive2018ww40 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2018/WW40");
        File archive2019 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2019");
        File archive2019ww01 = new File(PUBLIC_POINT_PATH_FOR_TEST_ONLY + "/ARCHIVE/2019/WW01");
        List<File> archive2018ww40Files = Arrays.asList(archive2018ww40.listFiles());
        List<File> archive2019ww01Files = Arrays.asList(archive2019ww01.listFiles());

        Assert.assertTrue(archive.exists());
        Assert.assertTrue(archive2018.exists());
        Assert.assertTrue(archive2019.exists());
        Assert.assertEquals(archive2018ww40Files.size(), 6);
        Assert.assertEquals(archive2019ww01Files.size(), 9);
    }

    private List<File> getHourTasksOnly(File[] files) {
        List<File> hourTaskFilesOnly = new ArrayList<>();
        for (File file : files) {
            String name = file.getName();
            if (name.matches("HourTask_20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9].json")) {
                hourTaskFilesOnly.add(file);
            }
        }
        return hourTaskFilesOnly;
    }

    private void preparePublicPoint(String publicPointPath) {
        File publicPoint = new File(publicPointPath);
        File[] files = publicPoint.listFiles();
        for (File file : files) {
            file.delete();
        }
        publicPoint.delete();
        publicPoint.mkdirs();
    }

    private List<LocalDate> createDummyHourTasksWithSomeSubTaskTheSameLikeMainTask(String publicPointPath) {
        //day 1
        String dayStart1 = "2018-10-07,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,06:01:00",
                "12663 Important Client",
                "preparing mesh");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,08:01:42",
                "12663 Important Client",
                "making report");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-07,12:01:36",
                "kitchen",
                "supper");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,12:30:24",
                "15332 Important Client",
                "thinking");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-07,17:03:01",
                "12663 Important Client",
                "");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-07,17:08:01",
                "12663 Important Client",
                "");
        // day 2
        String dayStart2 = "2018-10-11,06:01:00";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,06:01:00",
                "12663 Important Client",
                "creating mock");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,09:01:42",
                "12663 Important Client",
                "telco");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-11,11:01:36",
                "kitchen",
                "eating sth");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,11:40:24",
                "15332 Important Client",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-11,15:55:01",
                "12663 Important Client",
                "sth else");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-11,19:03:55",
                "12663 Important Client",
                "FINITo");
        // day 3
        String dayStart3 = "2018-10-12,04:48:20";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,04:48:20",
                "78440 new super project",
                "meshing");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,05:55:52",
                "12663 Important Client",
                "discussion");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-12,10:12:36",
                "kitchen",
                "important meeting");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,11:10:24",
                "78440 new super project",
                "report");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-12,15:23:01",
                "12663 Important Client",
                "i dont know");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-12,18:23:01",
                "12663 Important Client",
                "nothing special");
        // day 4
        String dayStart4 = "2018-10-13,06:01:05";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-13,06:01:05",
                "78440 new super project",
                "review emails");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-13,06:34:15",
                "12663 Important Client",
                "teleconference meeting");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-13,07:01:00",
                "INNE",
                "eating second breakfast");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-13,07:15:31",
                "78440 new super project",
                "continue work");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-13,13:45:01",
                "12663 Important Client",
                "answer to emails");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2018-10-13,13:57:17",
                "INNE",
                "INNE");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-13,14:02:31",
                "78440 new super project",
                "continue work");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2018-10-13,14:59:01",
                "78440 new super project",
                "78440 new super project");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2018-10-13,15:17:16",
                "78440 new super project",
                "last talk");
        // day 5
        String dayStart5 = "2019-01-03,06:01:05";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-01-03,06:01:05",
                "78440 new super project",
                "review emails");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-01-03,06:34:15",
                "12663 Important Client",
                "teleconference meeting");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2019-01-03,07:01:00",
                "INNE",
                "eating second breakfast");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-01-03,07:15:31",
                "78440 new super project",
                "continue work");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-01-03,13:45:01",
                "12663 Important Client",
                "answer to emails");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2019-01-03,13:57:17",
                "INNE",
                "INNE");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-01-03,14:02:31",
                "78440 new super project",
                "continue work");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-01-03,14:59:01",
                "78440 new super project",
                "78440 new super project");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2019-01-03,15:17:16",
                "78440 new super project",
                "last talk");
        // day 6
        String dayStart6 = "2019-10-14,06:01:05";
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-10-14,06:01:05",
                "78440 new super project",
                "review emails");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-10-14,06:34:15",
                "12663 Important Client",
                "teleconference meeting");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2019-10-14,07:01:00",
                "INNE",
                "eating second breakfast");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-10-14,07:15:31",
                "78440 new super project",
                "continue work");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-10-14,13:45:01",
                "12663 Important Client",
                "answer to emails");
        writeExampleHourTask(TaskType.BREAK, publicPointPath,
                "2019-10-14,13:57:17",
                "INNE",
                "INNE");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-10-14,14:02:31",
                "78440 new super project",
                "continue work");
        writeExampleHourTask(TaskType.WORK, publicPointPath,
                "2019-10-14,14:59:01",
                "78440 new super project",
                "78440 new super project");
        writeExampleHourTask(TaskType.ENDDAY, publicPointPath,
                "2019-10-14,15:17:16",
                "78440 new super project",
                "last talk");

        return Arrays.asList(
                LocalDate.parse(dayStart1, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart2, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart3, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart4, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart5, TimestampFormatter.getDateTime()),
                LocalDate.parse(dayStart6, TimestampFormatter.getDateTime())
        );
    }

    private void writeExampleHourTask(TaskType type, String publicPoint, String dateTime, String task, String description) {
        HourTask hourTaskToWrite;
        hourTaskToWrite = new HourTaskImpl(type, publicPoint);
        hourTaskToWrite.setTimeStamp(LocalDateTime.parse(dateTime, TimestampFormatter.getDateTime()));
        hourTaskToWrite.setTask(task);
        hourTaskToWrite.setDescription(description);
        hourTaskToWrite.write();

        File taskLinksExample = new File(publicPoint + "\\TaskLink.json");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(taskLinksExample))) {
            writer.write("{\n" +
                    "    \"12663 Important Client\": \"d:\\\\test.txt\",\n" +
                    "    \"78440 new super project\": \"c:\\\\Windows\\\\notepad.exe\",\n" +
                    "    \"15332 Important Client\": \"c:\\\\test.txt\"\n" +
                    "}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}