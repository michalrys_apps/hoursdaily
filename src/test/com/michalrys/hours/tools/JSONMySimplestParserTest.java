/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.tools;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class JSONMySimplestParserTest {

    @Test
    public void shouldCreatePrettyPrintedJsonForGivenSingleSimpleData() {
        //given
        File tempFileToWrite = setFileAndMakeDirectory("d:/_temp_for_JsonPretty/tempFileA.json");

        String simpleDataName = "dummy name 1";
        Set<String> simpleDataValue = new HashSet<>();
        simpleDataValue.add("value 1");
        simpleDataValue.add("value 2");
        simpleDataValue.add("value 3");

        String expected = "{\"dummy name 1\": [\n    \"value 3\",\n    \"value 2\",\n    \"value 1\"\n]}";

        //when
        JSONMySimplestParser.write(tempFileToWrite, simpleDataName, simpleDataValue);

        //then
        String results = readFile(tempFileToWrite);
        deleteFile(tempFileToWrite);
        Assert.assertEquals(expected, results);
    }

    @Test
    public void shouldCreatePrettyJsonForGivenJsonObject() {
        //given
        File tempFileToWrite = setFileAndMakeDirectory("d:/_temp_for_JsonPretty/tempFileA.json");

        JSONObject givenJsonObject = new JSONObject();
        givenJsonObject.append("name 1", "value 1");
        givenJsonObject.append("name 2", "value 2");
        givenJsonObject.append("name 3", "value 3");

        String expected = "{" +
                "\n    \"name 3\": [\"value 3\"]," +
                "\n    \"name 2\": [\"value 2\"]," +
                "\n    \"name 1\": [\"value 1\"]" +
                "\n}";

        //when
        JSONMySimplestParser.write(tempFileToWrite, givenJsonObject);

        //then
        String results = readFile(tempFileToWrite);
        deleteFile(tempFileToWrite);
        Assert.assertEquals(expected, results);
    }

    private File setFileAndMakeDirectory(String filePath) {
        File file = new File(filePath);
        String parentPath = file.getParent();
        File parentPathFile = new File(parentPath);
        parentPathFile.mkdirs();
        return file;
    }

    private String readFile(File file) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            lines = reader.lines().collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder results = new StringBuilder();
        for (String line : lines) {
            results.append(line);
            results.append("\n");
        }
        results.replace(results.length() - 1, results.length(), "");
        return results.toString();
    }

    private void deleteFile(File file) {
        String parent = file.getParent();
        file.delete();
        File fileParent = new File(parent);
        fileParent.delete();
    }
}