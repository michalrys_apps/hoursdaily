/*
Copyright (C) 2019-2021 Michał Ryś <michalrys@gmail.com>
All rights reserved.
This file is part of the HoursDaily project.
The HoursDaily project can not be copied and/or distributed without the express
permission of Michał Ryś <michalrys@gmail.com>. This is for now - it will be updated soon.
*/
package com.michalrys.hours.tools;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TimestampFormatterTest {

    @Test
    public void shouldParseGivenDateTime(){
        //given
        String date = "2019-05-13,23:14:55";

        //when
        LocalDateTime parse = LocalDateTime.parse(date, TimestampFormatter.getDateTime());

        //then
        Assert.assertEquals("2019-05-13T23:14:55", parse.toString());
    }

    @Test
    public void shouldParseGivenTime(){
        //given
        String inputData = "23:14:55";

        //when
        LocalTime parse = LocalTime.parse(inputData, TimestampFormatter.getTime());

        //then
        Assert.assertEquals("23:14:55", parse.toString());
    }

    @Test
    public void shouldParseGivenDate(){
        //given
        String inputData = "2019-05-13";

        //when
        LocalDate parse = LocalDate.parse(inputData, TimestampFormatter.getDate());

        //then
        Assert.assertEquals("2019-05-13", parse.toString());
    }

    @Test
    public void shouldParseGivenDateTimeForFileName(){
        //given
        String date = "2019-05-13_23-14-55";

        //when
        LocalDateTime parse = LocalDateTime.parse(date, TimestampFormatter.getDateTimeForFileName());

        //then
        Assert.assertEquals("2019-05-13T23:14:55", parse.toString());
    }
}